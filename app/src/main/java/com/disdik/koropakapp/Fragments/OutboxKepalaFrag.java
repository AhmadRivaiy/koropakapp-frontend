package com.disdik.koropakapp.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterOutboxFinishSek;
import com.disdik.koropakapp.Class.AdapterOutboxKepala;
import com.disdik.koropakapp.Class.AdapterOutboxSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SieActivity.DetailOutboxSie;
import com.disdik.koropakapp.TikomdikActivity.DetailOutboxKepala;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutboxKepalaFrag extends Fragment {
    private View outboxFragment;

    private RecyclerView listView, listViewFinish;
    private EditText searchBtn;
    private ProgressBar progressBar;
    private CardView valid, noValid;
    private ImageView icKlr, icFinish;
    private String IdKantor, nameV, IdMaster, IdBidang;
    private FrameLayout titleSurat, suratLayout, finisLayout, titleFinish;

    private AdapterOutboxSekdis adapter;
    private AdapterOutboxFinishSek adapterFinish;
    private AdapterEmpty adapterEmpty;
    private ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;
    private ArrayList<HashMap<String, String>> list_dataEmptyClone;
    private ArrayList<HashMap<String, String>> list_dataFinish;

    private int stateLaySur = 1;
    private int stateLayFin = 1;
    private int stateEmptyList = 0;

    private String CondFilter = "list_keluar", CondSrt = "1";
    public OutboxKepalaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        outboxFragment = inflater.inflate(R.layout.fragment_outbox_kepala, container, false);
        searchBtn   =   outboxFragment.findViewById(R.id.searchBox);
        progressBar =   outboxFragment.findViewById(R.id.progressBarList);
        valid       =   outboxFragment.findViewById(R.id.validCard);
        noValid     =   outboxFragment.findViewById(R.id.noValidCard);

        //TODO: Get Data From Shared Preference
        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);

        //TODO:Set Rycleview Properties
        listView = outboxFragment.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        //TODO: Collapse Expand Navbar
        valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
        valid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            noValid.setCardBackgroundColor(Color.WHITE);
            CondSrt = "1";

            CondFilter = "list_keluar_finish";
            getListSuratKeluar();
        });

        noValid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.WHITE);
            noValid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            CondSrt = "0";

            CondFilter = "list_keluar";
            getListNoParaf();
            //Toast.makeText(getActivity(), "No Valid", Toast.LENGTH_SHORT).show();
        });

        return outboxFragment;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    showListMessage(s, CondFilter);
                } catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);
                data.put("query", query);

                return rh.sendPostRequest(Config.URL_FILTER_OUTBOX_KEPALA, data);

            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void getListSuratKeluar(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    showListMessage(s, "list_keluar_finish");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_GET_SURAT_KELUAR_KEPALA, data);

            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void getListNoParaf(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    showListMessage(s, "list_keluar");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_GET_SURAT_KELUAR_KEPALA, data);

            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListMessage(String s, String Cond) {
        list_dataEmpty = new ArrayList<HashMap<String, String>>();
        list_data = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray(Cond);
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("status_read_sekdis", json.getString("status_read_sekdis"));
                    map.put("title", json.getString("title"));
                    map.put("message", json.getString("message"));
                    map.put("statDate", json.getString("statDate"));
                    list_data.add(map);
                    adapter = new AdapterOutboxSekdis(getActivity(), list_data);
                    ((AdapterOutboxSekdis) adapter).setOnItemClickListener(new AdapterOutboxSekdis.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailOutboxKepala.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxFragment,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(CondSrt.equals("1")){
            getListSuratKeluar();
        }else{
            getListNoParaf();
        }
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

}
