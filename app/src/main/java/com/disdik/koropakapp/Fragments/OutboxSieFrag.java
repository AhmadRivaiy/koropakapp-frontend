package com.disdik.koropakapp.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterOutboxFinishSek;
import com.disdik.koropakapp.Class.AdapterOutboxSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.DetailOutboxSekdis;
import com.disdik.koropakapp.SieActivity.DetailOutboxSie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutboxSieFrag extends Fragment {
    private View outboxSieFrag;

    private RecyclerView listView, listViewFinish;
    private EditText searchBtn;
    private ProgressBar progressBar;
    private ImageView icKlr, icFinish;
    private CardView valid, noValid;
    private String IdKantor, nameV, IdMaster, IdBidang;
    private FrameLayout titleSurat, suratLayout, finisLayout, titleFinish;

    private AdapterOutboxSekdis adapter;
    private AdapterOutboxFinishSek adapterFinish;
    private AdapterEmpty adapterEmpty;
    private ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;
    private ArrayList<HashMap<String, String>> list_dataEmptyClone;
    private ArrayList<HashMap<String, String>> list_dataFinish;

    private int stateLaySur = 1;
    private int stateLayFin = 1;
    private int stateEmptyList = 0;

    private String CondFilter = "list_keluar_finish", CondSrt = "1";
    public OutboxSieFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        outboxSieFrag = inflater.inflate(R.layout.fragment_outbox_sie, container, false);
        searchBtn   =   outboxSieFrag.findViewById(R.id.searchBox);
        progressBar =   outboxSieFrag.findViewById(R.id.progressBarList);
//        icKlr       =   outboxSieFrag.findViewById(R.id.icTitleKlr);
//        icFinish    =   outboxSieFrag.findViewById(R.id.icFinishKlr);
//        finisLayout =   outboxSieFrag.findViewById(R.id.finishLayout);
//        titleFinish =   outboxSieFrag.findViewById(R.id.titleFinish);
//        titleSurat  =   outboxSieFrag.findViewById(R.id.titleSurat);
//        suratLayout =   outboxSieFrag.findViewById(R.id.suratLayout);
        valid       =   outboxSieFrag.findViewById(R.id.validCard);
        noValid     =   outboxSieFrag.findViewById(R.id.noValidCard);

        //TODO: Get Data From Shared Preference
        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);
        //End

        //TODO:Set Rycleview Properties
        listView = outboxSieFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        //TODO: Button Search
        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        //TODO: Collapse Expand Navbar
//        if(stateLaySur == 1){
//            icKlr.setImageResource(R.drawable.ic_expand);
//            stateLaySur = 0;
//        }
//
//        if(stateLayFin == 1){
//            icFinish.setImageResource(R.drawable.ic_expand);
//            stateLayFin = 0;
//        }
//
//        titleFinish.setOnClickListener(v -> {
//            if(stateLayFin == 1){
//                icFinish.setImageResource(R.drawable.ic_expand);
//                expand(finisLayout);
//                stateLayFin = 0;
//            }else {
//                icFinish.setImageResource(R.drawable.ic_expand_less);
//                collapse(finisLayout);
//                stateLayFin = 1;
//            }
//        });
//
//        titleSurat.setOnClickListener(v -> {
//            if(stateLaySur == 1){
//                icKlr.setImageResource(R.drawable.ic_expand);
//                expand(suratLayout);
//                stateLaySur = 0;
//            }else {
//                icKlr.setImageResource(R.drawable.ic_expand_less);
//                collapse(suratLayout);
//                stateLaySur = 1;
//            }
//        });

        valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
        valid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            noValid.setCardBackgroundColor(Color.WHITE);
            CondSrt = "1";

            CondFilter = "list_keluar_finish";
            getListMessageKlr();
        });

        noValid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.WHITE);
            noValid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            CondSrt = "0";

            CondFilter = "list_keluar";
            getListNoParaf();
            //Toast.makeText(getActivity(), "No Valid", Toast.LENGTH_SHORT).show();
        });

        return outboxSieFrag;
    }

    //TODO: Filter Message
    private void getFilter(final String query){
        class GetListFilter extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Log.d("response ", " " + s);
                try {
                    showListMessageKlr(s, CondFilter);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSieFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);
                data.put("query", query);

                return rh.sendPostRequest(Config.URL_FILTER_OUTBOX_SIE, data);
            }
        }
        GetListFilter ge = new GetListFilter();
        ge.execute();
    }

    //TODO:Get List Surat Keluar
    private void getListNoParaf(){
        class GetListSrtKlr extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.d("response ", " " + s);
                    showListMessageKlr(s, "list_keluar");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSieFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_GET_MESSAGE_SIE_OUT, data);
            }
        }
        GetListSrtKlr ge = new GetListSrtKlr();
        ge.execute();
    }

    private void getListMessageKlr(){
        class GetListSrtKlr extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.d("response ", " " + s);
                    showListMessageKlr(s, "list_keluar_finish");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSieFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_GET_MESSAGE_SIE_OUT, data);
            }
        }
        GetListSrtKlr ge = new GetListSrtKlr();
        ge.execute();
    }

    //TODO:Show Message with No Signature
    private void showListMessageKlr(String s, String Cond){
        list_data = new ArrayList<HashMap<String, String>>();
        list_dataEmpty = new ArrayList<HashMap<String, String>>();

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray(Cond);
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("no_agenda_klr", json.getString("no_agenda_klr"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("status_read_sekdis", json.getString("status_read_sekdis"));
                    map.put("title", json.getString("title"));
                    map.put("message", json.getString("message"));
                    map.put("statDate", json.getString("statDate"));
                    map.put("isSelected", json.getString("isSelected"));

                    list_data.add(map);
                    adapter = new AdapterOutboxSekdis(getActivity(), list_data);
                    ((AdapterOutboxSekdis) adapter).setOnItemClickListener(new AdapterOutboxSekdis.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailOutboxSie.class);
                            HashMap<String,String> map = (HashMap)list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity()!=null){
                        listView.setAdapter(adapter);
                    }
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
                stateEmptyList = 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxSieFrag,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(CondSrt.equals("1")){
            getListMessageKlr();
        }else{
            getListNoParaf();
        }
        //valueSrt.clear();
        //nameSrt.clear();
    }
}
