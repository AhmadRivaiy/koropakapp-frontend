package com.disdik.koropakapp.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.AdapterRiwayatGtk;
import com.disdik.koropakapp.Class.AdapterRiwayatSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiwayatGtkFrag extends Fragment {
    private View riwayatGtkFrag;
    private RecyclerView listView;
    private LayoutInflater inflater;
    private AdapterRiwayatGtk adapter;
    ArrayList<HashMap<String, String>> list_data;

    private String getId;
    public RiwayatGtkFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        riwayatGtkFrag = inflater.inflate(R.layout.fragment_riwayat_gtk, container, false);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getId = user.get(SharedPrefManager.SP_ID_JABATAN);

        listView = riwayatGtkFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        //getListRiwayat();


        return riwayatGtkFrag;
    }

    private void getListRiwayat(){
        class getListDisposisi extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat_gtk");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Riwayat Kosong!", Toast.LENGTH_SHORT).show();
                    }else {
                        showListRiwayat(s);
                    }
                } catch (JSONException a){
                    a.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), riwayatGtkFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_RIWAYAT_GTK, getId);
                return s;
            }
        }
        getListDisposisi ge = new getListDisposisi();
        ge.execute();
    }

    private void showListRiwayat(String s) {

        list_data = new ArrayList<HashMap<String, String>>();
        Log.d("response ", s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat_gtk");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("no", json.getString("no"));
                    map.put("no_surat_msk", json.getString("no_surat_msk"));
                    map.put("sifat", json.getString("sifat"));
                    map.put("isi_disposisi", json.getString("isi_disposisi"));
                    map.put("tgl_disposisi", json.getString("tgl_disposisi"));
                    map.put("tgl_disposisi_str", json.getString("tgl_disposisi_str"));
                    map.put("status_baca", json.getString("status_baca"));
                    map.put("status_str", json.getString("status_str"));
                    map.put("id_master", json.getString("id_master"));
                    map.put("isi_laporan", json.getString("isi_laporan"));
                    map.put("files", json.getString("files"));
                    map.put("nama", json.getString("nama"));
                    list_data.add(map);
                    adapter = new AdapterRiwayatGtk(getActivity(), list_data);
                    ((AdapterRiwayatGtk) adapter).setOnItemClickListener(new AdapterRiwayatGtk.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String status = list_data.get(position).get("status_str");
                            String tujuan = list_data.get(position).get("nama");
                            String tgl = list_data.get(position).get("tgl_disposisi");
                            String isi = list_data.get(position).get("isi_laporan").toString();
                            String img = list_data.get(position).get("files");
                            helpFile(status, tujuan, tgl, isi, img);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Pesan Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), riwayatGtkFrag,
                        "No Internet Connection!");
            }
        }
    }

    public void helpFile(String sender, String tujuan, String tgl, String isi,String img) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.cek_laporan, null);
        TextView statusL    = dialogView.findViewById(R.id.statusLapor);
        TextView tujuanL    = dialogView.findViewById(R.id.tujuanLapor);
        TextView tanggalL   = dialogView.findViewById(R.id.tanggalLapor);
        TextView isiL       = dialogView.findViewById(R.id.isiLapor);
        ImageView imgL      = dialogView.findViewById(R.id.fotoLaporan);

        statusL.setText(sender);
        tujuanL.setText(tujuan);
        tanggalL.setText(tgl);
        isiL.setText(isi);

        Glide.with(getActivity())
                .load(Config.URL_IMG_LAPORAN + img)
                .asBitmap()
                .into(imgL);
        alertDialogBuilder.setView(dialogView);
        alertDialogBuilder.setCancelable(true);
        AlertDialog builder = alertDialogBuilder.create();
        builder.show();
    }

    @Override
    public void onResume(){
        super.onResume();
        getListRiwayat();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

}
