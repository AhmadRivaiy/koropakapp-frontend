package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterInbox;
import com.disdik.koropakapp.Class.AdapterOutbox;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailMessage;
import com.disdik.koropakapp.DetailSuratKlr;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutboxAgendaFrag extends Fragment {
    private View outboxAgendaFrag;
    private RecyclerView listView;
    private CardView valid, noValid;
    private ProgressBar progressBar;

    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private AdapterOutbox adapter;
    private AdapterEmpty adapterEmpty;
    String getSessionId, IdBidang;

    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    ProgressDialog loading;
    private String IdKantor, IdMaster, CondFilter = "list_klr", CondSrt = "1";
    public OutboxAgendaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        outboxAgendaFrag = inflater.inflate(R.layout.fragment_outbox_agenda, container, false);
        requestQueue = Volley.newRequestQueue(getActivity());
        valid       =   outboxAgendaFrag.findViewById(R.id.validCard);
        noValid     =   outboxAgendaFrag.findViewById(R.id.noValidCard);
        progressBar =   outboxAgendaFrag.findViewById(R.id.loadKlrValidasi);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionId    = user.get(SharedPrefManager.SP_ID_JABATAN);
        IdBidang        = user.get(SharedPrefManager.SP_ID_KANTOR);

        listView = outboxAgendaFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
        valid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            noValid.setCardBackgroundColor(Color.WHITE);
            CondSrt = "1";
            progressBar.setVisibility(View.VISIBLE);
            CondFilter = "list_klr_valid";
            getListValid();
            //Toast.makeText(getActivity(), "Valid", Toast.LENGTH_SHORT).show();
        });

        noValid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.WHITE);
            noValid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            CondSrt = "0";
            progressBar.setVisibility(View.VISIBLE);
            CondFilter = "list_klr";
            getListMessageKlr();
            //Toast.makeText(getActivity(), "No Valid", Toast.LENGTH_SHORT).show();
        });

        //getListMessageKlr();
        return outboxAgendaFrag;
    }

    private void getListMessageKlr(){
        class listMessageKlr extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                collapse(progressBar);
                try {
                    showListMessageKlr(s, "list_klr");
                    }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_MESSAGE_OUT, IdBidang);
                return s;
            }
        }
        listMessageKlr ge = new listMessageKlr();
        ge.execute();
    }

    private void getListValid(){
        class listKlrValid extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                collapse(progressBar);
                try {
                    showListMessageKlr(s, "list_klr_valid");
                }catch (Exception a){
                    a.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_MESSAGE_OUT, IdBidang);
                return s;
            }
        }
        listKlrValid ge = new listKlrValid();
        ge.execute();
    }

    private void showListMessageKlr(String s, String obj) {
        list_data = new ArrayList<HashMap<String, String>>();
        list_dataEmpty = new ArrayList<HashMap<String, String>>();

        Log.d("response ", " " + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray(obj);
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("no_surat_klr", json.getString("no_surat_klr"));
                    map.put("tujuan_surat_klr", json.getString("tujuan_surat_klr"));
                    map.put("tgl_surat_klr", json.getString("tgl_surat_klr"));
                    map.put("status", json.getString("status"));
                    map.put("statDate", json.getString("statDate"));
                    list_data.add(map);
                    adapter = new AdapterOutbox(getActivity(), list_data);
                    ((AdapterOutbox) adapter).setOnItemClickListener(new AdapterOutbox.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailSuratKlr.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxAgendaFrag,
                        "No Internet Connection!");
            }
        }
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(CondSrt.equals("1")){
            getListValid();
        }else{
            getListMessageKlr();
        }
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home) {

            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
