package com.disdik.koropakapp.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterDisposisiSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.DetailDisposisiSekdis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class DisposisiSekdisFrag extends Fragment {
    public View disposisiSekdisFrag;
    private RecyclerView listView;
    private AdapterDisposisiSekdis adapter;
    ArrayList<HashMap<String, String>> list_data;

    private EditText searchBtn;
    private ProgressBar progressBar;

    private String getId;
    public DisposisiSekdisFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        disposisiSekdisFrag = inflater.inflate(R.layout.fragment_disposisi_sekdis, container, false);
        searchBtn   =   disposisiSekdisFrag.findViewById(R.id.searchBox);
        progressBar =   disposisiSekdisFrag.findViewById(R.id.progressBarList);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getId = user.get(SharedPrefManager.SP_ID_MASTER);

        listView = disposisiSekdisFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        //getListDisposisi();
        return disposisiSekdisFrag;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_disposisi");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), disposisiSekdisFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FILTER_DISPOSISI_SEKDIS, getId + "&query=" + query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getListDisposisi(){
        class getListDisposisi extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_disposisi");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Pesan Kosong", Toast.LENGTH_SHORT).show();
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), disposisiSekdisFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_DISPOSISI_SEKDIS, getId);
                return s;
            }
        }
        getListDisposisi ge = new getListDisposisi();
        ge.execute();
    }

    private void showListMessage(String s){
        list_data = new ArrayList<HashMap<String, String>>();
        Log.d("response ", s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_disposisi");
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("no", json.getString("no"));
                    map.put("id_disposisi", json.getString("id_disposisi"));
                    map.put("id_surat_msk", json.getString("id_surat_msk"));
                    map.put("isi_disposisi", json.getString("isi_disposisi"));
                    map.put("asal_surat_msk", json.getString("asal_surat_msk"));
                    map.put("tgl_surat_msk", json.getString("tgl_surat_msk"));
                    map.put("status_baca", json.getString("status_baca"));
                    map.put("tgl_disposisi_str", json.getString("tgl_disposisi_str"));
                    map.put("statDate", json.getString("statDate"));
                    list_data.add(map);
                    adapter = new AdapterDisposisiSekdis(getActivity(), list_data);
                    ((AdapterDisposisiSekdis) adapter).setOnItemClickListener(new AdapterDisposisiSekdis.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailDisposisiSekdis.class);
                            HashMap<String,String> map = (HashMap)list_data.get(position);
                            String empId = map.get(Config.TAG_ID_DISPOSISI).toString();
                            intent.putExtra("id_disposisi", empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity()!=null){
                        listView.setAdapter(adapter);
                    }
                }
            }else{
                Toast.makeText(getActivity(), "Disposisi Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), disposisiSekdisFrag,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getListDisposisi();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

}
