package com.disdik.koropakapp.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterOutboxFinishSek;
import com.disdik.koropakapp.Class.AdapterOutboxSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.DetailOutboxSekdis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutboxGtkFrag extends Fragment {
    private View outboxGtkFrag;

    private RecyclerView listView, listViewFinish;
    private EditText searchBtn;
    private ProgressBar progressBar;
    private ImageView icKlr, icFinish;
    private String IdKantor, nameV, IdMaster, IdBidang;
    private FrameLayout titleSurat, suratLayout, finisLayout, titleFinish;

    private AdapterOutboxSekdis adapter;
    private AdapterOutboxFinishSek adapterFinish;
    private AdapterEmpty adapterEmpty;
    private ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;
    private ArrayList<HashMap<String, String>> list_dataEmptyClone;
    private ArrayList<HashMap<String, String>> list_dataFinish;

    private int stateLaySur = 1;
    private int stateLayFin = 1;
    private int stateEmptyList = 0;

    public OutboxGtkFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        outboxGtkFrag = inflater.inflate(R.layout.fragment_outbox_gtk, container, false);
        searchBtn   =   outboxGtkFrag.findViewById(R.id.searchBox);
        progressBar =   outboxGtkFrag.findViewById(R.id.progressBarList);
        icKlr       =   outboxGtkFrag.findViewById(R.id.icTitleKlr);
        icFinish    =   outboxGtkFrag.findViewById(R.id.icFinishKlr);
        finisLayout =   outboxGtkFrag.findViewById(R.id.finishLayout);
        titleFinish =   outboxGtkFrag.findViewById(R.id.titleFinish);
        titleSurat  =   outboxGtkFrag.findViewById(R.id.titleSurat);
        suratLayout =   outboxGtkFrag.findViewById(R.id.suratLayout);

        //TODO: Get Data From Shared Preference
        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);
        //End

        //TODO:Set Rycleview Properties
        listView = outboxGtkFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        listViewFinish = outboxGtkFrag.findViewById(R.id.listViewFinish);
        LinearLayoutManager llmFin = new LinearLayoutManager(getActivity());
        llmFin.setOrientation(RecyclerView.VERTICAL);
        listViewFinish.setLayoutManager(llmFin);

        //TODO: Button Search
        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        //TODO: Collapse Expand Navbar
        if(stateLaySur == 1){
            icKlr.setImageResource(R.drawable.ic_expand);
            stateLaySur = 0;
        }

        if(stateLayFin == 1){
            icFinish.setImageResource(R.drawable.ic_expand);
            stateLayFin = 0;
        }

        titleFinish.setOnClickListener(v -> {
            if(stateLayFin == 1){
                icFinish.setImageResource(R.drawable.ic_expand);
                expand(finisLayout);
                stateLayFin = 0;
            }else {
                icFinish.setImageResource(R.drawable.ic_expand_less);
                collapse(finisLayout);
                stateLayFin = 1;
            }
        });

        titleSurat.setOnClickListener(v -> {
            if(stateLaySur == 1){
                icKlr.setImageResource(R.drawable.ic_expand);
                expand(suratLayout);
                stateLaySur = 0;
            }else {
                icKlr.setImageResource(R.drawable.ic_expand_less);
                collapse(suratLayout);
                stateLaySur = 1;
            }
        });

        return outboxGtkFrag;
    }

    //TODO: Filter Message
    private void getFilter(final String query){
        class GetListFilter extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    Log.d("GTK Filter", " " + s);
                    showListMessageKlr(s);
                    showList(s);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxGtkFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_FILTER_OUTBOX_GTK, data);
            }
        }
        GetListFilter ge = new GetListFilter();
        ge.execute();
    }

    //TODO:Get List Surat Keluar
    private void getListMessageKlr(){
        class GetListSrtKlr extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.d("GTK", " " + s);
                    showListMessageKlr(s);
                    showList(s);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxGtkFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);

                return rh.sendPostRequest(Config.URL_GET_SURAT_KELUAR_GTK, data);
            }
        }
        GetListSrtKlr ge = new GetListSrtKlr();
        ge.execute();
    }

    //TODO:Show Message with No Signature
    private void showListMessageKlr(String s){
        list_data = new ArrayList<HashMap<String, String>>();
        list_dataEmpty = new ArrayList<HashMap<String, String>>();

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_keluar");
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("no_surat_klr", json.getString("no_surat_klr"));
                    map.put("no_agenda_klr", json.getString("no_agenda_klr"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("status_read_sekdis", json.getString("status_read_sekdis"));
                    map.put("tujuan_surat_klr", json.getString("tujuan_surat_klr"));
                    map.put("statDate", json.getString("statDate"));
                    map.put("isSelected", json.getString("isSelected"));

                    list_data.add(map);
                    adapter = new AdapterOutboxSekdis(getActivity(), list_data);
                    ((AdapterOutboxSekdis) adapter).setOnItemClickListener(new AdapterOutboxSekdis.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailOutboxSekdis.class);
                            HashMap<String,String> map = (HashMap)list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity()!=null){
                        listView.setAdapter(adapter);
                    }
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
                stateEmptyList = 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxGtkFrag,
                        "No Internet Connection!");
            }
        }
    }

    //TODO:Show Messagge with Signature
    private void showList(String s){
        list_dataFinish = new ArrayList<HashMap<String, String>>();
        list_dataEmptyClone= new ArrayList<HashMap<String, String>>();

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_keluar_finish");
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("no_surat_klr", json.getString("no_surat_klr"));
                    map.put("no_agenda_klr", json.getString("no_agenda_klr"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("status_read_sekdis", json.getString("status_read_sekdis"));
                    map.put("tujuan_surat_klr", json.getString("tujuan_surat_klr"));
                    map.put("statDate", json.getString("statDate"));
                    map.put("isSelected", json.getString("isSelected"));

                    list_dataFinish.add(map);
                    adapterFinish = new AdapterOutboxFinishSek(getActivity(), list_dataFinish);
                    ((AdapterOutboxFinishSek) adapterFinish).setOnItemClickListener(new AdapterOutboxFinishSek.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailOutboxSekdis.class);
                            HashMap<String,String> map = (HashMap)list_dataFinish.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity()!=null){
                        listViewFinish.setAdapter(adapterFinish);
                    }
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                list_dataEmptyClone.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmptyClone);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listViewFinish.setAdapter(adapterEmpty);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxGtkFrag,
                        "No Internet Connection!");
            }
        }
    }

    //TODO: Animated Expand & Collapse
    private static void expand(final View view) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        getListMessageKlr();
        //valueSrt.clear();
        //nameSrt.clear();
    }
}
