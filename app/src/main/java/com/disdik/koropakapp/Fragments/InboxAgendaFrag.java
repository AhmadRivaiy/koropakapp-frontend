package com.disdik.koropakapp.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterInbox;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailMessage;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxAgendaFrag extends Fragment {
    public View inboxAgendaFrag;
    private RecyclerView listView;
    SharedPrefManager sharedPrefManager;

    private EditText searchBtn;
    private ProgressBar progressBar;

    private AdapterInbox adapter;
    private AdapterEmpty adapterEmpty;
    String getSessionId, IdBidang;
    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    public InboxAgendaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        inboxAgendaFrag = inflater.inflate(R.layout.fragment_inbox_agenda, container, false);
        searchBtn   =   inboxAgendaFrag.findViewById(R.id.searchBox);
        progressBar =   inboxAgendaFrag.findViewById(R.id.progressBarList);

        listView = inboxAgendaFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionId    = user.get(SharedPrefManager.SP_ID_JABATAN);
        IdBidang        = user.get(SharedPrefManager.SP_ID_KANTOR);

        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });
        return inboxAgendaFrag;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_message");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Surat Masuk Kosong", Toast.LENGTH_SHORT).show();
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), inboxAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_FILTER_INBOX_AGENDA, IdBidang + "&query=" + query);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getListMessage(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Fetching...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_message");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Surat Masuk Kosong", Toast.LENGTH_SHORT).show();
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), inboxAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_MESSAGE, IdBidang);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListMessage(String s) {
        list_dataEmpty = new ArrayList<HashMap<String, String>>();
        list_data = new ArrayList<HashMap<String, String>>();

        try {
            Log.d("response ", s);
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_message");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_msk", json.getString("id_surat_msk"));
                    map.put("no_surat_msk", json.getString("no_surat_msk"));
                    map.put("asal_surat_msk", json.getString("asal_surat_msk"));
                    map.put("tgl_surat_msk", json.getString("tgl_surat_msk"));
                    map.put("status_read", json.getString("status_read"));
                    map.put("statDate", json.getString("statDate"));
                    list_data.add(map);
                    adapter = new AdapterInbox(getActivity(), list_data);
                    ((AdapterInbox) adapter).setOnItemClickListener(new AdapterInbox.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailMessage.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Masuk Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), inboxAgendaFrag,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getListMessage();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home) {

            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
