package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeGtkFrag extends Fragment {
    private View homeGtkFrag;

    private TextView name, email, bidang, stat, dtTextview, dis, titleAbsen, dateAbsenCv;
    private String getUsername, getSessionemail, getNamaBidang, getStatus, formattedDate, getIdMaster, getTokenLogin;
    private ImageView profile, icAbsen, icAbsen_Two;
    private TextView MessageAdmin;
    private CardView CvAbsen;
    private ProgressBar pbAbsen;

    public HomeGtkFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeGtkFrag = inflater.inflate(R.layout.fragment_home_gtk, container, false);
        //Call Defined Data
        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionemail = user.get(SharedPrefManager.SP_EMAIL);
        getUsername     = user.get(SharedPrefManager.SP_NAMA);
        getNamaBidang   = user.get(SharedPrefManager.SP_NAMA_JABATAN);
        getStatus       = user.get(SharedPrefManager.SP_STATUS_PEGAWAI);
        getIdMaster     = user.get(SharedPrefManager.SP_ID_MASTER);
        getTokenLogin   = user.get(SharedPrefManager.SP_TOKEN_LOGIN);

        name        = homeGtkFrag.findViewById(R.id.nameKepala);
        email       = homeGtkFrag.findViewById(R.id.emailKepala);
        bidang      = homeGtkFrag.findViewById(R.id.bidangKepala);
        dtTextview  = homeGtkFrag.findViewById(R.id.textDateTextview);
        dis         = homeGtkFrag.findViewById(R.id.disposisiText);
        profile     = homeGtkFrag.findViewById(R.id.profile_image);

        //AbsenPreference
        titleAbsen  = homeGtkFrag.findViewById(R.id.titleKehadiran);
        dateAbsenCv = homeGtkFrag.findViewById(R.id.dateKehadiran);
        icAbsen     = homeGtkFrag.findViewById(R.id.icKehadiran);
        icAbsen_Two = homeGtkFrag.findViewById(R.id.icKehadiran_Dua);

        pbAbsen     = homeGtkFrag.findViewById(R.id.progress_Absen);
        CvAbsen     = homeGtkFrag.findViewById(R.id.cvAbsen);
        CvAbsen.setVisibility(View.GONE);

        RelativeLayout layoutAlert =   homeGtkFrag.findViewById(R.id.layoutAlert);
        MessageAdmin   =  homeGtkFrag.findViewById(R.id.messageFromAdmin);
        ImageView closeAlert    =  homeGtkFrag.findViewById(R.id.closeAlert);

        //SomeData
        name.setText(getUsername);
        email.setText(getSessionemail);
        bidang.setText(getNamaBidang);
        dtTextview.setText(formattedDate);

        try {
            Ion.with(this).load(Config.URL_GET_MSG_ADMIN)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String message   = result.get("messageAdmin").getAsString();
                                MessageAdmin.setText(Html.fromHtml(message));
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }

        try {
            Ion.with(this).load(Config.URL_GET_PASSWORD_NOW + getIdMaster)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String img   = result.get("imageProfile").getAsString();

                                if(img.equals("0")){
                                    Glide.with(getActivity())
                                            .load(R.mipmap.img_gtk)
                                            .crossFade()
                                            .into(profile);
                                }else {
                                    Glide.with(getActivity()).load(Config.URL_IMG_PROFILE + img)
                                            .crossFade()
                                            .into(profile);
                                    profile.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                                }
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }

        try {
            Ion.with(getActivity()).load(Config.URL_CHECK_STATUS_GTK + getIdMaster)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try {
                                String dis_msk = result.get("dis_msk").getAsString();
                                dis.setText(dis_msk);
                            } catch (Exception a) {
                                if (getActivity() != null) {
                                    new CustomToast().Show_Toast(getActivity(), homeGtkFrag,
                                            "Server Maintenance!");
                                }
                            }
                        }
                    });
        }catch (Exception ar){
            ar.printStackTrace();
        }

        closeAlert.setOnClickListener(l->{
            collapse(layoutAlert);
        });

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("E, d MMM yyyy");
        formattedDate = df.format(c.getTime());

        //init();
        return homeGtkFrag;
    }

    private void init(){
        class Init extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pbAbsen.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pbAbsen.setVisibility(View.GONE);
                CvAbsen.setVisibility(View.VISIBLE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("statusAbsen");
                    JSONObject json     = jsonArray.getJSONObject(0);
                    String status       = json.getString("status");
                    String message      = json.getString("message");
                    String dateAbsen    = json.getString("dateAbsen");

                    if (status.equals("0")){
                        new CustomToast().Show_Toast(getActivity(), homeGtkFrag,
                                "Mohon Login Ulang!");
                    }else{
                        if(status.equals("1")){
                            icAbsen.setImageResource(R.drawable.ic_done_hadir);
                            icAbsen_Two.setImageResource(R.drawable.ic_done_hadir);
                            titleAbsen.setText(message);
                            dateAbsenCv.setText(dateAbsen);
                            CvAbsen.setCardBackgroundColor(Color.parseColor("#4CAF50"));
                        }else{
                            icAbsen.setImageResource(R.drawable.ic_nohadir);
                            icAbsen_Two.setImageResource(R.drawable.ic_nohadir);
                            titleAbsen.setText(message);
                            dateAbsenCv.setVisibility(View.GONE);
                            CvAbsen.setCardBackgroundColor(Color.parseColor("#F16459"));
                        }
                    }
                    Log.e("Response Absen", " " + s);
                }catch (JSONException a){
                    a.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), homeGtkFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String url = (Config.URL_CEK_ABSEN);

                HashMap<String,String> data = new HashMap<>();
                data.put("tokenLogin", getTokenLogin);

                String result = rh.sendPostRequest(url, data);
                return result;
            }
        }
        Init ui = new Init();
        ui.execute();
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        init();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }
}
