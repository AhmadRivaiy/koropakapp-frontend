package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterGridSie;
import com.disdik.koropakapp.Class.AdapterRiwayatSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailRiwayat;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiwayatSieFrag extends Fragment {
    private View riwayatSieFrag;
    private GridView simpleGrid;
    private AdapterGridSie adapterGridSie;
    private ProgressBar loadSieProgress;
    private AdapterRiwayatSekdis adapter;
    private AdapterEmpty adapterEmpty;

    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    private LayoutInflater inflater;

    private String getId, getUsername, IdBidang;

    public RiwayatSieFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        riwayatSieFrag = inflater.inflate(R.layout.fragment_riwayat_sie, container, false);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getUsername = user.get(SharedPrefManager.SP_NAMA);
        getId        = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdBidang     = user.get(SharedPrefManager.SP_ID_BIDANG);

        simpleGrid      =   riwayatSieFrag.findViewById(R.id.simpleGridView);
        loadSieProgress =   riwayatSieFrag.findViewById(R.id.loadSie);
        return riwayatSieFrag;
    }

    private void getListStaff(){
        class ListStaff extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                collapse(loadSieProgress);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("listStaff");
                    if (jsonArray.length() == 0){
                        if(getActivity() != null) {
                            Toast.makeText(getActivity(), "Riwayat Kosong!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        showListStaff(s);
                    }
                } catch (JSONException a){
                    a.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), riwayatSieFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_STAFF, "?id_bidang=" + IdBidang);
                return s;
            }
        }
        ListStaff ge = new ListStaff();
        ge.execute();
    }

    private void showListStaff(String s){
        list_data = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("listStaff");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("no", json.getString("no"));
                    map.put("idMaster", json.getString("idMaster"));
                    map.put("imgProfile", json.getString("imgProfile"));
                    map.put("nameSie", json.getString("nameSie"));
                    list_data.add(map);
                    adapterGridSie = new AdapterGridSie(getActivity(), list_data);
                    simpleGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            // set an Intent to Another Activity
                            Intent intent = new Intent(getActivity(), DetailRiwayat.class);
                            intent.putExtra("idMaster", list_data.get(position).get("idMaster"));
                            intent.putExtra("nameSie", list_data.get(position).get("nameSie"));
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        simpleGrid.setAdapter(adapterGridSie);
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Sie Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), riwayatSieFrag,
                        "No Internet Connection!");
            }
        }
    }

//    private void showListStaff(String s) {
//        list_dataEmpty = new ArrayList<HashMap<String, String>>();
//        list_data = new ArrayList<HashMap<String, String>>();
//
//        try {
//            JSONObject jsonObject = new JSONObject(s);
//            JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat");
//            if (jsonArray.length() != 0) {
//                for (int a = 0; a < jsonArray.length(); a++) {
//                    JSONObject json = jsonArray.getJSONObject(a);
//                    HashMap<String, String> map = new HashMap<String, String>();
//                    map.put("no", json.getString("no"));
//                    map.put("sifat", json.getString("sifat"));
//                    map.put("isi_disposisi", json.getString("isi_disposisi"));
//                    map.put("tgl_disposisi", json.getString("tgl_disposisi"));
//                    map.put("tgl_disposisi_str", json.getString("tgl_disposisi_str"));
//                    map.put("status_baca", json.getString("status_baca"));
//                    map.put("status_str", json.getString("status_str"));
//                    map.put("id_master", json.getString("id_master"));
//                    map.put("isi_laporan", json.getString("isi_laporan"));
//                    map.put("files", json.getString("files"));
//                    map.put("nama", json.getString("nama"));
//                    list_data.add(map);
//                    adapter = new AdapterRiwayatSekdis(getActivity(), list_data);
//                    ((AdapterRiwayatSekdis) adapter).setOnItemClickListener(new AdapterRiwayatSekdis.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(View view, int position) {
//                            String status = list_data.get(position).get("status_str");
//                            String tujuan = list_data.get(position).get("nama");
//                            String tgl = list_data.get(position).get("tgl_disposisi");
//                            String isi = list_data.get(position).get("isi_laporan");
//                            String img = list_data.get(position).get("files");
//                            helpFile(status, tujuan, tgl, isi, img);
//                        }
//                    });
//                    if (getActivity() != null) {
//                        listView.setAdapter(adapter);
//                    }
//                }
//            } else {
//                HashMap<String, String> map = new HashMap<String, String>();
//                map.put("message", "Riwayat Kosong!");
//                map.put("isSelected", "false");
//                list_dataEmpty.add(map);
//                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
//                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        // Do Something
//                    }
//                });
//                if (getActivity()!=null){
//                    listView.setAdapter(adapterEmpty);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } catch (Exception e){
//            e.printStackTrace();
//            if(getActivity() != null) {
//                new CustomToast().Show_Toast(getActivity(), riwayatSieFrag,
//                        "No Internet Connection!");
//            }
//        }
//    }

    public void helpFile(String sender, String tujuan, String tgl, String isi, String img) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.cek_laporan, null);
        TextView statusL    = dialogView.findViewById(R.id.statusLapor);
        TextView tujuanL    = dialogView.findViewById(R.id.tujuanLapor);
        TextView tanggalL   = dialogView.findViewById(R.id.tanggalLapor);
        TextView isiL       = dialogView.findViewById(R.id.isiLapor);
        ImageView imgL      = dialogView.findViewById(R.id.fotoLaporan);

        statusL.setText(sender);
        tujuanL.setText(tujuan);
        tanggalL.setText(tgl);
        isiL.setText(isi);

        Glide.with(getActivity())
                .load(Config.URL_IMG_LAPORAN + img)
                .asBitmap()
                .into(imgL);
        alertDialogBuilder.setView(dialogView);
        alertDialogBuilder.setCancelable(true);
        AlertDialog builder = alertDialogBuilder.create();
        builder.show();
    }


    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }


    @Override
    public void onResume(){
        super.onResume();
        getListStaff();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }
}
