package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.AdapterNews;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeKepalaFrag extends Fragment{
    private static View homeFragment;
    private TextView name, bidang, stat, dtTextview, dis, titleAbsen, dateAbsenCv, msk, klr;
    private String getUsername, getSessionemail, getNamaBidang, getStatus, formattedDate, IdMaster, getTokenLogin, IdKantor;
    private ImageView icAbsen, icAbsen_Two;
    private TextView MessageAdmin;
    private Button buatKonsep;
    private FrameLayout CvAbsen;
    private ProgressBar dialogOne, dialogTwo, dialogThree, pbAbsen;

    private RecyclerView listView;
    private SharedPrefManager sharedPrefManager;
    private AdapterNews adapter;
    public HomeKepalaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeFragment = inflater.inflate(R.layout.fragment_home_kepala, container, false);

        //Call Defined Data
        sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionemail = user.get(SharedPrefManager.SP_EMAIL);
        getUsername = user.get(SharedPrefManager.SP_NAMA);
        getNamaBidang = user.get(SharedPrefManager.SP_NAMA_JABATAN);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        getTokenLogin   = user.get(SharedPrefManager.SP_TOKEN_LOGIN);
        String timeNow   = user.get(SharedPrefManager.SP_HOURS_OF_DAY);

        name        = homeFragment.findViewById(R.id.nameKepala);
        bidang      = homeFragment.findViewById(R.id.bidangKepala);
        msk         = homeFragment.findViewById(R.id.mskText);
        klr         = homeFragment.findViewById(R.id.klrText);
        dis         = homeFragment.findViewById(R.id.disposisiText);
        ImageView profile       = homeFragment.findViewById(R.id.profile_image);
        ImageView notif         = homeFragment.findViewById(R.id.notifActivity);
        TextView firstMeet      = homeFragment.findViewById(R.id.firstMeet);
        ScrollView homeKepala   = homeFragment.findViewById(R.id.bgHomeKepala);

        dialogOne  =   homeFragment.findViewById(R.id.loadingOne);
        dialogTwo  =   homeFragment.findViewById(R.id.loadingTwo);
        dialogThree  =   homeFragment.findViewById(R.id.loadingThree);

        //AbsenPreference
        titleAbsen  = homeFragment.findViewById(R.id.titleKehadiran);
        dateAbsenCv = homeFragment.findViewById(R.id.dateKehadiran);

        pbAbsen     = homeFragment.findViewById(R.id.progress_Absen);
        CvAbsen     = homeFragment.findViewById(R.id.bgAbsen);
        CvAbsen.setVisibility(View.GONE);
        klr.setVisibility(View.GONE);
        dis.setVisibility(View.GONE);
        msk.setVisibility(View.GONE);

        RelativeLayout layoutAlert =   homeFragment.findViewById(R.id.layoutAlert);
        MessageAdmin   =  homeFragment.findViewById(R.id.messageFromAdmin);

        //SomeData
        name.setText(getUsername);
        bidang.setText(getNamaBidang);
        firstMeet.setText(timeNow);

        if(timeNow.equals("Selamat Malam,")){
            homeKepala.setBackgroundResource(R.drawable.night_bg);
        }

        try {
            Ion.with(this).load(Config.URL_GET_MSG_ADMIN)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String message   = result.get("messageAdmin").getAsString();
                                MessageAdmin.setText(Html.fromHtml(message));
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }


        try {
            Ion.with(this).load(Config.URL_GET_PASSWORD_NOW + IdMaster)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String img   = result.get("imageProfile").getAsString();

                                if(img.equals("0")){
                                    Glide.with(getActivity())
                                            .load(Config.URL_PROFILE_KEPALA)
                                            .crossFade()
                                            .into(profile);
                                }else {
                                    Glide.with(getActivity()).load(Config.URL_IMG_PROFILE + img)
                                            .crossFade()
                                            .into(profile);
                                    profile.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.transparent));
                                }
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }

        profile.setOnClickListener(v ->{
            v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.image_clicked));
            Intent intent = new Intent(getActivity(), HomeSettings.class);
            startActivity(intent);
        });

        notif.setOnClickListener(v ->{
            v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.image_clicked));
            Intent intent = new Intent(getActivity(), NotifActivity.class);
            startActivity(intent);
        });

        listView = homeFragment.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.HORIZONTAL);
        listView.setLayoutManager(llm);

        return  homeFragment;
    }

    private void init(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                dialogOne.setVisibility(View.GONE);
                dialogTwo.setVisibility(View.GONE);
                dialogThree.setVisibility(View.GONE);
                pbAbsen.setVisibility(View.GONE);
                CvAbsen.setVisibility(View.VISIBLE);
                klr.setVisibility(View.VISIBLE);
                dis.setVisibility(View.VISIBLE);
                msk.setVisibility(View.VISIBLE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("statusAbsen");
                    JSONObject json = jsonArray.getJSONObject(0);

                    String stat_msk = json.getString("stat_msk");
                    String stat_klr = json.getString("stat_klr");
                    String dis_msk  = json.getString("dis_msk");
                    msk.setText(stat_msk);
                    klr.setText(stat_klr);
                    dis.setText(dis_msk);

                    String status       = json.getString("status");
                    String message      = json.getString("message");
                    String dateAbsen    = json.getString("dateAbsen");

                    if (status.equals("0")){
                        new CustomToast().Show_Toast(getActivity(), homeFragment,
                                "Mohon Login Ulang!");
                    }else{
                        if(status.equals("1")){
                            titleAbsen.setText(Html.fromHtml(message));
                            dateAbsenCv.setText(Html.fromHtml(dateAbsen));
                            CvAbsen.setBackgroundResource(R.drawable.btn_corner_bg);
                        }else{
                            titleAbsen.setText(Html.fromHtml(message));
                            dateAbsenCv.setText(Html.fromHtml(dateAbsen));
                            CvAbsen.setBackgroundResource(R.drawable.btn_corner_bg_red);
                        }
                    }

                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), homeFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_master", IdMaster);
                data.put("id_kantor", IdKantor);
                data.put("tokenLogin", getTokenLogin);

                return rh.sendPostRequest(Config.URL_CHECK_STATUS_KEPALA, data);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getNews(){
        class ListNews extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    showListNews(s);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), homeFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> user = sharedPrefManager.getSPUser();
                return user.get(SharedPrefManager.SP_LIST_NEWS);
            }
        }
        ListNews ge = new ListNews();
        ge.execute();
    }

    private void showListNews(String s) {
        ArrayList<HashMap<String, String>> list_data = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_news");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("idNews", json.getString("id_news"));
                    map.put("imgNews", json.getString("img_news"));
                    map.put("tagNews", json.getString("tag_news"));
                    map.put("titleNews", json.getString("title_news"));
                    map.put("descNews", json.getString("deskripsi_news"));
                    map.put("dateNews", json.getString("date_news"));
                    map.put("linkNews", json.getString("link_news"));
                    list_data.add(map);
                    adapter = new AdapterNews(getActivity(), list_data);
                    ((AdapterNews) adapter).setOnItemClickListener(new AdapterNews.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse(list_data.get(position).get("linkNews")));
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                if(getActivity() != null) {
                    new CustomToast().Show_Toast(getActivity(), homeFragment,
                            "Tidak Ada Berita!");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), homeFragment,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
        getNews();
        ((KepalaTikomdikActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((KepalaTikomdikActivity)getActivity()).getSupportActionBar().show();
    }
}
