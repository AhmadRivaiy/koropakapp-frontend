package com.disdik.koropakapp.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterInboxKepala;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.GetCountReadHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.TikomdikActivity.DetailInboxKepala;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxKepalaFrag extends Fragment {
    private View inboxFragment;
    private RecyclerView listView;
    private AdapterInboxKepala adapter;
    ArrayList<HashMap<String, String>> list_data;

    private EditText searchBtn;
    private ProgressBar progressBar;
    String IdKantor, IdMaster, MSG_MSK;
    public InboxKepalaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        inboxFragment = inflater.inflate(R.layout.fragment_inbox_kepala, container, false);
        searchBtn   =   inboxFragment.findViewById(R.id.searchBox);
        progressBar =   inboxFragment.findViewById(R.id.progressBarList);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        MSG_MSK = user.get(SharedPrefManager.SP_SRT_MSK);

        listView = inboxFragment.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        //getListMessage();
        return inboxFragment;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_message");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), inboxFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FILTER_INBOX_KEPALA, IdKantor + "&query=" + query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void getListMessage(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            //ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_message");
                    if (jsonArray.length() == 0){
                        if(getActivity() != null) {
                            Toast.makeText(getActivity(), "Surat Masuk Kosong!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        showListMessage(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), inboxFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_MESSAGE_KEPALA, IdKantor);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListMessage(String s) {

        list_data = new ArrayList<HashMap<String, String>>();
        Log.d("response ", s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_message");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_msk", json.getString("id_surat_msk"));
                    map.put("judul", json.getString("judul"));
                    map.put("isi_surat_msk", json.getString("isi_surat_msk"));
                    map.put("tgl_surat_msk", json.getString("tgl_surat_msk"));
                    map.put("status_read_kpl", json.getString("status_read_kpl"));
                    map.put("statDate", json.getString("statDate"));
                    list_data.add(map);
                    adapter = new AdapterInboxKepala(getActivity(), list_data);
                    ((AdapterInboxKepala) adapter).setOnItemClickListener(new AdapterInboxKepala.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailInboxKepala.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Pesan Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), inboxFragment,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getListMessage();
    }
}
