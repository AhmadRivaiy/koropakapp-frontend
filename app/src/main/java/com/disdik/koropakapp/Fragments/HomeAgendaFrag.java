package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.app.Fragment;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.AgendaActivity.DetailKartuMasuk;
import com.disdik.koropakapp.AgendaActivity.InputSuratAgenda;
import com.disdik.koropakapp.AgendaActivity.InputSuratKeluarAgenda;
import com.disdik.koropakapp.AgendaActivity.KartuSuratMasuk;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterKartu;
import com.disdik.koropakapp.Class.AdapterNews;
import com.disdik.koropakapp.Class.AdapterOutbox;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailSuratKlr;
import com.disdik.koropakapp.KonsepSurat;
import com.disdik.koropakapp.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeAgendaFrag extends Fragment {
    private View homeAgendaFrag;
    CardView inputSurat, inputSuratKeluar, kartuSurat;
    private FrameLayout  CvAbsen;

    String getSessionId, IdKantor, getTokenLogin, getUsername, getNamaBidang;
    TextView MessageAdmin, titleAbsen, dateAbsenCv, validasiSurat, disposisiDiteruskan, nameUser, bidangUser;
    private ProgressBar dialogOne, dialogTwo, dialogThree, pbAbsen;

    private RecyclerView listView;
    private SharedPrefManager sharedPrefManager;
    private AdapterNews adapter;
    public HomeAgendaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeAgendaFrag = inflater.inflate(R.layout.fragment_home_agenda, container, false);
        sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionId    = user.get(SharedPrefManager.SP_ID_JABATAN);
        IdKantor        = user.get(SharedPrefManager.SP_ID_KANTOR);
        getTokenLogin   = user.get(SharedPrefManager.SP_TOKEN_LOGIN);
        getUsername     = user.get(SharedPrefManager.SP_NAMA);
        getNamaBidang   = user.get(SharedPrefManager.SP_NAMA_JABATAN);
        String timeNow   = user.get(SharedPrefManager.SP_HOURS_OF_DAY);

        nameUser            = homeAgendaFrag.findViewById(R.id.nameUser);
        bidangUser          = homeAgendaFrag.findViewById(R.id.bidangUser);
        inputSurat          =   homeAgendaFrag.findViewById(R.id.addSuratMasuk);
        inputSuratKeluar    =   homeAgendaFrag.findViewById(R.id.addSuratKeluar);
        kartuSurat          =   homeAgendaFrag.findViewById(R.id.kartuSuratMasuk);
        validasiSurat       =   homeAgendaFrag.findViewById(R.id.validasiSurat);
        disposisiDiteruskan =   homeAgendaFrag.findViewById(R.id.disposisiDiterusakan);
        TextView firstMeet  =   homeAgendaFrag.findViewById(R.id.firstMeet);
        //loadKartuMsk        =   homeAgendaFrag.findViewById(R.id.loadKartuMsk);

        dialogOne  =   homeAgendaFrag.findViewById(R.id.loadingOne);
        dialogTwo  =   homeAgendaFrag.findViewById(R.id.loadingTwo);

        //AbsenPreference
        titleAbsen  = homeAgendaFrag.findViewById(R.id.titleKehadiran);
        dateAbsenCv = homeAgendaFrag.findViewById(R.id.dateKehadiran);

        pbAbsen     = homeAgendaFrag.findViewById(R.id.progress_Absen);
        CvAbsen     = homeAgendaFrag.findViewById(R.id.bgAbsen);
        CvAbsen.setVisibility(View.GONE);
        validasiSurat.setVisibility(View.GONE);
        disposisiDiteruskan.setVisibility(View.GONE);

        MessageAdmin   =  homeAgendaFrag.findViewById(R.id.messageFromAdmin);

        nameUser.setText(getUsername);
        bidangUser.setText(getNamaBidang);
        firstMeet.setText(timeNow);

        try {
            Ion.with(this).load(Config.URL_GET_MSG_ADMIN)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String message   = result.get("messageAdmin").getAsString();
                                MessageAdmin.setText(Html.fromHtml(message));
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }

        inputSurat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent  =   new Intent(getActivity(), InputSuratAgenda.class);
                startActivity(intent);
            }
        });

        inputSuratKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent   =   new Intent(getActivity(), KonsepSurat.class);
                startActivity(intent);
            }
        });

        kartuSurat.setOnClickListener(v ->{
            Intent intent   =   new Intent(getActivity(), KartuSuratMasuk.class);
            startActivity(intent);
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
        }


        listView = homeAgendaFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.HORIZONTAL);
        listView.setLayoutManager(llm);

        return homeAgendaFrag;
    }

    private void getStatus(){
        class statusData extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                pbAbsen.setVisibility(View.GONE);
                dialogOne.setVisibility(View.GONE);
                dialogTwo.setVisibility(View.GONE);
                CvAbsen.setVisibility(View.VISIBLE);
                validasiSurat.setVisibility(View.VISIBLE);
                disposisiDiteruskan.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("statusAbsen");
                    JSONObject json     = jsonArray.getJSONObject(0);
                    String status       = json.getString("status");
                    String stat_valid   = json.getString("stat_valid");
                    String stat_disp    = json.getString("stat_disp");
                    String message      = json.getString("message");
                    String dateAbsen    = json.getString("dateAbsen");

                    validasiSurat.setText(stat_valid);
                    disposisiDiteruskan.setText(stat_disp);

                    if (status.equals("0")){
                        new CustomToast().Show_Toast(getActivity(), homeAgendaFrag,
                                "Mohon Login Ulang!");
                    }else{
                        if(status.equals("1")){
                            titleAbsen.setText(Html.fromHtml(message));
                            dateAbsenCv.setText(Html.fromHtml(dateAbsen));
                            CvAbsen.setBackgroundResource(R.drawable.btn_corner_bg);
                        }else{
                            titleAbsen.setText(Html.fromHtml(message));
                            dateAbsenCv.setText(Html.fromHtml(dateAbsen));
                            CvAbsen.setBackgroundResource(R.drawable.btn_corner_bg_red);
                        }
                    }
                }catch (Exception a){
                    a.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), homeAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("tokenLogin", getTokenLogin);

                return rh.sendPostRequest(Config.URL_GET_STATUS_AGENDA, data);
            }
        }
        statusData ge = new statusData();
        ge.execute();
    }


    private void getNews(){
        class ListNews extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    showListNews(s);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), homeAgendaFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> user = sharedPrefManager.getSPUser();
                return user.get(SharedPrefManager.SP_LIST_NEWS);
            }
        }
        ListNews ge = new ListNews();
        ge.execute();
    }

    private void showListNews(String s) {
        ArrayList<HashMap<String, String>> list_data = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_news");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("idNews", json.getString("id_news"));
                    map.put("imgNews", json.getString("img_news"));
                    map.put("tagNews", json.getString("tag_news"));
                    map.put("titleNews", json.getString("title_news"));
                    map.put("descNews", json.getString("deskripsi_news"));
                    map.put("dateNews", json.getString("date_news"));
                    map.put("linkNews", json.getString("link_news"));
                    list_data.add(map);
                    adapter = new AdapterNews(getActivity(), list_data);
                    ((AdapterNews) adapter).setOnItemClickListener(new AdapterNews.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse(list_data.get(position).get("linkNews")));
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                if(getActivity() != null) {
                    new CustomToast().Show_Toast(getActivity(), homeAgendaFrag,
                            "Tidak Ada Berita!");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), homeAgendaFrag,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getStatus();
        getNews();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }
}
