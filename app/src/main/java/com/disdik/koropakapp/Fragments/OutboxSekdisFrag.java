package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterOutboxFinishSek;
import com.disdik.koropakapp.Class.AdapterOutboxKadis;
import com.disdik.koropakapp.Class.AdapterOutboxSekdis;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.PimpinanActivity.DetailOutboxKadis;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.DetailOutboxSekdis;
import com.disdik.koropakapp.VerifyActivity;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutboxSekdisFrag extends Fragment {
    private View outboxSekdisFrag;

    private RecyclerView listView, listViewFinish;
    private EditText searchBtn;
    private ProgressBar progressBar;
    private CardView valid, noValid;
    private ImageView icKlr, icFinish;
    private Button cbSekdis;
    private String IdKantor, nameV, IdMaster, IdBidang;
    private FrameLayout titleSurat, suratLayout, finisLayout, titleFinish;

    private AdapterOutboxSekdis adapter;
    private AdapterOutboxFinishSek adapterFinish;
    private AdapterEmpty adapterEmpty;
    private ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;
    private ArrayList<HashMap<String, String>> list_dataFinish;
    private ArrayList<HashMap<String, String>> list_dataEmptyClone;
    private ArrayList<String> valueSrt = new ArrayList<String>();
    private ArrayList<String> nameSrt = new ArrayList<String>();

    private int stateLaySur = 1;
    private int stateLayFin = 1;
    private int stateEmptyList = 0;
    private String CondFilter = "list_keluar", CondSrt = "1";

    public OutboxSekdisFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        outboxSekdisFrag = inflater.inflate(R.layout.fragment_outbox_sekdis, container, false);
        searchBtn   =   outboxSekdisFrag.findViewById(R.id.searchBox);
        progressBar =   outboxSekdisFrag.findViewById(R.id.progressBarList);
        valid       =   outboxSekdisFrag.findViewById(R.id.validCard);
        noValid     =   outboxSekdisFrag.findViewById(R.id.noValidCard);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);

        listView = outboxSekdisFrag.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
//                if(!editable.toString().equals("")) {
//                    getFilter(editable.toString());
//                    Log.e("Searched", editable.toString());
//                }
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());
            }
        });

        //TODO: Collapse Expand Navbar
        valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
        valid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            noValid.setCardBackgroundColor(Color.WHITE);
            CondSrt = "1";

            CondFilter = "list_keluar_finish";
            getListMessageKlr();
        });

        noValid.setOnClickListener(v -> {
            valid.setCardBackgroundColor(Color.WHITE);
            noValid.setCardBackgroundColor(Color.parseColor("#78c7f8"));
            CondSrt = "0";

            CondFilter = "list_keluar";
            getListNoParaf();
            //Toast.makeText(getActivity(), "No Valid", Toast.LENGTH_SHORT).show();
        });

        return outboxSekdisFrag;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    showListMessageKlr(s, CondSrt);
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSekdisFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);
                data.put("id_master", IdMaster);
                data.put("query", query);

                return rh.sendPostRequest(Config.URL_FILTER_OUTBOX_SEKDIS, data);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getListMessageKlr(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.d("response ", " " + s);
                    showListMessageKlr(s, "list_keluar_finish");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSekdisFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);
                data.put("id_master", IdMaster);

                return rh.sendPostRequest(Config.URL_GET_MESSAGE_SEKDIS_OUT, data);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getListNoParaf(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    Log.d("response ", " " + s);
                    showListMessageKlr(s, "list_keluar");
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), outboxSekdisFrag,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdKantor);
                data.put("id_bidang", IdBidang);
                data.put("id_master", IdMaster);

                return rh.sendPostRequest(Config.URL_GET_MESSAGE_SEKDIS_OUT, data);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListMessageKlr(String s, String Condd){
        list_data = new ArrayList<HashMap<String, String>>();
        list_dataEmpty = new ArrayList<HashMap<String, String>>();

        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray(Condd);
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_klr", json.getString("id_surat_klr"));
                    map.put("no_surat_klr", json.getString("no_surat_klr"));
                    map.put("no_agenda_klr", json.getString("no_agenda_klr"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("status_read_sekdis", json.getString("status_read_sekdis"));
                    map.put("tujuan_surat_klr", json.getString("tujuan_surat_klr"));
                    map.put("statDate", json.getString("statDate"));
                    map.put("isSelected", json.getString("isSelected"));

                    list_data.add(map);
                    adapter = new AdapterOutboxSekdis(getActivity(), list_data);
                    ((AdapterOutboxSekdis) adapter).setOnItemClickListener(new AdapterOutboxSekdis.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailOutboxSekdis.class);
                            HashMap<String,String> map = (HashMap)list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_KLR).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity()!=null){
                        listView.setAdapter(adapter);
                    }
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Surat Keluar Kosong!");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(getActivity(), list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                if (getActivity()!=null){
                    listView.setAdapter(adapterEmpty);
                }
                stateEmptyList = 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), outboxSekdisFrag,
                        "No Internet Connection!");
            }
        }
    }

//    private void PrintTtd() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//        LayoutInflater inflater = getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.modal_print_ttd, null);
//        ListView nameValue  = dialogView.findViewById(R.id.lv);
//        TextView alertEmpty = dialogView.findViewById(R.id.alertEmpty);
//
//        ArrayAdapter<String> adapterSrt = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, nameSrt);
//        // DataBind ListView with items from ArrayAdapter
//        nameValue.setAdapter(adapterSrt);
//        adapterSrt.notifyDataSetChanged();
//        alertEmpty.setVisibility(View.GONE);
//
//        if((valueSrt.size() == 0) || (stateEmptyList == 1)){
//            alertDialogBuilder
//                    .setView(dialogView)
//                    .setCancelable(false)
//                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                            valueSrt.clear();
//                            nameSrt.clear();
//                        }
//                    });
//            nameSrt.add("Silahkan Pilih Surat Anda");
//            nameValue.setVisibility(View.GONE);
//            alertEmpty.setVisibility(View.VISIBLE);
//        }else {
//            alertDialogBuilder
//                    .setView(dialogView)
//                    .setCancelable(false)
//                    .setPositiveButton("Tanda Tangan", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            String selectClause = StringUtils.join(valueSrt.toArray(), ',' );
////                            Intent intent = new Intent(getActivity(), VerifyActivity.class);
////                            intent.putExtra("valueSrt", selectClause);
////                            intent.putExtra("IdMaster", IdMaster);
////                            startActivity(intent);
//                            if(getActivity() != null) {
//                                new CustomToast().Show_Toast(getActivity(), outboxSekdisFrag,
//                                        "Coming Soon !");
//                            }
//                            valueSrt.clear();
//                            nameSrt.clear();
//                            dialog.dismiss();
//                        }
//                    })
//                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                            valueSrt.clear();
//                            nameSrt.clear();
//                        }
//                    });
//        }
//        // set pesan dari dialog
//
//
//        // membuat alert dialog dari builder
//        AlertDialog alertDialog = alertDialogBuilder.create();
//
//        // menampilkan alert dialog
//        alertDialog.show();
//    }
//
//    public static void expand(final View view) {
//
//        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        final int targetHeight = view.getMeasuredHeight();
//
//        // Set initial height to 0 and show the view
//        view.getLayoutParams().height = 0;
//        view.setVisibility(View.VISIBLE);
//
//        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
//        anim.setInterpolator(new AccelerateInterpolator());
//        anim.setDuration(500);
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
//                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
//                view.setLayoutParams(layoutParams);
//            }
//        });
//        anim.addListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                // At the end of animation, set the height to wrap content
//                // This fix is for long views that are not shown on screen
//                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
//                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
//            }
//        });
//        anim.start();
//    }
//
//    public static void collapse(final View v) {
//        final int initialHeight = v.getMeasuredHeight();
//
//        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
//        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            public void onAnimationUpdate(ValueAnimator animation) {
//                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
//                v.requestLayout();
//            }
//        });
//        va.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                v.setVisibility(View.GONE);
//            }
//
//            @Override public void onAnimationStart(Animator animation) {}
//            @Override public void onAnimationCancel(Animator animation) {}
//            @Override public void onAnimationRepeat(Animator animation) {}
//        });
//        va.setDuration(300);
//        va.setInterpolator(new DecelerateInterpolator());
//        va.start();
//    }

    @Override
    public void onResume(){
        super.onResume();
        if(CondSrt.equals("1")){
            getListMessageKlr();
        }else{
            getListNoParaf();
        }
    }
}
