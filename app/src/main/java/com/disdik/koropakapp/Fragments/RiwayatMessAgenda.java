package com.disdik.koropakapp.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.disdik.koropakapp.Class.AdapterInbox;
import com.disdik.koropakapp.Class.AdapterRiwAgenda;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailMessage;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiwayatMessAgenda extends Fragment {
    private View riwayatMessAgenda;
    private RecyclerView listView;
    SharedPrefManager sharedPrefManager;

    private EditText searchBtn;
    private ProgressBar progressBar;

    private AdapterRiwAgenda adapter;
    String getSessionId, IdBidang;
    ArrayList<HashMap<String, String>> list_data;
    public RiwayatMessAgenda() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        riwayatMessAgenda = inflater.inflate(R.layout.fragment_riwayat_mess_agenda, container, false);
        searchBtn   =   riwayatMessAgenda.findViewById(R.id.searchBox);
        progressBar =   riwayatMessAgenda.findViewById(R.id.progressBarList);

        listView = riwayatMessAgenda.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getSessionId    = user.get(SharedPrefManager.SP_ID_JABATAN);
        IdBidang        = user.get(SharedPrefManager.SP_ID_KANTOR);


        searchBtn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        return riwayatMessAgenda;
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Surat Masuk Kosong", Toast.LENGTH_SHORT).show();
                    }else {
                        showListRiwayat(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), riwayatMessAgenda,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_FILTER_RIWAYAT_MESSAGE, IdBidang + "&query=" + query);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getListRiwayat(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Fetching...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat");
                    if (jsonArray.length() == 0){
                        Toast.makeText(getActivity(), "Surat Masuk Kosong", Toast.LENGTH_SHORT).show();
                    }else {
                        showListRiwayat(s);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), riwayatMessAgenda,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_RIWAYAT_MESSAGE, IdBidang);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListRiwayat(String s) {

        list_data = new ArrayList<HashMap<String, String>>();
        try {
            Log.d("response ", s);
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_riwayat");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id_surat_msk", json.getString("id_surat_msk"));
                    map.put("no_surat_msk", json.getString("no_surat_msk"));
                    map.put("tgl_upload", json.getString("tgl_upload"));
                    map.put("r_num", json.getString("r_num"));
                    list_data.add(map);
                    adapter = new AdapterRiwAgenda(getActivity(), list_data);
                    ((AdapterRiwAgenda) adapter).setOnItemClickListener(new AdapterRiwAgenda.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(getActivity(), DetailMessage.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                            intent.putExtra(Config.EMP_ID, empId);
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        listView.setAdapter(adapter);
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Pesan Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), riwayatMessAgenda,
                        "No Internet Connection!");
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        getListRiwayat();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home) {
            getActivity().finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
