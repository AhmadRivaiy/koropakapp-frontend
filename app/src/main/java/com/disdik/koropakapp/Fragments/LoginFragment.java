package com.disdik.koropakapp.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import com.disdik.koropakapp.AgendaActivity.HomeAgenda;
import com.disdik.koropakapp.AgendaActivity.InputSuratAgenda;
import com.disdik.koropakapp.BidangActivity.BidangActivity;
import com.disdik.koropakapp.BuildConfig;
import com.disdik.koropakapp.CadinActivity.HomeCadinActivity;
import com.disdik.koropakapp.Class.AESHelper;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Class.Utils;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.GTKActivity.HomeGTKActivity;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.MaintenainceActivity;
import com.disdik.koropakapp.PimpinanActivity.PimpinanActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.SekdisActivity;
import com.disdik.koropakapp.SieActivity.HomeSieActivity;
import com.disdik.koropakapp.SplashScreen;
import com.disdik.koropakapp.StaffActivity.HomeStaffActivity;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.disdik.koropakapp.UpdateActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private View view;

    private EditText emailid, password;
    private Button loginButton;
    private CheckBox show_hide_password;
    private LinearLayout loginLayout;
    private Animation shakeAnimation;
    private FragmentManager fragmentManager;

    private String level = "0", id_jabatan;
    private String encryptedPass = "";
    private String getEm, getPass, id_bidang, id_kantor, nama_jabatan, id_master, token, nama;
    private SharedPrefManager sharedPrefManager;

    public LoginFragment() {
        // Required empty public constructor
    }


    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_layout, container, false);
        initViews();
        setListeners();
        sharedPrefManager = new SharedPrefManager(getActivity());

        RequestHandler requestHandler = new RequestHandler();
        String timeNow = requestHandler.getStringFromMilli();
        sharedPrefManager.saveSPString(SharedPrefManager.SP_HOURS_OF_DAY, timeNow);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Login ", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        Log.d("Login ", token);

                    }
                });

        return view;
    }

    // Initiate Views
    private void initViews() {
        fragmentManager = getActivity().getSupportFragmentManager();

        emailid             = view.findViewById(R.id.login_emailid);
        password            = view.findViewById(R.id.login_password);
        loginButton         = view.findViewById(R.id.loginBtn);
        show_hide_password  = view.findViewById(R.id.show_hide_password);
        loginLayout         = view.findViewById(R.id.login_layout);

        // Load ShakeAnimation
        shakeAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.shake_anim);

        // Setting text selector over textviews
        @SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.textview_selector);
        try {
            ColorStateList csl = ColorStateList.createFromXml(getResources(),
                    xrp);
            show_hide_password.setTextColor(csl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Set Listeners
    private void setListeners() {
        loginButton.setOnClickListener(this);

        // Set check listener over checkbox for showing and hiding password
        show_hide_password
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton button,
                                                 boolean isChecked) {

                        // If it is checkec then show password else hide
                        // password
                        if (isChecked) {

                            show_hide_password.setText(R.string.hide_pwd);// change
                            // checkbox
                            // text

                            password.setInputType(InputType.TYPE_CLASS_TEXT);
                            password.setTransformationMethod(HideReturnsTransformationMethod
                                    .getInstance());// show password
                        } else {
                            show_hide_password.setText(R.string.show_pwd);// change
                            // checkbox
                            // text

                            password.setInputType(InputType.TYPE_CLASS_TEXT
                                    | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            password.setTransformationMethod(PasswordTransformationMethod
                                    .getInstance());// hide password

                        }

                    }
                });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.loginBtn) {
            checkValidation();
        }
    }

    // Check Validation before login
    private void checkValidation() {
        // Get email id and password
        getEm = emailid.getText().toString();
        getPass = password.getText().toString();

        // Check patter for email id
        Pattern p = Pattern.compile(Utils.regEx);

        Matcher m = p.matcher(getEm);

        // Check for both field is empty or not
        if (getEm.equals("") || getEm.length() == 0
                || getPass.equals("") || getPass.length() == 0) {
            loginLayout.startAnimation(shakeAnimation);
            new CustomToast().Show_Toast(getActivity(), view,
                    "Mohon Di Isi");

        }
        // Check if email id is valid or not
        else {
           cekLogin();
        }
    }

    private void cekLogin(){
        class CekLogin extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(getActivity(), "Check User", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                String Newversion = "";
                String versionCode = BuildConfig.VERSION_NAME;
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("statusLogin");
                    JSONObject json     = jsonArray.getJSONObject(0);
                    String status       = json.getString("status");
                    String waktumt      = json.getString("waktu_mt");
                    String messagemt    = json.getString("message_mt");
                    String status_mt    = json.getString("status_mt");
                    Newversion          = json.getString("new_version");

                    if (status.equals("false")){
                        loginLayout.startAnimation(shakeAnimation);
                        new CustomToast().Show_Toast(getActivity(), view,
                                "Wrong Email or Password!");
                    }else {
                        if(status_mt.equals("0")){
                            if(versionCode.equals(Newversion)){
                                goToLogin(s);
                            }else{
                                Intent intent = new Intent(getActivity(), UpdateActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }else {
                            Intent intent = new Intent(getActivity(), MaintenainceActivity.class);
                            intent.putExtra("waktu", waktumt);
                            intent.putExtra("message", messagemt);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), loginLayout,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String url = (Config.URL_GET_USER);

                HashMap<String,String> data = new HashMap<>();
                data.put("email", getEm);
                data.put("password", getPass);
                data.put("token", token);

                String result = rh.sendPostRequest(url, data);
                return result;
            }
        }
        CekLogin ui = new CekLogin();
        ui.execute();
    }

    private void goToLogin(String s){
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("statusLogin");
            JSONObject json = jsonArray.getJSONObject(0);
            level       = json.getString("level");
            id_jabatan  = json.getString("id_jabatan");
            nama        = json.getString("nama");
            id_bidang   = json.getString("id_bidang");
            id_kantor   = json.getString("id_kantor");
            nama_jabatan= json.getString("nama_jabatan");
            String namaKantor= json.getString("nama_lvl_kantor");
            String namaBidang= json.getString("nama_bidang");
            String nNip      = json.getString("nip");
            id_master        = json.getString("id_master");
            String TokenLogin   = json.getString("tokenLogin");

            encryptedPass = AESHelper.encrypt(getPass);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_EMAIL, getEm);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_PASSWORD, encryptedPass);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA, nama);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_LEVEL, level);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_ID_JABATAN, id_jabatan);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_ID_BIDANG, id_bidang);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA_JABATAN, nama_jabatan);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_ID_MASTER, id_master);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_ID_KANTOR, id_kantor);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA_KANTOR, namaKantor);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_NAMA_BIDANG, namaBidang);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_NIP, nNip);
            sharedPrefManager.saveSPString(SharedPrefManager.SP_TOKEN_LOGIN, TokenLogin);
            sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);

            Log.e("TEST", "encrypted:" + encryptedPass);
            //Check Who Login to This Application
            CheckSession();
        } catch (Exception a){
            a.printStackTrace();
        }
    }

    private void CheckSession(){
        if(id_jabatan.equals("07") && level.equals("03")){//FCM
            FirebaseMessaging.getInstance().subscribeToTopic("gtk")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            String msg = "Succes!";
                            if (!task.isSuccessful()) {
                                msg = "Failed!";
                            }
                            Log.e("FCM Agenda", msg);
                        }
                    });
            Intent agenda = new Intent(getActivity(), HomeGTKActivity.class);
            startActivity(agenda);
            getActivity().finish();
        }else {
            switch (level) {
                case "99":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("agenda")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Agenda", msg);
                                }
                            });
                    Intent agenda = new Intent(getActivity(), HomeAgenda.class);
                    startActivity(agenda);
                    getActivity().finish();
                    break;
                case "00":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("staff")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Staff", msg);
                                }
                            });
                    Intent staff = new Intent(getActivity(), HomeStaffActivity.class);
                    startActivity(staff);
                    getActivity().finish();
                    break;
                case "01":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("kadis")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Kadis", msg);
                                }
                            });
                    Intent kadis = new Intent(getActivity(), PimpinanActivity.class);
                    startActivity(kadis);
                    getActivity().finish();
                    break;
                case "02":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("sekdis")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Sekdis", msg);
                                }
                            });
                    Intent sekdis = new Intent(getActivity(), SekdisActivity.class);
                    startActivity(sekdis);
                    getActivity().finish();
                    break;
                case "03":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("bidang")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Bidang", msg);
                                }
                            });
                    Intent bidang = new Intent(getActivity(), BidangActivity.class);
                    startActivity(bidang);
                    getActivity().finish();
                    break;
                case "04":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("sie")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM Sie", msg);
                                }
                            });
                    Intent sie = new Intent(getActivity(), HomeSieActivity.class);
                    startActivity(sie);
                    getActivity().finish();
                    break;
                case "05":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("kepalaTikomdik")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM KepalaTikomdik", msg);
                                }
                            });
                    Intent kepala = new Intent(getActivity(), KepalaTikomdikActivity.class);
                    startActivity(kepala);
                    getActivity().finish();
                    break;
                case "06":
                    //FCM
                    FirebaseMessaging.getInstance().subscribeToTopic("cadin")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    String msg = "Succes!";
                                    if (!task.isSuccessful()) {
                                        msg = "Failed!";
                                    }
                                    Log.e("FCM KepalaTikomdik", msg);
                                }
                            });
                    Intent cadin = new Intent(getActivity(), HomeCadinActivity.class);
                    startActivity(cadin);
                    getActivity().finish();
                    break;
                default:
                    new CustomToast().Show_Toast(getActivity(), view,
                            "User Not Found, Login Again!");
            }
        }
    }

    private void inputToken(String idMaster, String idJabatan, String idBidang, String level, String token, String id_kantor){
        Ion.with(getActivity()).load(Config.URL_INPUT_TOKEN + idMaster + "&id_jabatan=" + idJabatan + "&id_bidang=" + idBidang + "&hak_akses=" + level + "&token=" + token + "&id_kantor=" + id_kantor)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try{
                            String status = result.get("status").getAsString();
                        }
                        catch (Exception a){
                            new CustomToast().Show_Toast(getActivity(), view,
                                    "Server Maintenance!");
                        }
                    }
                });
    }
}
