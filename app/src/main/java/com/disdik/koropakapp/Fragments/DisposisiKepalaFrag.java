package com.disdik.koropakapp.Fragments;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.AdapterDisposisiKepala;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterGridSie;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.DetailRiwayat;
import com.disdik.koropakapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DisposisiKepalaFrag extends Fragment {
    private static View disposisiFragment;
    private GridView simpleGrid;
    private AdapterGridSie adapterGridSie;
    private ProgressBar loadSieProgress;

    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    private String getId, getUsername, token;
    public DisposisiKepalaFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        disposisiFragment =  inflater.inflate(R.layout.fragment_disposisi_kepala, container, false);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(getActivity());
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getId       = user.get(SharedPrefManager.SP_ID_MASTER);
        getUsername = user.get(SharedPrefManager.SP_NAMA);
        token       = user.get(SharedPrefManager.SP_TOKEN_LOGIN);

        simpleGrid  =   disposisiFragment.findViewById(R.id.simpleGridView);
        loadSieProgress =   disposisiFragment.findViewById(R.id.loadSie);
        return disposisiFragment;
    }

    private void getListSie(){
        class ListSie extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                collapse(loadSieProgress);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_sie");
                    if (jsonArray.length() == 0){
                        if(getActivity() != null) {
                            Toast.makeText(getActivity(), "Riwayat Kosong!", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        showListSie(s);
                    }
                } catch (JSONException a){
                    a.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                    if(getActivity() != null) {
                        new CustomToast().Show_Toast(getActivity(), disposisiFragment,
                                "No Internet Connection!");
                    }
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_GET_SIE, "?token=" + token);
                return s;
            }
        }
        ListSie ge = new ListSie();
        ge.execute();
    }

    private void showListSie(String s){
        list_data = new ArrayList<HashMap<String, String>>();
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_sie");
            if (jsonArray.length() != 0) {
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("no", json.getString("no"));
                    map.put("idMaster", json.getString("idMaster"));
                    map.put("imgProfile", json.getString("imgProfile"));
                    map.put("nameSie", json.getString("nameSie"));
                    list_data.add(map);
                    adapterGridSie = new AdapterGridSie(getActivity(), list_data);
                    simpleGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            // set an Intent to Another Activity
                            Intent intent = new Intent(getActivity(), DetailRiwayat.class);
                            intent.putExtra("idMaster", list_data.get(position).get("idMaster"));
                            intent.putExtra("nameSie", list_data.get(position).get("nameSie"));
                            startActivity(intent);
                        }
                    });
                    if (getActivity() != null) {
                        simpleGrid.setAdapter(adapterGridSie);
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Sie Kosong!", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            if(getActivity() != null) {
                new CustomToast().Show_Toast(getActivity(), disposisiFragment,
                        "No Internet Connection!");
            }
        }
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        ValueAnimator va = ValueAnimator.ofInt(initialHeight, 0);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (Integer) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        va.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override public void onAnimationStart(Animator animation) {}
            @Override public void onAnimationCancel(Animator animation) {}
            @Override public void onAnimationRepeat(Animator animation) {}
        });
        va.setDuration(300);
        va.setInterpolator(new DecelerateInterpolator());
        va.start();
    }

    @Override
    public void onResume(){
        super.onResume();
        getListSie();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }
}
