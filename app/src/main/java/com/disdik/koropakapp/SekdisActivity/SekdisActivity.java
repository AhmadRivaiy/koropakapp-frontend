package com.disdik.koropakapp.SekdisActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiKadisFrag;
import com.disdik.koropakapp.Fragments.DisposisiSekdisFrag;
import com.disdik.koropakapp.Fragments.HomeKadisFrag;
import com.disdik.koropakapp.Fragments.HomeSekdisFrag;
import com.disdik.koropakapp.Fragments.InboxKadisFrag;
import com.disdik.koropakapp.Fragments.InboxSekdisFrag;
import com.disdik.koropakapp.Fragments.OutboxKadisFrag;
import com.disdik.koropakapp.Fragments.OutboxSekdisFrag;
import com.disdik.koropakapp.Fragments.RiwayatDisposisiSekdis;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.PimpinanActivity.PimpinanActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.disdik.koropakapp.SieActivity.HomeSieActivity;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SekdisActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    SharedPrefManager sharedPrefManager;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekdis);
        sharedPrefManager = new SharedPrefManager(this);
        myDb = new DatabaseHelper(this);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        //Default ProgramFragment
        mToolbar.setTitle("Sekertaris Dinas");
        loadFragment(new HomeSekdisFrag());
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view_sekdis);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container_sekdis, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_notif:
                //Toast.makeText(this, "Notif", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SekdisActivity.this, NotifActivity.class);
                startActivity(intent);
                break;
            case R.id.feedback:
                Intent feedback = new Intent(SekdisActivity.this, FeedBackActivity.class);
                startActivity(feedback);
                break;
            case R.id.logout:
                showDialog();
                break;
            case R.id.settings:
                Intent settings = new Intent(SekdisActivity.this, HomeSettings.class);
                startActivity(settings);
                break;
        }
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem settingsItem = menu.findItem(R.id.action_notif);
        // set your desired icon here based on a flag if you like
        Cursor stat = myDb.cekCountNotif();
        StringBuffer buffer = new StringBuffer();
        while (stat.moveToNext() ) {
            buffer.append(stat.getString(0));
        }
        int a = Integer.parseInt(buffer.toString());
        if(a > 0){
            settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notification_on));
        }else {
            settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_ring));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.nvg_srt_masuk:
                fragment = new InboxSekdisFrag();
                mToolbar.setTitle("Surat Masuk");
                break;
            case R.id.nvg_srt_keluar:
                fragment = new OutboxSekdisFrag();
                mToolbar.setTitle("Surat Keluar");
                break;
            case R.id.navigation_home:
                fragment = new HomeSekdisFrag();
                mToolbar.setTitle("Home");
                break;
            case R.id.nvg_disposisi:
                fragment = new DisposisiSekdisFrag();
                mToolbar.setTitle("Disposisi");
                break;
            case R.id.riwayat_disposisi:
                fragment = new RiwayatDisposisiSekdis();
                mToolbar.setTitle("Riwayat Disposisi");
                break;
        }
        return loadFragment(fragment);
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder.setTitle("Keluar dari aplikasi?");
        alertDialogBuilder
                .setMessage("Tap Ya untuk keluar!")
                .setIcon(R.mipmap.ic_app_koropak_round)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //Delete SQLIte
                        //myDb.deleteDataNotif();
                        // jika tombol diklik, maka akan menutup activity ini
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        startActivity(new Intent(SekdisActivity.this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
