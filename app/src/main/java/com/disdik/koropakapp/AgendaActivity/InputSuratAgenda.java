package com.disdik.koropakapp.AgendaActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class InputSuratAgenda extends AppCompatActivity {
    private SimpleDateFormat dateFormat;
    private DatePickerDialog datePickerDialog;
    private DatabaseHelper myDb;

    private static final String TAG = "ISuratMasuk";
    private String IdBidang;
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    private EditText noSurat, perihalSurat, asalSurat, ketSurat, dateSurat, indexSurat, kodeSurat, noUrutSurat, lampiranSurat, pengolahSurat, catatanSurat, dateForwardMsg, tandaTerimaSurat;
    private TextView intFile;
    private Bitmap bitmap;
    private ImageView imageView;
    private Uri uri;
    List<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_surat_agenda);
        dateFormat   = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        //TODO:Shared
        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdBidang = user.get(SharedPrefManager.SP_ID_KANTOR);

        //ToolBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Input Surat Masuk");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDb.deleteData();
                onBackPressed(); // Implemented by activity
            }
        });

        //TODO:getEditText
        indexSurat      =   findViewById(R.id.indeksSurat);
        kodeSurat       =   findViewById(R.id.kodeSurat);
        noUrutSurat     =   findViewById(R.id.noUrutSurat);

        perihalSurat    =   findViewById(R.id.perihalSurat);
        ketSurat        =   findViewById(R.id.keteranganSurat);

        asalSurat       =   findViewById(R.id.asalSurat);
        dateSurat       =   findViewById(R.id.dateInputMsg);
        noSurat         =   findViewById(R.id.noSurat);

        lampiranSurat   =   findViewById(R.id.lampiranSurat);
        pengolahSurat   =   findViewById(R.id.pengolahSurat);
        dateForwardMsg  =   findViewById(R.id.dateForwardMsg);

        tandaTerimaSurat=   findViewById(R.id.tandaTerimaSurat);
        catatanSurat    =   findViewById(R.id.catatanSurat);

        imageView       =   findViewById(R.id.showResult);
        intFile         =   findViewById(R.id.intFile);

        myDb = new DatabaseHelper(this);
        myDb.deleteData();
    }

    //TODO:GetDateMessage
    public void dtSurat(View view) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateSurat.setText(dateFormat.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    //TODO:GetDateForwardMessage
    public void fwSurat(View view) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateForwardMsg.setText(dateFormat.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    //TODO:Requesting permission
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Akses Kamera Tidak Diberikan. Mohon Diberi Akses!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                showImagePickerOptions();
                break;
        }
    }

    //TODO:Choose Option Take Image
    public void showImagePickerOptions() {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(InputSuratAgenda.this);
        builder.setTitle(R.string.ambil_foto_doc);

        // add a list
        String[] item = {getString(R.string.buka_kamera), getString(R.string.ambil_dari_gallery), getString(R.string.batal)};
        builder.setItems(item, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        showTakeCamera();
                        break;
                    case 1:
                        onChooseGallerySelected();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    //TODO:GetScannedImage
    private void showTakeCamera() {
        int REQUEST_CODE = 99;
        int preference = ScanConstants.OPEN_CAMERA;
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE);
    }

    private void onChooseGallerySelected() {
        int REQUEST_CODE = 99;
        int preference = ScanConstants.OPEN_MEDIA;
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                imageView.setImageBitmap(bitmap);
                String nameFile = getFileName(uri);
                AddData(nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //TODO:SetNameImage
    public String getStringImage(Bitmap bmp){
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp = Bitmap.createScaledBitmap(bmp, 1240, 1754, true);
        bmp.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public String getFileName(Uri uri){
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    //TODO:SendScannedImage
    public void AddData(String name) {
        boolean isInserted = myDb.insertData(name);
        Cursor stat = myDb.cekStat();
        StringBuffer buffer = new StringBuffer();
        while (stat.moveToNext() ) {
            buffer.append(stat.getString(0));
        }
        intFile.setText(buffer.toString());
        if(isInserted) {
            Toast.makeText(InputSuratAgenda.this, "Ok!", Toast.LENGTH_LONG).show();
            class UploadImage extends AsyncTask<Bitmap,Void,String> {
                ProgressDialog loading;
                RequestHandler rh = new RequestHandler();

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(InputSuratAgenda.this, "Uploading Image", "Please wait...",true,true);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    if(s.equals("")){
                        Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                protected String doInBackground(Bitmap... params) {
                    Bitmap bitmap = params[0];
                    String url = (Config.URL_UPLOAD_IMG);

                    String uploadImage = getStringImage(bitmap);
                    HashMap<String,String> data = new HashMap<>();
                    data.put("image", uploadImage);
                    data.put("name", getFileName(uri));

                    String result = rh.sendPostRequest(url, data);
                    return result;

                }

            }

            UploadImage ui = new UploadImage();
            ui.execute(bitmap);
        }
    }

    //TODO:SendData
    private void uploadImage(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(InputSuratAgenda.this, "Memproses...", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                    Log.e("Input Err", " " + s);
                }else{
                    Intent intent = new Intent(InputSuratAgenda.this, HomeAgenda.class);
                    startActivity(intent);
                    finish();
                    Log.e("Input Err", " " + s);
                    Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String url = (Config.URL_UPLOAD_AGENDA);
                Cursor res = myDb.getAllData();

                ArrayList<String> name = new ArrayList<String>();
                while (res.moveToNext() ) {
                    name.add(res.getString(1) + ".jpg");
                }
                String selectClause = StringUtils.join(name.toArray(), ',' );
                String uploadImage = getStringImage(bitmap);

                HashMap<String,String> data = new HashMap<>();
                //data.put("image", uploadImage);
                data.put("name", selectClause);

                data.put("index_surat_msk", indexSurat.getText().toString());
                data.put("kode_surat_msk", kodeSurat.getText().toString());
                data.put("no_urut_surat_msk", noUrutSurat.getText().toString());

                data.put("perihal_surat_msk", perihalSurat.getText().toString());
                data.put("asal_surat_msk", asalSurat.getText().toString());
                data.put("tgl_surat_msk", dateSurat.getText().toString());

                data.put("no_surat_msk", noSurat.getText().toString());
                data.put("lampiran_surat_msk", lampiranSurat.getText().toString());
                data.put("pengolah_surat_msk", pengolahSurat.getText().toString());

                data.put("tgl_diteruskan", dateForwardMsg.getText().toString());
                data.put("tanda_terima", tandaTerimaSurat.getText().toString());
                data.put("catatan_surat_msk", catatanSurat.getText().toString());

                data.put("id_kantor", IdBidang);
                data.put("keterangan_msk", ketSurat.getText().toString());

                return rh.sendPostRequest(url, data);
            }

        }

        UploadImage ui = new UploadImage();
        ui.execute(bitmap);

    }

    @Override
    public void onBackPressed() {
        myDb.deleteData();
        finish();
    }

    public void scanBtn(View view) {
        checkPermissions();
    }

    public void onSubmit(View view) {
        if(uri!=null){
            if((hasContent(indexSurat)) &&
                    (hasContent(kodeSurat)) &&
                        (hasContent(noUrutSurat)) &&
                            (hasContent(perihalSurat)) &&
                                (hasContent(ketSurat)) &&
                                    (hasContent(asalSurat)) &&
                                        (hasContent(dateSurat)) &&
                                        (hasContent(noSurat)) &&
                                    (hasContent(lampiranSurat)) &&
                                (hasContent(pengolahSurat)) &&
                            (hasContent(dateForwardMsg)) &&
                        (hasContent(tandaTerimaSurat)) &&
                    (hasContent(catatanSurat))){
                uploadImage();
                //Toast.makeText(InputSuratAgenda.this,"Terisi!",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(InputSuratAgenda.this,"Tolong Isi Dengan Lenkap !",Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(InputSuratAgenda.this,"Tidak Ada Gambar",Toast.LENGTH_LONG).show();
        }
    }

    private boolean hasContent(EditText et) {
        // Always assume false until proven otherwise
        boolean bHasContent = false;

        if (et.getText().toString().trim().length() > 0) {
            // Got content
            bHasContent = true;
        }
        return bHasContent;
    }

    public void helpFile(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage("Bila Gambar Pada Urutannya Tidak Dari 0, Mohon Untuk Kembali Ke Home!");
        builder.show();
    }

}
