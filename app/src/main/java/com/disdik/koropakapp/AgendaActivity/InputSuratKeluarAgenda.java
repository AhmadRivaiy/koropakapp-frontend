package com.disdik.koropakapp.AgendaActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@SuppressLint({"NewApi", "SetJavaScriptEnabled"})
public class InputSuratKeluarAgenda extends AppCompatActivity{
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;
    private String jenisSurat, tujuanSurat, LOG = "InputSRTKLR";
    private EditText noSurat, noSuratII, ketTujuanSurat, catSurat, dateSurat, perihalSurat, searchS, resultS, searchKCD, resultK;
    private TextView inputDate, intFile;
    private LinearLayout searchSchool, linearInput, fieldKCD;

    String IdBidang, npsnSekolah, idMaster;
    String tujuanK, tujuanS, resultSkola;
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private Bitmap bitmap;
    private ImageView imageView;
    private Uri uri;
    private DatabaseHelper myDb;
    private LayoutInflater inflater;
    private WebView webView;
    private ProgressBar progressBar;
    private AlertDialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_surat_keluar_agenda);
        //SetLocaleDate
        dateFormatter   = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        //Clear DBSQLite
        myDb = new DatabaseHelper(this);
        myDb.deleteDataSrt();

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdBidang = user.get(SharedPrefManager.SP_ID_KANTOR);

        //GetEditText
        noSurat         = findViewById(R.id.noSurat);
        ketTujuanSurat  = findViewById(R.id.keteranganSurat);
        catSurat        = findViewById(R.id.catatanSurat);
        dateSurat       = findViewById(R.id.dateInputMsg);
        perihalSurat    = findViewById(R.id.ringkasanSurat);
        intFile         = findViewById(R.id.intFile);
        imageView       = findViewById(R.id.showResult);

        searchSchool    = findViewById(R.id.fieldSekolah);
        linearInput     = findViewById(R.id.linearInputKlr);
        searchS         = findViewById(R.id.searchSekolah);
        resultS         = findViewById(R.id.resultSekolah);

        fieldKCD        = findViewById(R.id.fieldKcd);
        searchS         = findViewById(R.id.searchSekolah);
        resultK         = findViewById(R.id.resultKCD);
        searchKCD       = findViewById(R.id.searchKCD);

        searchS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        searchKCD.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilterKCD(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Input Surat Keluar");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

        searchSchool.setVisibility(View.GONE);
        fieldKCD.setVisibility(View.GONE);

    }

    public void getNoSurat(View view) {
        helpFile();
    }

    public void helpFile() {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.modal_nosurat, null);
            webView = dialogView.findViewById(R.id.webViewNoSurat);
            progressBar = dialogView.findViewById(R.id.progressBar);
            EditText edit = dialogView.findViewById(R.id.edit);
            edit.setFocusable(true);
            edit.requestFocus();

            ButtonClickJavascriptInterface myJavaScriptInterface = new ButtonClickJavascriptInterface(InputSuratKeluarAgenda.this);
            webView.addJavascriptInterface(myJavaScriptInterface, "Android");
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(Config.URL_GETNOSURAT);
            webView.setWebViewClient(new InputSuratKeluarAgenda.myWebClient());

            alertDialogBuilder.setView(dialogView);
            alertDialogBuilder.setCancelable(true);
            builder = alertDialogBuilder.create();
            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            openAppRating(this);
            Toast.makeText(InputSuratKeluarAgenda.this, "Please Install Google Chrome App, to Run App With Smoothly", Toast.LENGTH_LONG).show();
        }
    }

    public static void openAppRating(Context context) {
        // you can also use BuildConfig.APPLICATION_ID
        String appId = "com.android.chrome";
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));
        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = context.getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp: otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.koropakapp")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                // make sure it does NOT open in the stack of your activity
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task reparenting if needed
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                //  this make sure it still go to the app page you requested
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this make sure only the Google Play app is allowed to
                // intercept the intent
                rateIntent.setComponent(componentName);
                context.startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if GP not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="+appId));
            context.startActivity(webIntent);
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(InputSuratKeluarAgenda.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }

    public class ButtonClickJavascriptInterface {
        Context mContext;
        ButtonClickJavascriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
       public void onResult(final String toast){
            /*Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            final ProgressDialog progresRing = ProgressDialog.show(MyActivity.this, "Trinity Tuts", "Wait Loading...", true);
            runOnUiThread(new Runnable() {
                // TODO: 2020-02-07  
                @Override
                public void run() {
                    noSurat.setText(toast);
                }
            });
            builder.dismiss();
            */
            Thread thread = new Thread(){
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(1000);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    noSurat.setText(toast);
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    builder.dismiss();
                };
            };
            thread.start();
        }
    }

    public void onRadioButtonClick(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton)view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.umumSurat:
                if (checked)
                    jenisSurat = "Umum";
                break;
            case R.id.perintahSurat:
                if (checked)
                    jenisSurat = "Surat Perintah";
                break;
            case R.id.ketSurat:
                if (checked)
                    jenisSurat = "Surat Keterangan";
                break;
            case R.id.izinSurat:
                if (checked)
                    jenisSurat = "Surat Izin";
                break;
            case R.id.undanganSurat:
                if (checked)
                    jenisSurat = "Surat Undangan";
                break;
            case R.id.lainSurat:
                if (checked)
                    jenisSurat = "Lain - Lain";
                break;
            case R.id.uptdTujuan:
                if (checked)
                    tujuanSurat = "UPTD Tikomdik";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            case R.id.disdikTujuan:
                if (checked)
                    tujuanSurat = "Disdik Jawa Barat";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            case R.id.kcdTujuan:
                if (checked)
                    tujuanSurat = "KCD";
                    searchSchool.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "1";
                    expand(fieldKCD);
                break;
            case R.id.sekolahTujuan:
                if (checked)
                    tujuanSurat = "Sekolah";
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "1";
                    tujuanK = "0";
                    expand(searchSchool);
                break;
            case R.id.instansiTujuan:
                if (checked)
                    tujuanSurat = "Instansi Lain";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            case R.id.perTujuan:
                if (checked)
                    tujuanSurat = "Per-Orangan";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            default:
                jenisSurat = "";
                tujuanSurat = "";
        }
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultSekolah");
                    if (data.length() == 0){
                        Toast.makeText(InputSuratKeluarAgenda.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String npsn = json.getString("npsn");
                        String nama = json.getString("nama_sekolah");
                        npsnSekolah = npsn;
                        resultS.setText(nama);
                        Log.e("Result", npsnSekolah);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

        @Override
        protected String doInBackground(Void... params) {
            RequestHandler rh = new RequestHandler();
            String s = rh.sendGetRequestParam(Config.URL_FETCH_SEKOLAH, query);
            return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getFilterKCD(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultKcd");
                    if (data.length() == 0){
                        Toast.makeText(InputSuratKeluarAgenda.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String id_master = json.getString("id_master");
                        String nama_jabatan = json.getString("nama_jabatan");
                        idMaster = id_master;
                        resultK.setText(nama_jabatan);
                        Log.e("Result", idMaster);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FETCH_KCD, query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void getDate(View view) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateSurat.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    //Requesting permission
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Akses Kamera Tidak Diberikan. Mohon Diberi Akses!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                showFileChooser();
                break;
        }
    }

    private void showFileChooser() {
        int REQUEST_CODE = 99;
        int preference = ScanConstants.OPEN_CAMERA;
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
                imageView.setImageBitmap(bitmap);
                String nameFile = getFileName(uri);
                AddData(nameFile);
                Log.e("Array", nameFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    String getFileName(Uri uri){
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public void AddData(String name) {
        boolean isInserted = myDb.insertDataSrt(name);
        Cursor stat = myDb.cekStatSrt();
        StringBuffer buffer = new StringBuffer();
        while (stat.moveToNext() ) {
            buffer.append(stat.getString(0));
        }
        intFile.setText(buffer.toString());
        if(isInserted == true) {
            Toast.makeText(InputSuratKeluarAgenda.this, "Ok! : 1", Toast.LENGTH_LONG).show();
            class UploadImage extends AsyncTask<Bitmap,Void,String> {
                ProgressDialog loading;
                RequestHandler rh = new RequestHandler();

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(InputSuratKeluarAgenda.this, "Menambah Gambar", "Please wait...",true,true);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    if(s.equals("")){
                        Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                protected String doInBackground(Bitmap... params) {
                    Bitmap bitmap = params[0];
                    String url = (Config.URL_UPLOAD_IMG_OUT);

                    String uploadImage = getStringImage(bitmap);
                    HashMap<String,String> data = new HashMap<>();
                    data.put("image", uploadImage);
                    data.put("name", getFileName(uri));

                    String result = rh.sendPostRequest(url, data);
                    return result;

                }

            }

            UploadImage ui = new UploadImage();
            ui.execute(bitmap);
        }
    }

    public String getStringImage(Bitmap bmp){
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp = Bitmap.createScaledBitmap(bmp, 1240, 1754, true);
        bmp.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void scanImg(View view) {
        checkPermissions();
    }

    public void onSubmit(View view) {
        if(uri!=null) {
            if(tujuanS != null) {
                if(tujuanS.equals("0")) {
                    if(tujuanK.equals("0")){
                        int getNoSurat = noSurat.getText().toString().length();
                        int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                        int getCatSurat = catSurat.getText().toString().length();
                        int getDateSurat = dateSurat.getText().toString().length();
                        int tujuan = tujuanSurat.length();
                        int jenis = jenisSurat.length();

                        if ((getNoSurat != 0) && (getRingkasanSurat != 0) && (getDateSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0)) {
                            npsnSekolah =   "0";
                            idMaster    =   "0";
                            uploadImage();
                        } else {
                            Toast.makeText(InputSuratKeluarAgenda.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        int getNoSurat = noSurat.getText().toString().length();
                        int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                        int getCatSurat = catSurat.getText().toString().length();
                        int getDateSurat = dateSurat.getText().toString().length();
                        int tujuan = tujuanSurat.length();
                        int jenis = jenisSurat.length();
                        int id_master = resultK.getText().toString().length();
                        if ((getNoSurat != 0) && (getRingkasanSurat != 0) && (getDateSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0) && (id_master != 0)) {
                            npsnSekolah =   "0";
                            uploadImage();
                        } else {
                            Toast.makeText(InputSuratKeluarAgenda.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                        }
                    }
                }else {
                    int getNoSurat = noSurat.getText().toString().length();
                    int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                    int getCatSurat = catSurat.getText().toString().length();
                    int getDateSurat = dateSurat.getText().toString().length();
                    int tujuan = tujuanSurat.length();
                    int jenis = jenisSurat.length();
                    int npsn = resultS.getText().toString().length();

                    if ((getNoSurat != 0) && (getRingkasanSurat != 0) && (getDateSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0) && (npsn != 0)) {
                        idMaster    =   "0";
                        uploadImage();
                    } else {
                        Toast.makeText(InputSuratKeluarAgenda.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                    }
                }
            }else {
                Toast.makeText(InputSuratKeluarAgenda.this,"Tolong Isi Dengan Lenkap!",Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(InputSuratKeluarAgenda.this,"Gambar Kosong !",Toast.LENGTH_LONG).show();
        }
    }

    private void uploadImage(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(InputSuratKeluarAgenda.this, "Uploading Data", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Log.e(LOG, s);
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet Data",Toast.LENGTH_LONG).show();
                }else{
                    Log.d("FCM ", s);
                    Toast.makeText(getApplicationContext(), "Berhasil Input!",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(InputSuratKeluarAgenda.this, HomeAgenda.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                String url = (Config.URL_UPLOAD_SRTKLR);
                Cursor res = myDb.getAllDataSrt();

                ArrayList<String> name = new ArrayList<String>();
                while (res.moveToNext() ) {
                    name.add(res.getString(1) + ".jpg");
                }
                String selectClause = StringUtils.join(name.toArray(), ',' );

                //GetText
                String getNoSurat = noSurat.getText().toString();
                String getTujuanSurat = ketTujuanSurat.getText().toString();
                String getPerihalSurat = perihalSurat.getText().toString();
                String getCatSurat = catSurat.getText().toString();
                String getDateSurat = dateSurat.getText().toString();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_kantor", IdBidang);
                data.put("name", selectClause);
                data.put("kode_surat", getNoSurat);
                data.put("isi_surat_klr", getPerihalSurat);
                data.put("jenis_surat_klr", jenisSurat);
                data.put("tujuan_surat_klr", tujuanSurat);
                data.put("keterangan_klr", getTujuanSurat);
                data.put("tgl_surat_klr", getDateSurat);
                data.put("catatan", getCatSurat);
                data.put("npsn_tujuan", npsnSekolah);
                data.put("id_master", idMaster);

                String result = rh.sendPostRequest(url, data);
                return result;

            }

        }

        UploadImage ui = new UploadImage();
        ui.execute(bitmap);

    }

    public static void expand(final View view) {

        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    public void helpFile(View view) {
    }

}
