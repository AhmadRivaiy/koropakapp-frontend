package com.disdik.koropakapp.AgendaActivity;

import android.annotation.TargetApi;
import androidx.appcompat.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiMasukAgenda;
import com.disdik.koropakapp.Fragments.DisposisiStaffFrag;
import com.disdik.koropakapp.Fragments.HomeAgendaFrag;
import com.disdik.koropakapp.Fragments.InboxAgendaFrag;
import com.disdik.koropakapp.Fragments.OutboxAgendaFrag;
import com.disdik.koropakapp.Fragments.OutboxAgendaList;
import com.disdik.koropakapp.Fragments.OutboxStaffFrag;
import com.disdik.koropakapp.Fragments.RiwayatMessAgenda;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.Pengaturan;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

public class HomeAgenda extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private SharedPrefManager sharedPrefManager;
    FragmentManager fragmentManager;
    Fragment fragment = null;
    RelativeLayout homeAgenda;
    Toolbar toolbar;
    CoordinatorLayout coordinatorLayout;
    private String ownAction = "0", getIdMaster, getDayTime;
    private ImageView imgNav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_agenda);
        //Session
        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        String getSessionemail = user.get(SharedPrefManager.SP_EMAIL);
        String getUsername = user.get(SharedPrefManager.SP_NAMA);
        getDayTime  = user.get(SharedPrefManager.SP_HOURS_OF_DAY);
        getIdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        final String getId = user.get(SharedPrefManager.SP_ID_JABATAN);

        //ToolBar
        toolbar     = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        coordinatorLayout = findViewById(R.id.coordinator);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode( ActionBar.NAVIGATION_MODE_STANDARD );
        actionBar.setDisplayShowTitleEnabled( true );
        actionBar.setDisplayHomeAsUpEnabled( true );
        actionBar.setHomeButtonEnabled( true );
        actionBar.setTitle("Beranda");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (ownAction){
                    case "1":
                        Intent inbox = new Intent(getApplicationContext(), InputSuratAgenda.class);
                        startActivity(inbox);
                        break;
                    case "2":
                        Intent outbox = new Intent(getApplicationContext(), InputSuratKeluarAgenda.class);
                        startActivity(outbox);
                        break;
                        default:
                            Intent feedback = new Intent(getApplicationContext(), FeedBackActivity.class);
                            startActivity(feedback);
                }
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        //NavHeader
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headeView      = navigationView.getHeaderView(0);
        imgNav              =   headeView.findViewById(R.id.imageNavHeader);
        TextView titleNav   =   headeView.findViewById(R.id.titleNavHeader);
        TextView emailNav   =   headeView.findViewById(R.id.emailNavHeader);
        titleNav.setText(getUsername);
        emailNav.setText(getSessionemail);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // tampilan default awal ketika aplikasii dijalankan
        if (savedInstanceState == null) {
            fragment = new HomeAgendaFrag();
            callFragment(fragment);
            toolbar.setBackgroundColor(Color.TRANSPARENT);
            if(getDayTime.equals("Selamat Malam,")) {
                coordinatorLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.night_bg));
            }else {
                coordinatorLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_cloud));
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Ion.with(this).load(Config.URL_GET_PASSWORD_NOW + getIdMaster)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                String img   = result.get("imageProfile").getAsString();

                                if(img.equals("0")){
                                    Glide.with(HomeAgenda.this)
                                            .load(Config.URL_PROFILE_AGENDA)
                                            .crossFade()
                                            .into(imgNav);
                                }else {
                                    Glide.with(HomeAgenda.this).load(Config.URL_IMG_PROFILE + img)
                                            .crossFade()
                                            .into(imgNav);
                                    imgNav.setColorFilter(ContextCompat.getColor(HomeAgenda.this, android.R.color.transparent));
                                }
                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        ActionBar actionBar = getSupportActionBar();

        switch (id){
            case R.id.nav_home:
                ownAction = "0";
                //Beranda
                fragment = new HomeAgendaFrag();
                toolbar.setTitle("Beranda");
                toolbar.setBackgroundColor(Color.TRANSPARENT);
                if(getDayTime.equals("Selamat Malam,")) {
                    coordinatorLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.night_bg));
                }else {
                    coordinatorLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.bg_cloud));
                }
                callFragment(fragment);
                break;

            case R.id.nav_inbox:
                ownAction = "1";
                //Inbox
                fragment = new InboxAgendaFrag();
                toolbar.setTitle("Surat Masuk");
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                coordinatorLayout.setBackground(null);
                callFragment(fragment);
                break;

            case R.id.nav_srt_klr:
                ownAction = "2";
                //Outbox
                fragment = new OutboxAgendaFrag();
                toolbar.setTitle("Validasi Surat Keluar");
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                coordinatorLayout.setBackground(null);
                callFragment(fragment);
                break;

            case R.id.nav_riwayat:
                //Outbox
                fragment = new RiwayatMessAgenda();
                toolbar.setTitle("Riwayat Surat Masuk");
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                coordinatorLayout.setBackground(null);
                callFragment(fragment);
                break;

            case R.id.nav_disposisi:
                fragment = new DisposisiMasukAgenda();
                toolbar.setTitle("Disposisi Masuk");
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                coordinatorLayout.setBackground(null);
                callFragment(fragment);
                break;

            case R.id.nav_srtKlr:
                //Outbox
                fragment = new OutboxAgendaList();
                toolbar.setTitle("Surat Keluar");
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                coordinatorLayout.setBackground(null);
                callFragment(fragment);
                break;

            case R.id.nav_profile:
                Intent intent = new Intent(this, HomeSettings.class);
                startActivity(intent);
                break;

            case R.id.nav_feedback:
                Intent intentF = new Intent(this, FeedBackActivity.class);
                startActivity(intentF);
                break;

            case R.id.nav_setting:
                Intent intentSetting = new Intent(this, Pengaturan.class);
                startActivity(intentSetting);
                break;

            case R.id.nav_logout:
                showDialog();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder.setTitle("Keluar dari aplikasi?");
        alertDialogBuilder
                .setMessage("Tap Ya untuk keluar!")
                .setIcon(R.mipmap.ic_app_koropak_round)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        startActivity(new Intent(HomeAgenda.this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // untuk mengganti isi kontainer menu yang dipiih
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void callFragment(Fragment fragment) {
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.mainFrame, fragment)
                .commit();

    }
}
