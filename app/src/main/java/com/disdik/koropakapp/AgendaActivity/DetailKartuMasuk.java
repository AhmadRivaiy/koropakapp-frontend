package com.disdik.koropakapp.AgendaActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DetailKartuMasuk extends AppCompatActivity {
    private PDFView pdfView;
    private String IdSuratMsk, filesName;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kartu_masuk);
        Toolbar toolbar         =   findViewById(R.id.toolbar);
        TextView noSurat        =   findViewById(R.id.noSuratMsk);
        TextView perihalSurat   =   findViewById(R.id.perihalSuratMsk);
        pdfView                 =   findViewById(R.id.viewKartuMasuk);

        toolbar.setTitle("Kartu Surat Masuk");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        IdSuratMsk     = intent.getStringExtra("id_surat_msk");
        filesName      = intent.getStringExtra("file_kartu_masuk");
        String noSuratMsk       = intent.getStringExtra("no_surat_msk");
        String PerihalSurat     = intent.getStringExtra("perihal_surat_msk");

        noSurat.setText(noSuratMsk);
        perihalSurat.setText(PerihalSurat);

        downloadPDF();
    }

    public void downloadBtnMsk(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_download_msk, null);
        TextView close = dialogView.findViewById(R.id.closeClick);
        TextView filePdf    =   dialogView.findViewById(R.id.nameFileKartu);

        filePdf.setText(filesName);

        close.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false);

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // menampilkan alert dialog
        alertDialog.show();
    }

    //TODO: Pdf Downloaded
    public void downloadPDF(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailKartuMasuk.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                File file = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/download/" + filesName);
                if (file.exists()) {
                    pdfView.fromFile(file)
                            .enableSwipe(true) // allows to block changing pages using swipe
                            .swipeHorizontal(true)
                            .enableDoubletap(true)
                            .defaultPage(0)
                            .onDrawAll(new OnDrawListener() {
                                @Override
                                public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                                    Log.e("onDrawAll",pageWidth+":"+pageHeight+":"+displayedPage);
                                }
                            })
                            .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                            .password(null)
                            .scrollHandle(null)
                            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                            // spacing between pages in dp. To define spacing color, set view background
                            .spacing(0)
                            .pageFitPolicy(FitPolicy.WIDTH)
                            .load();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                int count;
                try {
                    Log.d("PDF Downloading", "url = " + Config.URL_DOWNLOAD_PDF_KARTUMSK + filesName);
                    URL _url = new URL(Config.URL_DOWNLOAD_PDF_KARTUMSK + filesName);
                    URLConnection conection = _url.openConnection();
                    conection.connect();
                    InputStream input = new BufferedInputStream(_url.openStream(),
                            8192);
                    OutputStream output = new FileOutputStream(
                            Environment.getExternalStorageDirectory() + "/download/" + filesName);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    Log.e("Error ", e.getMessage());
                }
                return null;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
