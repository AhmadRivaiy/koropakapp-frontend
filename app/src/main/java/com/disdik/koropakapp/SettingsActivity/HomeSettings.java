package com.disdik.koropakapp.SettingsActivity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.BidangActivity.DetailDisposisiBidang;
import com.disdik.koropakapp.Class.ImagePickerActivity;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class HomeSettings extends AppCompatActivity {
    public static final int REQUEST_IMAGE = 100;
    private ImageView photoProvile, btn;

    private String Document_img1, getMas, old, statusPass, tokenUser;
    Bitmap bitmap;
    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_settings);
        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getMas      = user.get(SharedPrefManager.SP_ID_MASTER);
        tokenUser   = user.get(SharedPrefManager.SP_TOKEN_LOGIN);
        String namaUser      = user.get(SharedPrefManager.SP_NAMA);
        String emailUser     = user.get(SharedPrefManager.SP_EMAIL);
        String nipUser       = user.get(SharedPrefManager.SP_NIP);
        String kantorUser    = user.get(SharedPrefManager.SP_NAMA_KANTOR);
        String bidangTerkait = user.get(SharedPrefManager.SP_NAMA_BIDANG);
        String jabatanUser   = user.get(SharedPrefManager.SP_NAMA_JABATAN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        photoProvile    =   findViewById(R.id.profile_image);
        btn             =   findViewById(R.id.btnEdit);
        TextView tNama   =   findViewById(R.id.namaUser);
        TextView tEmail  =   findViewById(R.id.emailUser);
        TextView tNip    =   findViewById(R.id.nipUser);
        //TextView tNomorHp  =   findViewById(R.id.nomorHpUser);
        TextView tKantor   =   findViewById(R.id.kantorUser);
        TextView tBidang   =   findViewById(R.id.bidangUser);
        TextView tJabatan  =   findViewById(R.id.jabatanUser);

        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(HomeSettings.this, R.anim.image_clicked));
                showImagePickerOptions();
            }
        });

        tNama.setText(namaUser);
        tEmail.setText(emailUser);
        tJabatan.setText(jabatanUser);
        tNip.setText(nipUser);
        tKantor.setText(kantorUser);
        tBidang.setText(bidangTerkait);

        try {
            Ion.with(this).load(Config.URL_GET_PASSWORD_NOW + getMas)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try{
                                statusPass   = result.get("status_pass").getAsString();
                                old          = result.get("old").getAsString();
                                String img   = result.get("imageProfile").getAsString();

                                if(img.equals("0")){
                                    Glide.with(HomeSettings.this)
                                            .load(R.mipmap.ic_person)
                                            .into(photoProvile);
                                    photoProvile.setColorFilter(ContextCompat.getColor(HomeSettings.this, android.R.color.transparent));
                                }else {
                                    Glide.with(HomeSettings.this).load(Config.URL_IMG_PROFILE + img)
                                            .into(photoProvile);
                                    photoProvile.setColorFilter(ContextCompat.getColor(HomeSettings.this, android.R.color.transparent));
                                }

                            }
                            catch (Exception a){
                                a.printStackTrace();
                            }
                        }
                    });
        }catch (Exception a){
            a.printStackTrace();
        }
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }

            @Override
            public void onChooseResetDefault() {
                resetDefault();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(HomeSettings.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(HomeSettings.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void resetDefault(){
        class UploadImage extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(HomeSettings.this, "Uploading Image", "Please wait...", true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if (s.equals("")) {
                    Toast.makeText(getApplicationContext(), "Tidak Ada Koneksi Internet IMAGE", Toast.LENGTH_LONG).show();
                }else {
                    Glide.with(HomeSettings.this).load("http://www.therapyspotstatesboro.com/wp-content/uploads/2014/04/user.png")
                            .into(photoProvile);
                    photoProvile.setColorFilter(ContextCompat.getColor(HomeSettings.this, android.R.color.transparent));
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String url = (Config.URL_RESET_PHOTO);

                HashMap<String, String> data = new HashMap<>();
                data.put("name", "0");
                data.put("id_master", getMas);

                String result = rh.sendPostRequest(url, data);
                return result;

            }

        }
        UploadImage ui = new UploadImage();
        ui.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    // loading profile image from local cache
                    loadProfile(uri.toString());
                    BitMapToString(bitmap);
                    updatePhoto();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void loadProfile(String url) {
        Log.d("Image", "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(photoProvile);
        photoProvile.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(userImage1 != null){
            userImage1.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        }else {
            Document_img1 = null;
        }
        return Document_img1;
    }

    public void updatePhoto() {
        class UploadImage extends AsyncTask<Bitmap, Void, String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(HomeSettings.this, "Uploading Image", "Please wait...", true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if (s.equals("")) {
                    Toast.makeText(getApplicationContext(), "Tidak Ada Koneksi Internet IMAGE", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String url = (Config.URL_CHANGE_PHOTO);

                String uploadImage = BitMapToString(bitmap);
                HashMap<String, String> data = new HashMap<>();
                data.put("image", uploadImage);
                data.put("name", String.valueOf(System.currentTimeMillis()));
                data.put("id_master", getMas);

                String result = rh.sendPostRequest(url, data);
                return result;

            }

        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
