package com.disdik.koropakapp.TikomdikActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiKepalaFrag;
import com.disdik.koropakapp.Fragments.DisposisiKplFrag;
import com.disdik.koropakapp.Fragments.HomeKepalaFrag;
import com.disdik.koropakapp.Fragments.InboxKepalaFrag;
import com.disdik.koropakapp.Fragments.LainnyaFrag;
import com.disdik.koropakapp.Fragments.OutboxKepalaFrag;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class KepalaTikomdikActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    SharedPrefManager sharedPrefManager;
    DatabaseHelper myDb;
    private BottomNavigationView bottomNavigationView;

    String getIdMaster, IdKantor, getTokenLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kepala_tikomdik);

        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getIdMaster     = user.get(SharedPrefManager.SP_ID_MASTER);
        IdKantor        = user.get(SharedPrefManager.SP_ID_KANTOR);
        getTokenLogin   = user.get(SharedPrefManager.SP_TOKEN_LOGIN);

        myDb    =   new DatabaseHelper(this);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        //Default ProgramFragment
        mToolbar.setTitle("Home");
        loadFragment(new HomeKepalaFrag());
        bottomNavigationView = findViewById(R.id.nav_view_kepala);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container_kepala, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeKepalaFrag();
                mToolbar.setTitle("Home");
                break;
            case R.id.nvg_srt_masuk:
                fragment = new InboxKepalaFrag();
                mToolbar.setTitle("Surat Masuk");
                break;
            case R.id.nvg_srt_keluar:
                fragment = new OutboxKepalaFrag();
                mToolbar.setTitle("Surat Keluar");
                break;

                /*
            case R.id.nvg_disposisi:
                fragment = new DisposisiKplFrag();
                mToolbar.setTitle("Disposisi");
                break;
                */

            case R.id.riwayat_disposisi:
                fragment = new DisposisiKepalaFrag();
                mToolbar.setTitle("Riwayat Disposisi");
                break;

            case R.id.nvg_lainnya:
                fragment = new LainnyaFrag();
                mToolbar.setTitle("Lainnya");
                break;
        }
        return loadFragment(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);

                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("statusAbsen");
                    JSONObject json = jsonArray.getJSONObject(0);

                    int stat_msk = json.getInt("stat_msk");
                    int stat_klr = json.getInt("stat_klr");

                    if(stat_msk > 0) {
                        bottomNavigationView.getOrCreateBadge(R.id.nvg_srt_masuk).setNumber(stat_msk);
                    }

                    if(stat_msk == 0){
                        bottomNavigationView.getOrCreateBadge(R.id.nvg_srt_masuk).setVisible(false);
                    }

                    if(stat_klr > 0) {
                        bottomNavigationView.getOrCreateBadge(R.id.nvg_srt_keluar).setNumber(stat_klr);
                    }

                    if(stat_klr == 0){
                        bottomNavigationView.getOrCreateBadge(R.id.nvg_srt_keluar).setVisible(false);
                    }

                }catch (JSONException a){
                    a.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_master", getIdMaster);
                data.put("id_kantor", IdKantor);

                return rh.sendPostRequest(Config.URL_CHECK_STATUS_KEPALA, data);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }
}
