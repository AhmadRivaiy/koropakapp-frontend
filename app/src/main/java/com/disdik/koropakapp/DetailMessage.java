package com.disdik.koropakapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.AgendaActivity.HomeAgenda;
import com.disdik.koropakapp.Class.AdapterImage;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.SekdisActivity.DetailMailSekdis;
import com.disdik.koropakapp.TikomdikActivity.DetailInboxKepala;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailMessage extends AppCompatActivity {
    TextView judul, isi, tgl, dari, kepada, sifat, dateM, pengirim, noAgenda, intimg, statusAcc;
    String id_dis = null, IdKantor, getUsername;
    CardView info;
    LayoutInflater inflater;
    CardView inputAgenda;
    ProgressBar progressBar;
    LinearLayout lembar;

    private RecyclerView listView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private SharedPrefManager sharedPrefManager;
    private AdapterImage adapter;
    private WebView webView;
    String getSessionId;
    ArrayList<HashMap<String, String>> list_data;

    private String spFiles = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_message);
        requestQueue = Volley.newRequestQueue(this);

        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor        = user.get(SharedPrefManager.SP_ID_KANTOR);
        getUsername     = user.get(SharedPrefManager.SP_NAMA);

        Intent intent = getIntent();
        id_dis = intent.getStringExtra(Config.EMP_ID);
        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        //Judul
        judul = findViewById(R.id.titleDetailPesan);
        pengirim = findViewById(R.id.fromMasuk);
        dari = findViewById(R.id.fromMsk);
        kepada = findViewById(R.id.forMsk);
        tgl = findViewById(R.id.tglMsk);
        sifat = findViewById(R.id.asalMsk);
        dateM = findViewById(R.id.dateMessage);
        noAgenda = findViewById(R.id.statusAgenda);
        statusAcc = findViewById(R.id.statusAcc);
        intimg = findViewById(R.id.intImg);

        //
        lembar = findViewById(R.id.lembarDisposisi);

        //prgresBAr
        progressBar     = findViewById(R.id.progressBar);

        pengirim.setText(getString(R.string.admin));
        dari.setText(getString(R.string.admin));
        kepada.setText(getString(R.string.pimpinan));

        info = findViewById(R.id.infoMessage);
        showDetailMessage();
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(DetailMessage.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }

    private void showDetailMessage(){
        Ion.with(this).load(Config.URL_GET_DETAIL + id_dis + "&id_kantor=" + IdKantor)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @SuppressLint("SetJavaScriptEnabled")
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try{
                            String id_surat_msk         = result.get("id_surat_msk").getAsString();
                            String judul_str            = result.get("judul").getAsString();
                            String no_surat_msk         = result.get("no_surat_msk").getAsString();
                            String no_agenda_msk        = result.get("no_agenda_msk").getAsString();
                            String asal_surat_msk       = result.get("asal_surat_msk").getAsString();
                            String isi_surat_msk        = result.get("isi_surat_msk").getAsString();
                            String tgl_upload           = result.get("tgl_upload").getAsString();
                            String files                = result.get("files").getAsString();
                            String status_read          = result.get("status_read").getAsString();
                            String tgl_surat_msk        = result.get("tgl_surat_msk").getAsString();
                            String status_read_kpl      = result.get("status_read_kpl").getAsString();
                            String tgl_surat_msk_str    = result.get("tgl_surat_msk_str").getAsString();


                            intimg.setText(files + " Gambar");
                            noAgenda.setText(no_agenda_msk);
                            judul.setText(no_surat_msk);
                            //isi.setText(isi_surat_msk);
                            sifat.setText(asal_surat_msk);
                            tgl.setText(tgl_surat_msk);
                            dateM.setText(tgl_surat_msk_str);

                            if(status_read_kpl.equals("0")){
                                statusAcc.setText("Waiting...");
                                statusAcc.setTypeface(null, Typeface.BOLD_ITALIC);
                                statusAcc.setTextColor(Color.parseColor("#FFC107"));
                            }else{
                                statusAcc.setText("Accepted!");
                                statusAcc.setTypeface(null, Typeface.BOLD);
                                statusAcc.setTextColor(Color.parseColor("#009688"));
                                //expand(lembar);
                            }

                            webView = (WebView) findViewById(R.id.webViewDisposisi);
                            webView.getSettings().setLoadsImagesAutomatically(true);
                            webView.getSettings().setJavaScriptEnabled(true);
                            webView.getSettings().setDomStorageEnabled(true);

                            // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
                            //webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                            webView.setWebViewClient(new DetailMessage.myWebClient());
                            webView.loadUrl(Config.URL_TRACKING_SURAT + "nomor_surat=" + no_surat_msk + "&tgl_surat=" + tgl_surat_msk + "&id_surat=" + id_surat_msk);

                        }
                        catch (Exception a){
                            a.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Memanggil/Memasang menu item pada toolbar dari layout menu_bar.xml
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete :
                //Kode disini akan di eksekusi saat tombol about di klik
                Toast.makeText(this, "Ini Delete", Toast.LENGTH_SHORT).show();
                break;
            case R.id.unread :
                Toast.makeText(this, "Mark As Unread", Toast.LENGTH_SHORT).show();
                /*
                Ion.with(this).load(Config.URL_UPDATE_READ + id_dis + "&data=0")
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                try{
                                    String status  = result.get("status").getAsString();
                                }
                                catch (Exception a){
                                    a.printStackTrace();
                                }
                            }
                        });

                Intent intent = new Intent(this, HomeAgenda.class);
                startActivity(intent);
                finish();
                */
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void viewScanned(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_img, null);
        listView = dialogView.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.HORIZONTAL);
        listView.setLayoutManager(llm);

        list_data = new ArrayList<HashMap<String, String>>();
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_GET_IMG_IN + id_dis + "&id_kantor=" + IdKantor, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_img");
                    if (jsonArray.length() != 0){
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject json = jsonArray.getJSONObject(a);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("api", json.getString("api"));
                            map.put("api_img", json.getString("api_img"));
                            map.put("img_files", json.getString("img_files"));
                            list_data.add(map);
                            adapter = new AdapterImage(DetailMessage.this, list_data);
                            ((AdapterImage) adapter).setOnItemClickListener(new AdapterImage.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                                }
                            });
                            listView.setAdapter(adapter);
                        }
                    }else{
                        Toast.makeText(DetailMessage.this, "Pesan Kosong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailMessage.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);

        // set pesan dari dialog
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    public static void expand(final View view) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
