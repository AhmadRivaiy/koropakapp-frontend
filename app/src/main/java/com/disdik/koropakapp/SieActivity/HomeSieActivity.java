package com.disdik.koropakapp.SieActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.disdik.koropakapp.BidangActivity.BidangActivity;
import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiSieFrag;
import com.disdik.koropakapp.Fragments.HomeBidangFrag;
import com.disdik.koropakapp.Fragments.HomeSieFrag;
import com.disdik.koropakapp.Fragments.LainnyaFrag;
import com.disdik.koropakapp.Fragments.OutboxSieFrag;
import com.disdik.koropakapp.Fragments.RiwayatSieFrag;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.HashMap;

public class HomeSieActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    SharedPrefManager sharedPrefManager;
    DatabaseHelper myDb;
    BottomNavigationView bottomNavigationView;
    String getIdMaster, IdKantor, IdBidang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_sie);
        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getIdMaster     = user.get(SharedPrefManager.SP_ID_MASTER);
        IdKantor     = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdBidang     = user.get(SharedPrefManager.SP_ID_BIDANG);

        myDb    =   new DatabaseHelper(this);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        //Default ProgramFragment
        mToolbar.setTitle("Home");
        loadFragment(new HomeSieFrag());

        bottomNavigationView = findViewById(R.id.nav_view_sie);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container_sie, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.nvg_home:
                fragment = new HomeSieFrag();
                mToolbar.setTitle("Home");
                break;
            case R.id.nvg_disposisi:
                fragment = new DisposisiSieFrag();
                mToolbar.setTitle("Disposisi");
                break;
            case R.id.nvg_surat_klr:
                fragment = new OutboxSieFrag();
                mToolbar.setTitle("Surat Keluar");
                break;
            case R.id.nvg_riwayat:
                fragment = new RiwayatSieFrag();
                mToolbar.setTitle("Riwayat");
                break;
            case R.id.nvg_lainnya:
                fragment = new LainnyaFrag();
                mToolbar.setTitle("Lainnya");
                break;
        }
        return loadFragment(fragment);
    }


    @Override
    public void onResume() {
        super.onResume();

        try {
            Ion.with(this).load(Config.URL_CHECK_STATUS_SIE + getIdMaster + "&id_kantor=" + IdKantor + "&id_bidang=" + IdBidang)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try {
                                int dis_msk = result.get("dis_msk").getAsInt();
                                int srt_klr = result.get("srt_klr").getAsInt();

                                if(dis_msk > 0) {
                                    bottomNavigationView.getOrCreateBadge(R.id.nvg_disposisi).setNumber(dis_msk);
                                }

                                if(dis_msk == 0){
                                    bottomNavigationView.getOrCreateBadge(R.id.nvg_disposisi).setVisible(false);
                                }

                                if(srt_klr > 0) {
                                    bottomNavigationView.getOrCreateBadge(R.id.nvg_surat_klr).setNumber(srt_klr);
                                }

                                if(srt_klr == 0){
                                    bottomNavigationView.getOrCreateBadge(R.id.nvg_surat_klr).setVisible(false);
                                }
                            } catch (Exception a) {
                                e.printStackTrace();
                            }
                        }
                    });
        }catch (Exception ae){
            ae.printStackTrace();
        }
    }
}
