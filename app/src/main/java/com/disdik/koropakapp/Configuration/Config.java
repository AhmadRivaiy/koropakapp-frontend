package com.disdik.koropakapp.Configuration;

public class Config {
    private static final String uri      = "http://103.108.158.181/Temp_LetterApp";
    private static final String uriGet   = "http://103.108.158.181/Koropak-app";

    public static final String URL_LOAD_PARAF               = uriGet + "/uploads/signature/paraf/";
    public static final String URL_CEK_MT                   = uri+"/Admin/cekMt";
    public static final String URL_WEBVIEW_UPDATE           = uri+"/Admin/UpdateViews";
    public static final String URL_GET_MSG_ADMIN            = uri+"/Admin/MessageAdmin";
    public static final String URL_GET_CERTIVICATE          = uri+"/Admin/getSerial?id_master=";

    public static final String URL_SET_READ                 = uri+"/C_Sekdis/updateReadDisposisi?id_disposisi=";
    public static final String URL_SET_READ_TIKOMDIK        = uri+"/Msg_Pimpinan/updateReadDisposisi?id_disposisi=";

    public static final String URL_CHANGE_PASSWORD          = uri+"/Admin/updatePassword";
    public static final String URL_GET_PASSWORD_NOW         = uri+"/Admin/getPasswordNow?id_master=";
    public static final String URL_CHANGE_PHOTO             = uri+"/Admin/changePhoto";

    public static final String URL_RESET_PHOTO              = uri+"/Admin/resetPhoto";
    public static final String URL_IMG_PROFILE              = "http://103.108.158.181/Koropak-app/uploads/photoProfile/";

    public static final String URL_UPLOAD_FEEDBACK          = uri+"/UploadFeedback/inputFeedback";
    public static final String URL_UPLOAD_KOREKSI           = uri+"/UploadFeedback/inputKoreksi";

    public static final String URL_UPLOAD_LAPORAN           = uri+"/UploadLaporan/updateLaporan";
    public static final String URL_UPLOAD_LAPORAN_TIKOMDIK  = uri+"/UploadLaporan/updateLaporanTikomdik";

    public static final String URL_INPUT_TOKEN              = uri+"/Push_Notification/insertToken?id_master=";

    public static final String URL_UPDATE_READ_INBOX        = uri+"/Admin/updateReadInbox?id_surat_msk=";
    public static final String URL_UPDATE_READ_OUTBOX       = uri+"/Admin/updateReadOutbox?id_surat_klr=";
    public static final String URL_IMG_LAPORAN              = "http://103.108.158.181/Koropak-app/uploads/laporan/";
    public static final String URL_IMG_KOREKSI              = "http://103.108.158.181/Koropak-app/uploads/log/";

    public static final String URL_LOG_SRT_KLR              = uri+"/C_Sekdis/listSrtKeluarLog?id_surat_klr=";
    public static final String URL_CEK_ABSEN                = uri+"/C_Staff/cekAbsen";
    public static final String URL_TEMPLATE_KONSEP          = uri+"/Template/getTemplateSrt?id_template=";

    /*
     *
     * Ini Untuk Agenda Level
     *
     *
     */

    public static final String  URL_GET_USER                 = uri+"/Login_Disdik/ceklogin_User";

    public static final String URL_UPLOAD_IMG               = uri+"/C_Agenda/uploadImg";
    public static final String URL_UPLOAD_IMG_OUT           = uri+"/C_Agenda/uploadImgKlr";
    public static final String URL_UPLOAD_AGENDA            = uri+"/C_Agenda/uploadAgenda";

    //Surat Keluar
    public static final String URL_UPLOAD_SRTKLR            = uri+"/C_Agenda/insertSuratKlr";

    //LinkPDF
    public static final String URL_DOWNLOAD_PDF             = "http://103.108.158.181/Koropak-app/uploads/save_pdf/qrcode-";
    public static final String URL_PRINT_PDF_QRCODE         = uri +"/C_Agenda/PrintPDFQRcode";

    public static final String URL_DOWNLOAD_PDF_SRTKLR     = "http://103.108.158.181/Koropak-app/uploads/pdf/";
    public static final String URL_DOWNLOAD_PDF_KARTUMSK   = "http://103.108.158.181/Koropak-app/uploads/kartuSuratMasuk/";

    //ListMessage
    public static final String URL_GET_MESSAGE              = uri+"/C_Agenda/listMessage?id_kantor=";
    public static final String URL_GET_MESSAGE_OUT          = uri+"/C_Agenda/listSuratKlr?id_kantor=";
    public static final String URL_GET_STATUS_AGENDA        = uri+"/C_Agenda/statusAgenda?id_kantor=";
    public static final String URL_GET_KARTU_MSK            = uri+"/C_Agenda/listKartuMasuk";

    //Riwayat
    public static final String URL_GET_RIWAYAT_MESSAGE          = uri+"/C_Agenda/listRiwayat?id_kantor=";
    public static final String URL_GET_FILTER_RIWAYAT_MESSAGE   = uri+"/C_Agenda/listRiwayatFilter?id_kantor=";

    //Filter
    public static final String URL_FILTER_INBOX_AGENDA  = uri+"/C_Agenda/getFilterListMsg?id_kantor=";

    public static final String URL_GET_DETAIL_OUT       = uri+"/C_Agenda/detailMessageKlr?id_surat_klr=";

    public static final String URL_GET_DETAIL           = uri+"/C_Agenda/detailMessage?id_surat_msk=";

    //GetImgKlr
    public static final String URL_GET_IMG_OUT          = uri+"/C_Agenda/getImg?id_surat_klr=";
    public static final String URL_GET_IMG_IN           = uri+"/C_Agenda/getImgMsk?id_surat_msk=";

    public static final String URL_FETCH_SEKOLAH        = uri+"/C_Agenda/fetchSekolah?query=";
    public static final String URL_FETCH_KCD            = uri+"/C_Agenda/fetchKcd?query=";


    public static final String URL_IMG_KLR              = "http://103.108.158.181/Koropak-app/uploads/surat_keluar/";
    public static final String URL_IMG_IN               = "http://103.108.158.181/Koropak-app/uploads/surat_masuk/";

    public static final String URL_GETNOSURAT           = uri+"/C_GetNoSurat";
    public static final String URL_TRACKING_SURAT       = uri + "/C_Agenda/TrackingSuratMasuk?";

    /*
     *
     *
     * Ini Untuk Kadis Level
     *
     *
     */

    public static final String URL_CHECK_STATUS_KADIS       = uri+"/C_Kadis/checkMessage?id_kantor=";
    public static final String URL_WEBVIEW_DISPOSISI        = uri+"/C_FormDisposisi/FormDisKadis?id_surat_msk=";

    //ListInbox
    public static final String URL_GET_MESSAGE_KADIS        = uri+"/C_Kadis/listSrtMasuk?id_kantor=";
    public static final String URL_GET_MESSAGE_KADIS_OUT    = uri+"/C_Kadis/listSrtKeluar?id_kantor=";

    public static final String URL_GET_DETAIL_INBOX_KADIS   = uri+"/C_Kadis/detailSrtMasuk?id_surat_msk=";
    public static final String URL_GET_DETAIL_KLR_KADIS     = uri+"/C_Kadis/detailSrtKeluar?id_surat_klr=";


    //Disposisi
    public static final String URL_GET_DISPOSISI_KADIS      = uri+"/C_Kadis/listDisposisi?id_kantor=";

    //Img
    public static final String URL_GET_IMG_KADIS_MSK        = uri+"/C_Kadis/getImgMsk?id_surat_msk=";
    public static final String URL_GET_IMG_KADIS_OUT        = uri+"/C_Kadis/getImgKLr?id_surat_klr=";


    public static final String URL_FILTER_INBOX_KADIS      = uri+"/C_Kadis/getFilterKadis?id_kantor=";
    public static final String URL_FILTER_OUTBOX_KADIS     = uri+"/C_Kadis/getFilterOutKadis?id_kantor=";

    /*
     *
     *
     * Ini Untuk Level Sekdis
     *
     *
     */

    public static final String URL_CHECK_STATUS_SEKDIS      = uri+"/C_Sekdis/checkMessage?id_master=";
    public static final String URL_SET_TANDA_TANGAN         = uri+"/C_Sekdis/SetTTDSekdis";

    //Disposisi
    public static final String URL_GET_DISPOSISI_SEKDIS     = uri+"/C_Sekdis/listDisposisi?id_master=";
    public static final String URL_GET_RIWAYAT_SEKDIS       = uri+"/C_Sekdis/listRiwayat?nama=";

    public static final String URL_GET_DETAIL_SEKDIS        = uri+"/C_Sekdis/detailDisposisi?id_disposisi=";

    public static final String URL_WEBVIEW_INBOX_SEKDIS     = uri+"/C_FormDisposisi/FormInboxSekdis?id_surat_msk=";
    public static final String URL_WEBVIEW_DISPOSISI_SEKDIS = uri+"/C_FormDisposisi/FormDisSekdis?id_disposisi=";

    //ListInbox
    public static final String URL_GET_MESSAGE_SEKDIS       = uri+"/C_Sekdis/listSrtMasuk?id_kantor=";
    public static final String URL_GET_MESSAGE_SEKDIS_OUT   = uri+"/C_Sekdis/listSrtKeluar";

    public static final String URL_GET_DETAIL_INBOX_SEKDIS  = uri+"/C_Sekdis/detailSrtMasuk?id_surat_msk=";
    public static final String URL_GET_DETAIL_KLR_SEKDIS    = uri+"/C_Sekdis/detailSrtKeluar?id_surat_klr=";

    public static final String URL_GET_IMG_SEKDIS_OUT       = uri+"/C_Sekdis/getImgKlr?id_surat_klr=";

    public static final String URL_FILTER_INBOX_SEKDIS             = uri+"/C_Sekdis/getFilterSekdis?id_kantor=";
    public static final String URL_FILTER_OUTBOX_SEKDIS            = uri+"/C_Sekdis/getFilterOutSekdis";
    public static final String URL_FILTER_DISPOSISI_SEKDIS         = uri+"/C_Sekdis/getFilterDisSekdis?id_master=";

    /*
     *
     *
     * Ini Untuk Level Bidang, kecuali GTK
     *
     * SMK
     */
    public static final String URL_CHECK_STATUS_BIDANG          = uri+"/C_Bidang/checkMessage?id_master=";

    //Disposisi
    public static final String URL_GET_DISPOSISI_BIDANG         = uri+"/C_Bidang/listDisposisi?id_master=";
    public static final String URL_GET_DETAIL_BIDANG            = uri+"/C_Bidang/detailDisposisi?id_disposisi=";
    public static final String URL_WEBVIEW_DISPOSISI_BIDANG     = uri+"/C_FormDisposisi/FormDisBidang?id_disposisi=";
    //Riwayat
    public static final String URL_GET_RIWAYAT_BIDANG           = uri+"/C_Bidang/listRiwayat?nama=";
    //GetIm
    public static final String URL_GET_IMG_BIDANG               = uri+"/C_Bidang/getImgMsk?id_surat_msk=";

    public static final String URL_FILTER_DISPOSISI_BIDANG      = uri+"/C_Bidang/getFilterDisBidang?id_master=";
    public static final String URL_GET_SURAT_KELUAR_BIDANG      = uri+"/C_Bidang/listSrtKeluar";
    public static final String URL_FILTER_OUTBOX_BIDANG         = uri+"/C_Bidang/getFilterOutBidang";
    public static final String URL_GET_DETAIL_KLR_BIDANG        = uri+"/C_Bidang/detailSrtKeluar?id_surat_klr=";
    /*
     *
     *
     * Ini Untuk Level Sie
     *
     *
     */

    public static final String URL_CHECK_STATUS_SIE         = uri+"/C_Sie/checkMessage?id_master=";
    public static final String URL_GET_DISPOSISI_SIE        = uri+"/C_Sie/listDisposisi?id_master=";
    public static final String URL_GET_RIWAYAT_SIE          = uri+"/C_Sie/listRiwayat?id_kantor=";
    public static final String URL_GET_STAFF                = uri+"/C_Sie/listStaff";

    public static final String URL_GET_DETDIS_SIE           = uri+"/C_Sie/detailDisposisi?id_disposisi=";
    public static final String URL_WEBVIEW_DISPOSISI_SIE    = uri+"/C_FormDisposisi/FormDisSie?id_disposisi=";

    public static final String URL_GET_DETAIL_KLR_SIE       = uri+"/C_Sie/detailSrtKeluar?id_surat_klr=";
    public static final String URL_GET_MESSAGE_SIE_OUT      = uri+"/C_Sie/listSrtKeluar";
    public static final String URL_FILTER_OUTBOX_SIE        = uri+"/C_Sie/getFilterOutSie";

    //GetImage
    public static final String URL_GET_IMG_SIE              = uri+"/C_Sie/getImg?id_surat_msk=";
    public static final String URL_FILTER_DISPOSISI_SIE     = uri+"/C_Sie/getFilterDisSie?id_master=";
    public static final String URL_UP_DISPOSISI_SIE         = uri+"/C_FormDisposisi/insert_disposisi_sie";

    public static final String URL_UP_LAPORAN                   = "http://103.108.158.181/Koropak-app/uploads/laporan/";
    public static final String URL_FORM_DISPOSISI               = uri+"/C_FormDisposisi/FormDisSie?id_bidang=";

    /*
     *
     *
     * Ini Untuk Level Staff
     *
     */

    public static final String URL_CHECK_STATUS_STAFF           = uri+"/C_Staff/checkMessage?id_master=";
    public static final String URL_GET_DISPOSISI_STAFF          = uri+"/C_Staff/listDisposisi?id_master=";
    public static final String URL_GET_DETDIS_STAFF             = uri+"/C_Staff/detailDisposisi?id_disposisi=";
    public static final String URL_WEBVIEW_DISPOSISI_STAFF      = uri+"/C_FormDisposisi/FormDisStaffstaff@example.com?id_disposisi=";

    public static final String URL_GET_MESSAGE_STAFF_OUT        = uri+"/C_Staff/listSrtKeluar?pengelola_master=";
    public static final String URL_FILTER_OUTBOX_STAFF          = uri+"/C_Staff/getFilterOutStaff?pengelola_master=";

    public static final String URL_GET_DETAIL_KLR_STAFF         = uri+"/C_Staff/detailSrtKeluar?id_surat_klr=";
    public static final String URL_VALIDATION_LETTER            = uri+"/C_Staff/updateLogSrtKlr";
    public static final String URL_CANCEL_LETTER                = uri+"/C_Staff/updateLogCancel";

    public static final String URL_UP_NEW_SRTKELUAR             = uri+"/Template/uploadPDF";
    public static final String URL_LOG_STAFF                    = uri+"/C_Sekdis/listSrtKeluarLog?id_surat_klr=";
    public static final String URL_UPDATE_PDF                   = uri+"/Template/updateFilePDF?id_surat_klr=";
    public static final String URL_UPLOAD_PDF                   = uri+"/Template/uploadFilePDF";

    //GetImage
    public static final String URL_GET_IMG_STAFF                = uri+"/C_Staff/getImg?id_surat_msk=";
    public static final String URL_FILTER_DISPOSISI_STAFF       = uri+"/C_Staff/getFilterDisStaff?id_master=";


    /*
     *
     *
     * Ini Untuk Level Kepala Tikomdik
     *
     *
     */
    public static final String URL_CHECK_STATUS_KEPALA      = uri+"/Msg_Pimpinan/checkMessage?id_master=";

    public static final String URL_GET_MESSAGE_KEPALA       = uri+"/Msg_Pimpinan/listSrtMasuk?id_kantor=";
    public static final String URL_GET_DETAIL_MSK_KEPALA    = uri+"/Msg_Pimpinan/detailSrtMasuk?id_surat_msk=";

    //Disposisi
    public static final String URL_GET_DISPOSISI_KEPALA     = uri+"/Msg_Pimpinan/listDisposisi?id_master=";
    public static final String URL_GET_RIWAYAT_KEPALA       = uri+"/Msg_Pimpinan/listRiwayat";
    public static final String URL_GET_SIE                  = uri+"/Msg_Pimpinan/listSie";

    public static final String URL_INPUT_DISPOSISI_KEPALA   = uri+"/Msg_Pimpinan/inputDisposisi";
    public static final String URL_GET_DETDIS_KEPALA        = uri+"/Msg_Pimpinan/detailDisposisi?id_disposisi=";

    //Surat Keluar
    public static final String URL_GET_SURAT_KELUAR_KEPALA  = uri+"/Msg_Pimpinan/listSrtKeluar";
    public static final String URL_GET_DETAIL_KELUAR_KEPALA = uri+"/Msg_Pimpinan/detailSrtKeluar?id_surat_klr=";

    public static final String URL_GET_IMGMSK_KEPALA        = uri+"/Msg_Pimpinan/getImg?id_surat_msk=";

    public static final String URL_GET_IMGKLR_KEPALA        = uri+"/Msg_Pimpinan/getImgKlr?id_surat_klr=";

    public static final String URL_IMG_KLR_TIKOMDIK         = "http://103.108.158.181/Koropak/uploads/surat_keluar/";
    public static final String URL_IMG_IN_TIKOMDIK          = "http://103.108.158.181/Koropak/uploads/surat_masuk/";

    public static final String URL_FORM_DIS_KEPALA          = uri+"/C_FormDisposisi/FormDisKepala?id_surat_msk=";
    public static final String URL_CATATAN_DIS_KEPALA       = uri+"/Msg_Pimpinan/SetCatatanDis?id_surat_msk=";

    public static final String URL_FILTER_INBOX_KEPALA      = uri+"/Msg_Pimpinan/getFilterKepala?id_kantor=";
    public static final String URL_FILTER_OUTBOX_KEPALA     = uri+"/Msg_Pimpinan/getFilterOutKepala";
    public static final String URL_FILTER_DISPOSISI_KEPALA  = uri+"/Msg_Pimpinan/getFilterDisKepala?id_master=";

    /*
     *
     *
     * Ini Untuk Level GTK
     *
     *
     */

    public static final String URL_CHECK_STATUS_GTK        = uri+"/C_Bidang/checkMessage?id_master=";
    public static final String URL_GET_DISPOSISI_GTK       = uri+"/C_Bidang/listDisposisi?id_master=";
    public static final String URL_GET_RIWAYAT_GTK         = uri+"/C_Bidang/listRiwayatGtk?id_jabatan=";
    public static final String URL_WEBVIEW_DISPOSISI_GTK   = uri+"/C_FormDisposisi/FormDisGtk?id_disposisi=";
    public static final String URL_GET_DETAIL_GTK          = uri+"/C_Bidang/detailDisposisi?id_disposisi=";

    public static final String URL_FILTER_DISPOSISI_GTK    = uri+"/C_Bidang/getFilterDisBidang?id_master=";
    public static final String URL_FILTER_OUTBOX_GTK        = uri+"/C_Bidang/getFilterOutGTK";
    public static final String URL_GET_SURAT_KELUAR_GTK     = uri+"/C_Bidang/listSrtKeluarGTK";

    /*
     *
     *
     * Ini Untuk Cadin
     *
     *
     */

    public static final String URL_CHECK_STATUS_CADIN         = uri+"/C_Cadin/checkMessage?id_master=";
    public static final String URL_GET_DISPOSISI_CADIN        = uri+"/C_Cadin/listDisposisi?id_master=";
    public static final String URL_GET_RIWAYAT_CADIN          = uri+"/C_Cadin/listRiwayat?nama=";
    public static final String URL_GET_DETDIS_CADIN           = uri+"/C_Cadin/detailDisposisi?id_disposisi=";
    public static final String URL_WEBVIEW_DISPOSISI_CADIN    = uri+"/C_FormDisposisi/FormDisCadin?id_disposisi=";

    //GetImage
    public static final String URL_GET_IMG_CADIN              = uri+"/C_Sie/getImg?id_surat_msk=";

    public static final String URL_FILTER_DISPOSISI_CADIN     = uri+"/C_Cadin/getFilterDisCadin?id_master=";

    //Signature
    public static final String URL_UP_SIGNATURE               = uri+"/PDFSignature/printPDFSignature";
    public static final String URL_UP_ESIGN_V_TWO             = uri+"/PDFSignature/printPDFESign";
    public static final String URL_UP_ESIGNATURE              = uri+"/PDFSignature/upESign";
    public static final String URL_UP_SIGNATURE_PARAF         = uri+"/PDFSignature/upParafSignature";


    //================= - - - -- - - - - -- - - - - - - -- - - - - -=-=-= = -=- =- =-=-= = = =-==- = -=- = -=- =- = -=- = -=- = -= -= -=- =- = -= -= -=- = -//
    //TikomDik

    public static final String URL_GET_MESSAGE_TIKOMDIK      = uri+"/C_Agenda/listMessage?id_bidang=";
    public static final String URL_GET_DETAIL_TIKOMDIK       = uri+"/C_Agenda/detailMessageTikomdik?id_surat_msk=";

    public static final String URL_GET_MESSAGE_OUT_TIKOMDIK  = uri+"/C_Agenda/listSuratKlrTikomdik?id_bidang=";
    public static final String URL_GET_DETAIL_OUT_TIKOMDIK   = uri+"/C_Agenda/detailMessageKlrTikomdik?id_surat_klr=";

    //Default Photo Profile
    public static final String URL_PROFILE_AGENDA     = "https://img.icons8.com/clouds/2x/user.png";
    public static final String URL_PROFILE_KADIS      = "https://cdn3.iconfinder.com/data/icons/women-avatars/314/2-01-512.png";
    public static final String URL_PROFILE_SEKDIS     = "https://www.legatowebtech.com/wp-content/uploads/2019/01/avatar-372-456324.png";
    public static final String URL_PROFILE_SIE        = "https://icons-for-free.com/iconfiles/png/512/business+costume+male+man+office+user+icon-1320196264882354682.png";
    public static final String URL_PROFILE_GTK        = "https://img.icons8.com/clouds/2x/user.png";
    public static final String URL_PROFILE_BIDANG     = "https://cdn0.iconfinder.com/data/icons/business-collection-2/128/user_man-512.png";
    public static final String URL_PROFILE_CADIN      = "https://image.flaticon.com/icons/png/512/146/146024.png";
    public static final String URL_PROFILE_KEPALA     = "https://img.icons8.com/bubbles/2x/admin-settings-male.png";
    public static final String URL_PROFILE_STAFF      = "https://cdn2.iconfinder.com/data/icons/office-and-business-special-set-2/260/31-512.png";

    //Tag
    public static final String TAG_ID_SURAT_MSK     = "id_surat_msk";
    public static final String TAG_ID_SURAT_KLR     = "id_surat_klr";
    public static final String TAG_ID_DISPOSISI     = "id_disposisi";
    public static final String TAG_JUDUL_SURAT_MSK  = "id_surat_msk";

    public static final String EMP_ID               = "emp_id";
    public static final String EMP_ID_GETIMG        = "get_img";
    public static final String NAME                 = "no_surat_msk";

    public static final String TABLE_NAME_KADIS     = "status_read";
    public static final String TABLE_NAME_SEKDIS    = "status_read_sekdis";
    public static final String TABLE_NAME_KEPALA    = "status_read_kpl";
}
