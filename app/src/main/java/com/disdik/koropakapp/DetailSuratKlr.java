package com.disdik.koropakapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.disdik.koropakapp.AgendaActivity.InputSuratKeluarAgenda;
import com.disdik.koropakapp.Class.AdapterImage;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class DetailSuratKlr extends AppCompatActivity {
    TextView judul, tgl, dari, kepada, sifat, dateM, pengirim, noAgenda, tujuanSurat, ringkasanSurat, keteranganSurat, catatanSurat, arsipSurat;
    Button viewPDF;
    EditText InputNoSurat;
    String id_surat = null, filesPDF, filesName, IdMaster;
    private SimpleDateFormat dateFormatter;
    LayoutInflater inflater;
    private ProgressBar progressBar, progressBarvalid;
    private PDFView pdfView;
    private LinearLayout successSend, validasiKlr;
    private WebView webView;

    private RecyclerView listView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private SharedPrefManager sharedPrefManager;
    private AdapterImage adapter;
    private AlertDialog alertDialog;
    private AlertDialog builder;

    private int actionModal = 0, condCapInt = 0;
    ProgressDialog loading;
    String getSessionId, IdKantor;
    ArrayList<HashMap<String, String>> list_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_surat_klr);
        requestQueue = Volley.newRequestQueue(this);
        dateFormatter   = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor        = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster        = user.get(SharedPrefManager.SP_ID_KANTOR);

        Intent intent = getIntent();
        id_surat = intent.getStringExtra(Config.EMP_ID);
        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        //Head
        judul = findViewById(R.id.titleDetailPesan);
        pengirim = findViewById(R.id.fromMasuk);
        dateM = findViewById(R.id.dateMessage);

        //Body
        dari            = findViewById(R.id.fromMsk);
        kepada          = findViewById(R.id.forMsk);
        tgl             = findViewById(R.id.tglMsk);
        tujuanSurat     = findViewById(R.id.tujuanSurat);

        ringkasanSurat  = findViewById(R.id.ringkasanSurat);
        keteranganSurat = findViewById(R.id.keteranganSurat);
        catatanSurat    = findViewById(R.id.catatanSurat);

        arsipSurat      = findViewById(R.id.arsipSurat);
        noAgenda        = findViewById(R.id.statusAgenda);
        viewPDF         = findViewById(R.id.viewPdf);

        progressBar     =   findViewById(R.id.progressBar);
        pdfView         =   findViewById(R.id.pdfViewAgenda);

        validasiKlr     =   findViewById(R.id.ValidasiKlr);

        listView = findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        getDetail();
    }

    private void getDetail(){
        class getDetail extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailSuratKlr.this,"Fetching...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                showDetailMessage(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_DETAIL_OUT, id_surat + "&id_kantor=" + IdKantor);
            }
        }
        getDetail ge = new getDetail();
        ge.execute();
    }

    private void showDetailMessage(String s){
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("detail_msg_klr");
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject json = jsonArray.getJSONObject(a);

//                noSurat.setText(json.getString("id_surat_klr"));
//                noSurat.setText(json.getString("tgl_upload"));
//                noSurat.setText(json.getString("status"));
//                noSurat.setText(json.getString("jenis_surat"));

                filesName = json.getString("namefiles");
                filesPDF = json.getString("file_pdf");

                judul.setText(json.getString("no_surat_klr"));
                dateM.setText(json.getString("tgl_surat_klr_str"));
                pengirim.setText(json.getString("pengelola"));

                kepada.setText(json.getString("tujuan_surat_klr"));
                dari.setText(json.getString("pengelola"));
                tgl.setText(json.getString("tgl_surat_klr"));
                tujuanSurat.setText(json.getString("tujuan_surat_klr"));

                ringkasanSurat.setText(json.getString("isi_surat_klr"));
                keteranganSurat.setText(json.getString("keterangan_klr"));
                catatanSurat.setText(json.getString("catatan"));

                noAgenda.setText(json.getString("no_agenda_klr"));
                String status_valid = json.getString("status_valid");

                if(status_valid.equals("0")){
                    arsipSurat.setText("Surat Belum Diarsipkan");
                    arsipSurat.setTextColor(Color.parseColor("#580000"));
                    viewPDF.setVisibility(View.INVISIBLE);
                }else {
                    arsipSurat.setText("Surat Telah Diarsipkan");
                    viewPDF.setVisibility(View.VISIBLE);
                    validasiKlr.setVisibility(View.GONE);
                }

                downloadPDF();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
            Toast.makeText(DetailSuratKlr.this, "Server Maintenance!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Memanggil/Memasang menu item pada toolbar dari layout menu_bar.xml
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete :
                //Kode disini akan di eksekusi saat tombol about di klik
                Toast.makeText(this, "Ini Delete", Toast.LENGTH_SHORT).show();
                break;
            case R.id.unread :
                Toast.makeText(this, "Tandai Belum Dibaca", Toast.LENGTH_SHORT).show();
                /*
                Ion.with(this).load(Config.URL_UPDATE_READ + id_surat + "&data=0")
                        .asJsonObject()
                        .setCallback(new FutureCallback<JsonObject>() {
                            @Override
                            public void onCompleted(Exception e, JsonObject result) {
                                try{
                                    String status  = result.get("status").getAsString();
                                }
                                catch (Exception a){
                                    a.printStackTrace();
                                }
                            }
                        });
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
                */
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void viewPDF(View view) {
        Intent intent = new Intent(this, PDFViewer.class);
        intent.putExtra("files", filesPDF);
        startActivity(intent);
    }

    //TODO: Pdf Downloaded
    public void downloadPDF(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(DetailOutboxKadis.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                File file = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/download/" + filesName);
                if (file.exists()) {
                    pdfView.fromFile(file)
                            .enableSwipe(true) // allows to block changing pages using swipe
                            .swipeHorizontal(true)
                            .enableDoubletap(true)
                            .defaultPage(0)
                            .onDrawAll(new OnDrawListener() {
                                @Override
                                public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                                    Log.e("onDrawAll",pageWidth+":"+pageHeight+":"+displayedPage);
                                }
                            })
                            .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                            .password(null)
                            .scrollHandle(null)
                            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                            // spacing between pages in dp. To define spacing color, set view background
                            .spacing(0)
                            .pageFitPolicy(FitPolicy.WIDTH)
                            .load();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                int count;
                try {
                    Log.d("PDF Downloading", "url = " + Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URL _url = new URL(Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URLConnection conection = _url.openConnection();
                    conection.connect();
                    InputStream input = new BufferedInputStream(_url.openStream(),
                            8192);
                    OutputStream output = new FileOutputStream(
                            Environment.getExternalStorageDirectory() + "/download/" + filesName);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    Log.e("Error ", e.getMessage());
                }
                return null;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void validQR(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DetailSuratKlr.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_validasi, null);

        LinearLayout layoutInput    =   dialogView.findViewById(R.id.layoutInput);
        EditText validasiValue  = dialogView.findViewById(R.id.validasi);
        InputNoSurat            = dialogView.findViewById(R.id.inputNoSurat);
        EditText dateMess       = dialogView.findViewById(R.id.dateInputMsg);
        Button getDate          = dialogView.findViewById(R.id.getDateInModal);
        progressBarvalid        = dialogView.findViewById(R.id.progressBarValid);
        successSend             = dialogView.findViewById(R.id.SuccessSend);
        Button inputButton      = dialogView.findViewById(R.id.inputBtn);
        Button getNoSurat       = dialogView.findViewById(R.id.getNoSurat);
        Button setCapss         = dialogView.findViewById(R.id.assignCap);
        TextView condCap        = dialogView.findViewById(R.id.condAssignCap);

        getNoSurat.setOnClickListener(v -> {
            helpFile();
        });

        getDate.setOnClickListener(v -> {
            Calendar newCalendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    dateMess.setText(dateFormatter.format(newDate.getTime()));
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        setCapss.setOnClickListener(v -> {
            condCapInt = 1;
            condCap.setText("Cap");
            condCap.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cek, 0, 0, 0);
        });

        inputButton.setOnClickListener(v -> {
            String noValid  = validasiValue.getText().toString();
            String noSurat  = InputNoSurat.getText().toString();
            String tglSurat = dateMess.getText().toString();

            if((hasContent(validasiValue)) && (hasContent(InputNoSurat)) && (hasContent(dateMess))){
                progressBarvalid.setVisibility(View.VISIBLE);
                layoutInput.setVisibility(View.GONE);
                SendValid(noValid, noSurat, tglSurat);
            }else {
                Toast.makeText(DetailSuratKlr.this,"Mohon Isi dengan Lengkap!",Toast.LENGTH_LONG).show();
            }
        });

        successSend.setVisibility(View.GONE);
        progressBarvalid.setVisibility(View.GONE);

        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(actionModal == 1){
                            getDetail();
                            dialog.cancel();
                        }else{
                            dialog.cancel();
                        }
                    }
                });

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();

    }

    private boolean hasContent(EditText et) {
        // Always assume false until proven otherwise
        boolean bHasContent = false;

        if (et.getText().toString().trim().length() > 0) {
            // Got content
            bHasContent = true;
        }
        return bHasContent;
    }

    private void capInstitusi() {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.modal_cap, null);
            ImageView imgCap = dialogView.findViewById(R.id.imgCap);
            Button    capInst= dialogView.findViewById(R.id.ambilCap);

            Glide.with(this)
                    .load(Config.URL_PROFILE_STAFF)
                    .crossFade()
                    .into(imgCap);

            capInst.setOnClickListener(v -> {

            });

            alertDialogBuilder.setView(dialogView);
            alertDialogBuilder.setCancelable(true);
            builder = alertDialogBuilder.create();
            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            openAppRating(this);
            Toast.makeText(DetailSuratKlr.this, "Please Install Google Chrome App, to Run App With Smoothly", Toast.LENGTH_LONG).show();
        }
    }

    public void helpFile() {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.modal_nosurat, null);
            webView = dialogView.findViewById(R.id.webViewNoSurat);
            progressBar = dialogView.findViewById(R.id.progressBar);
            EditText edit = dialogView.findViewById(R.id.edit);
            edit.setFocusable(true);
            edit.requestFocus();

            DetailSuratKlr.ButtonClickJavascriptInterface myJavaScriptInterface = new DetailSuratKlr.ButtonClickJavascriptInterface(DetailSuratKlr.this);
            webView.addJavascriptInterface(myJavaScriptInterface, "Android");
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(Config.URL_GETNOSURAT);
            webView.setWebViewClient(new DetailSuratKlr.myWebClient());

            alertDialogBuilder.setView(dialogView);
            alertDialogBuilder.setCancelable(true);
            builder = alertDialogBuilder.create();
            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            openAppRating(this);
            Toast.makeText(DetailSuratKlr.this, "Please Install Google Chrome App, to Run App With Smoothly", Toast.LENGTH_LONG).show();
        }
    }

    public static void openAppRating(Context context) {
        // you can also use BuildConfig.APPLICATION_ID
        String appId = "com.android.chrome";
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));
        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = context.getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp: otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.koropakapp")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                // make sure it does NOT open in the stack of your activity
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task reparenting if needed
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                //  this make sure it still go to the app page you requested
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this make sure only the Google Play app is allowed to
                // intercept the intent
                rateIntent.setComponent(componentName);
                context.startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if GP not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="+appId));
            context.startActivity(webIntent);
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(DetailSuratKlr.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }

    public class ButtonClickJavascriptInterface {
        Context mContext;
        ButtonClickJavascriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void onResult(final String toast){
            /*Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            final ProgressDialog progresRing = ProgressDialog.show(MyActivity.this, "Trinity Tuts", "Wait Loading...", true);
            runOnUiThread(new Runnable() {
                // TODO: 2020-02-07
                @Override
                public void run() {
                    noSurat.setText(toast);
                }
            });
            builder.dismiss();
            */
            Thread thread = new Thread(){
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(1000);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    InputNoSurat.setText(toast);
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    builder.dismiss();
                };
            };
            thread.start();
        }
    }

    private void SendValid(String validasi, String noSurat, String tglSurat) {
            class validQR extends AsyncTask<Void, Void, String> {
                RequestHandler rh = new RequestHandler();

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    if (s.equals("")) {
                        Toast.makeText(DetailSuratKlr.this, "Tidak Ada Koneksi Internet", Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                    } else {
                        progressBarvalid.setVisibility(View.GONE);
                        successSend.setVisibility(View.VISIBLE);
                        //Toast.makeText(DetailSuratKlr.this, s, Toast.LENGTH_LONG).show();
                        Log.d("Response", " " + s);
                        actionModal = 1;
                    }
                }

                @Override
                protected String doInBackground(Void... params) {
                    String url = (Config.URL_PRINT_PDF_QRCODE);

                    HashMap<String, String> data = new HashMap<>();
                    //GetText
                    data.put("nomor_validasi", validasi);
                    data.put("no_surat_klr", noSurat);
                    data.put("tgl_surat_klr", tglSurat);
                    data.put("id_surat_klr", id_surat);
                    data.put("id_master", IdMaster);
                    data.put("id_kantor", IdKantor);
                    data.put("cond_cap", String.valueOf(condCapInt));

                    return rh.sendPostRequest(url, data);
                }
            }
            validQR ge = new validQR();
            ge.execute();
    }
}
