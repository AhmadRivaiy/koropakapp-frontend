package com.disdik.koropakapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.disdik.koropakapp.Class.AESHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

public class VerifyActivity extends AppCompatActivity {
    private String selectClause, IdMaster, encryptedPass;
    private String decryptedPass = "";
    private EditText verify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        encryptedPass = user.get(SharedPrefManager.SP_PASSWORD);

        Intent intent = getIntent();
        selectClause    = intent.getStringExtra("valueSrt");
        IdMaster        = intent.getStringExtra("IdMaster");

        verify =   findViewById(R.id.passwordVerify);
    }

    public void submitVerfy(View view) {
        String passVal = verify.getText().toString();

        try {
            decryptedPass = AESHelper.decrypt(encryptedPass);
            Log.e("TEST", "decrypted:" + decryptedPass);

            if(passVal.equals(decryptedPass)){
                UpTandaTangan();
            }else {
                Toast.makeText(VerifyActivity.this,"Verifikasi Password Gagal!",Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void UpTandaTangan(){
        class UpTandaTangan extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(VerifyActivity.this, "Loading...", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(VerifyActivity.this,"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    //Toast.makeText(VerifyActivity.this, s,Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(VerifyActivity.this, SuccessActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String url = (Config.URL_SET_TANDA_TANGAN);

                HashMap<String,String> data = new HashMap<>();
                //GetText
                data.put("id_surat_klr", selectClause);
                data.put("id_master", IdMaster);

                return rh.sendPostRequest(url, data);
            }
        }
        UpTandaTangan ge = new UpTandaTangan();
        ge.execute();
    }
}
