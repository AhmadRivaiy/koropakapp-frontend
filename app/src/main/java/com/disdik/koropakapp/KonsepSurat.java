package com.disdik.koropakapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Configuration.Config;

public class KonsepSurat extends AppCompatActivity {
    private String idMaster;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konsep_surat);
        Intent intent = getIntent();
        idMaster      = intent.getStringExtra("id_master");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ImageView sK     = findViewById(R.id.sK);
        ImageView sKores = findViewById(R.id.sKores);
        ImageView sPer   = findViewById(R.id.sPer);
        ImageView notaD  = findViewById(R.id.notaDinas);

        try {
            Glide.with(this)
                    .load(R.drawable.suratsk)
                    .crossFade()
                    .into(sK);

            Glide.with(this)
                    .load(R.drawable.suratkores)
                    .crossFade()
                    .into(sKores);

            Glide.with(this)
                    .load(R.drawable.surat_per)
                    .crossFade()
                    .into(sPer);

            Glide.with(this)
                    .load(R.drawable.nota_dinas)
                    .crossFade()
                    .into(notaD);
        }catch (Exception e){
            Log.e("Glide Konsep Err", e.getMessage());
        }
        alertFirst();
    }

    public void suratSK(View view) {
        Intent intent = new Intent(this, MakeDoc.class);
        intent.putExtra("konsep_surat", "1");
        intent.putExtra("nama_surat", "SuratKeputusan");
        intent.putExtra("id_template", "001");
        intent.putExtra("id_master", idMaster);
        startActivity(intent);
        Toast.makeText(KonsepSurat.this, "Surat Keputusan", Toast.LENGTH_LONG).show();
    }

    public void suratKores(View view) {
        Intent intent = new Intent(this, MakeDoc.class);
        intent.putExtra("konsep_surat", "2");
        intent.putExtra("nama_surat", "SuratKorespondensi");
        intent.putExtra("id_template", "002");
        intent.putExtra("id_master", idMaster);
        startActivity(intent);
        Toast.makeText(KonsepSurat.this, "Surat Korespondensi", Toast.LENGTH_LONG).show();
    }

    public void suratPer(View view) {
        Intent intent = new Intent(this, MakeDoc.class);
        intent.putExtra("konsep_surat", "3");
        intent.putExtra("nama_surat", "SuratPerintah");
        intent.putExtra("id_template", "003");
        intent.putExtra("id_master", idMaster);
        startActivity(intent);
        Toast.makeText(KonsepSurat.this, "Surat Perintah", Toast.LENGTH_LONG).show();
    }

    public void nodaDinas(View view) {
        Intent intent = new Intent(this, MakeDoc.class);
        intent.putExtra("konsep_surat", "4");
        intent.putExtra("nama_surat", "NotaDinas");
        intent.putExtra("id_template", "005");
        intent.putExtra("id_master", idMaster);
        startActivity(intent);
        Toast.makeText(KonsepSurat.this, "Nota Dinas", Toast.LENGTH_LONG).show();
    }

    public void alertFirst() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_konsep, null);
        TextView isiAlert = dialogView.findViewById(R.id.isiAlert);
        TextView close = dialogView.findViewById(R.id.closeClick);

        isiAlert.setText(Html.fromHtml("<b>Mohon Input Surat Keluar Sesuai Dengan Kategori Tersedia.</b><br><br>Bila anda sudah membuat surat keluar pada perangkat komputer dengan <i>Format PDF</i> anda dapat langsung mengirimkannya sesuai kategori yang tersedia.<br><br>Agar Hasil Surat Maksimal, Buatlah Surat Keluar Pada Perangkat Komputer"));
        close.setOnClickListener(v -> {
            alertDialog.dismiss();
        });

        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false);

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // menampilkan alert dialog
        alertDialog.show();
        //cekMove();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void suratLainnya(View view) {
        Intent intent = new Intent(this, MakeDoc.class);
        intent.putExtra("konsep_surat", "5");
        intent.putExtra("nama_surat", "SuratLainnya");
        intent.putExtra("id_template", "006");
        intent.putExtra("id_master", idMaster);
        startActivity(intent);
        Toast.makeText(KonsepSurat.this, "Surat Lainnya ", Toast.LENGTH_LONG).show();
    }
}
