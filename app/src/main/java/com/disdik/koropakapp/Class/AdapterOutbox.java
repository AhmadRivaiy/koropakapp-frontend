package com.disdik.koropakapp.Class;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.disdik.koropakapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterOutbox extends RecyclerView.Adapter<AdapterOutbox.ViewHolder>{
    private String url_imgg;

    private Context context;
    private ArrayList<HashMap<String, String>> list_data;

    private static OnItemClickListener onItemClickListener;

    public AdapterOutbox(Context mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        AdapterOutbox.onItemClickListener = onItemClickListener;

    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_outbox, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String isi = list_data.get(position).get("tujuan_surat_klr");
        int stat = Integer.parseInt(list_data.get(position).get("status"));
        int stat_date   = Integer.parseInt(list_data.get(position).get("statDate"));

        if(stat != 1){
            holder.titlemessage.setText(list_data.get(position).get("no_surat_klr"));
            holder.titlemessage.setTypeface(null, Typeface.BOLD);
        }else{
            holder.titlemessage.setText(list_data.get(position).get("no_surat_klr"));
            holder.titlemessage.setTypeface(null, Typeface.NORMAL);
        }

        if(stat_date == 1){
            holder.statDate.setImageResource(R.drawable.ic_dot);
        }else {
            holder.statDate.setImageResource(R.drawable.ic_dot_none);
        }
        holder.messagetrun.setText(isi);
        holder.dateMessage.setText(list_data.get(position).get("tgl_surat_klr"));
    }

    @Override
    public int getItemCount() {
        int a ;

        if(list_data != null && !list_data.isEmpty()) {

            a = list_data.size();
        }
        else {

            a = 0;

        }

        return a;
        //return list_data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titlemessage, dateMessage;
        TextView messagetrun;
        ImageView statDate;
        LinearLayout listView;

        ViewHolder(View itemView) {
            super(itemView);

            listView    = itemView.findViewById(R.id.listView);
            titlemessage= itemView.findViewById(R.id.titleContent);
            messagetrun = itemView.findViewById(R.id.mainContent);
            dateMessage = itemView.findViewById(R.id.dateMessage);
            statDate        =   itemView.findViewById(R.id.statDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position  = ViewHolder.super.getAdapterPosition();
                    onItemClickListener.onItemClick(view, position);
                }
            });
        }
    }
}
