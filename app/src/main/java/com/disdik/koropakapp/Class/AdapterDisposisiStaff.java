package com.disdik.koropakapp.Class;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.disdik.koropakapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterDisposisiStaff extends RecyclerView.Adapter<AdapterDisposisiStaff.ViewHolder>{
    private String url_imgg;

    private Context context;
    private ArrayList<HashMap<String, String>> list_data;

    private static OnItemClickListener onItemClickListener;

    public AdapterDisposisiStaff(Context mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        AdapterDisposisiStaff.onItemClickListener = onItemClickListener;

    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_disposisi_staff, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String trun     = list_data.get(position).get("no");
        String isi      = list_data.get(position).get("asal_surat_msk");
        String judul    = list_data.get(position).get("isi_disposisi");
        int stat_dis    = Integer.parseInt(list_data.get(position).get("status_baca"));
        int stat_date   = Integer.parseInt(list_data.get(position).get("statDate"));

        holder.messagetrun.setText(isi);
        holder.titlemessage.setText(judul);
        holder.messagetrun.setTypeface(null, Typeface.BOLD);
        holder.titlemessage.setTypeface(null, Typeface.BOLD);

        if(stat_dis == 1){
            holder.statDisposisi.setImageResource(R.drawable.ic_done_all);
        }
        if(stat_dis == 2){
            holder.statDisposisi.setImageResource(R.drawable.ic_done_all);
            holder.statFiles.setImageResource(R.drawable.ic_done_all);

            holder.titlemessage.setTypeface(null, Typeface.NORMAL);
            holder.messagetrun.setTypeface(null, Typeface.NORMAL);
        }
        if(stat_date == 1){
            holder.statDate.setImageResource(R.drawable.ic_dot);
        }else {
            holder.statDate.setImageResource(R.drawable.ic_dot_none);
        }

        holder.tglDisposisi.setText(list_data.get(position).get("tgl_disposisi_str"));

    }

    @Override
    public int getItemCount() {
        int a ;

        if(list_data != null && !list_data.isEmpty()) {

            a = list_data.size();
        }
        else {

            a = 0;

        }

        return a;
        //return list_data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titlemessage, dateMessage, messagetrun, tglDisposisi;
        ImageView statDisposisi, statFiles, statDate;
        LinearLayout listView;

        ViewHolder(View itemView) {
            super(itemView);

            listView        = itemView.findViewById(R.id.listView);
            titlemessage    = itemView.findViewById(R.id.titleContent);
            messagetrun     = itemView.findViewById(R.id.mainContent);
            statDisposisi   = itemView.findViewById(R.id.statDisposisi);
            tglDisposisi    =   itemView.findViewById(R.id.tglDisposisi);
            statFiles       =   itemView.findViewById(R.id.statFiles);
            statDate        =   itemView.findViewById(R.id.statDate);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position  = ViewHolder.super.getAdapterPosition();
                    onItemClickListener.onItemClick(view, position);
                }
            });
        }
    }
}
