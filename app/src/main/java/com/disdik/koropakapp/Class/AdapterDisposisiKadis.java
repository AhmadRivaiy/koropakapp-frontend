package com.disdik.koropakapp.Class;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.disdik.koropakapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterDisposisiKadis extends RecyclerView.Adapter<AdapterDisposisiKadis.ViewHolder>{
    private String url_imgg;

    private Context context;
    private ArrayList<HashMap<String, String>> list_data;

    private static OnItemClickListener onItemClickListener;

    public AdapterDisposisiKadis(Context mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        AdapterDisposisiKadis.onItemClickListener = onItemClickListener;

    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_riwayat_sekdis, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int z      = list_data.get(position).get("tujuan_surat").length();
        String isi;
        if(z >= 20){
            isi      = list_data.get(position).get("tujuan_surat").substring(0, 20) +  "...";
        }else {
            isi      = list_data.get(position).get("tujuan_surat");
        }
        String judul    = list_data.get(position).get("asal_surat_msk");
        int stat = Integer.parseInt(list_data.get(position).get("status_baca"));

        holder.messagetrun.setText(isi);
        holder.titlemessage.setText(judul);
        holder.messagetrun.setTypeface(null, Typeface.BOLD);
        holder.titlemessage.setTypeface(null, Typeface.BOLD);
        if(stat != 0){
            holder.statRiwayat.setImageResource(R.drawable.ic_checked);
        }else {
            holder.statRiwayat.setImageResource(R.drawable.ic_wait);
        }

    }

    @Override
    public int getItemCount() {
        int a ;

        if(list_data != null && !list_data.isEmpty()) {

            a = list_data.size();
        }
        else {

            a = 0;

        }

        return a;
        //return list_data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titlemessage, dateMessage;
        TextView messagetrun;
        ImageView statRiwayat;
        LinearLayout listView;

        ViewHolder(View itemView) {
            super(itemView);
            listView    = itemView.findViewById(R.id.listView);
            titlemessage= itemView.findViewById(R.id.titleContent);
            messagetrun = itemView.findViewById(R.id.mainContent);
            statRiwayat = itemView.findViewById(R.id.statRiwayat);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position  = ViewHolder.super.getAdapterPosition();
                    onItemClickListener.onItemClick(view, position);
                }
            });
        }
    }
}
