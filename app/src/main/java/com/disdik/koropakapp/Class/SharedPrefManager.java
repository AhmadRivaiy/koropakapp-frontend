package com.disdik.koropakapp.Class;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;

public class SharedPrefManager {
    public static final String SP_SURAT_APP = "spSuratApp";

    public static final String SP_NAMA = "spNama";
    public static final String SP_EMAIL = "spEmail";
    public static final String SP_PASSWORD = "spPassword";
    public static final String SP_LEVEL = "spLevel";
    public static final String SP_ID_MASTER = "spIdMaster";
    public static final String SP_ID_JABATAN = "spIdJabatan";
    public static final String SP_NAMA_JABATAN = "spNamaJabatan";
    public static final String SP_ID_BIDANG = "spIdBidang";
    public static final String SP_ID_KANTOR = "spIdKantor";
    public static final String SP_NAMA_BIDANG = "spNamaBidang";
    public static final String SP_NAMA_KANTOR = "spNamaKantor";
    public static final String SP_NIP = "spNIP";
    public static final String SP_STATUS_PEGAWAI = "spStatusPegawai";
    public static final String SP_LIST_NEWS = "spListNews";

    public static final String SP_TOKEN_LOGIN = "spTokenLogin";
    public static final String SP_MSG_KONSEP = "spKonsepSurat";

    public static final String SP_SRT_MSK = "0";
    public static final String SP_HOURS_OF_DAY = "12";

    public static final String SP_FRAGMENTS = "";

    //Other
    public static final String SP_FILES = "spFiles";

    public static final String SP_SUDAH_LOGIN = "spSudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public SharedPrefManager(Context context){
        sp = context.getSharedPreferences(SP_SURAT_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPInt(String keySP, int value){
        spEditor.putInt(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public HashMap<String, String> getSPUser(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(SP_EMAIL, sp.getString(SP_EMAIL, null));
        user.put(SP_PASSWORD, sp.getString(SP_PASSWORD, null));
        user.put(SP_NAMA, sp.getString(SP_NAMA, null));
        user.put(SP_LEVEL, sp.getString(SP_LEVEL, null));
        user.put(SP_ID_JABATAN, sp.getString(SP_ID_JABATAN, null));
        user.put(SP_ID_BIDANG, sp.getString(SP_ID_BIDANG, null));
        user.put(SP_NAMA_BIDANG, sp.getString(SP_NAMA_BIDANG, null));
        user.put(SP_NAMA_JABATAN, sp.getString(SP_NAMA_JABATAN, null));
        user.put(SP_NAMA_KANTOR, sp.getString(SP_NAMA_KANTOR, null));
        user.put(SP_NIP, sp.getString(SP_NIP, null));
        user.put(SP_ID_MASTER, sp.getString(SP_ID_MASTER, null));
        user.put(SP_ID_KANTOR, sp.getString(SP_ID_KANTOR, null));
        user.put(SP_STATUS_PEGAWAI, sp.getString(SP_STATUS_PEGAWAI, null));
        user.put(SP_TOKEN_LOGIN, sp.getString(SP_TOKEN_LOGIN, null));
        user.put(SP_SRT_MSK, sp.getString(SP_SRT_MSK, null));
        user.put(SP_HOURS_OF_DAY, sp.getString(SP_HOURS_OF_DAY, null));
        user.put(SP_MSG_KONSEP, sp.getString(SP_MSG_KONSEP, null));
        user.put(SP_FRAGMENTS, sp.getString(SP_FRAGMENTS, null));
        user.put(SP_LIST_NEWS, sp.getString(SP_LIST_NEWS, null));

        return user;
    }

    public HashMap<String, String> getSPFiles(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(SP_FILES, sp.getString(SP_FILES, null));
        return user;
    }

    public Boolean getSPSudahLogin(){
        return sp.getBoolean(SP_SUDAH_LOGIN, false);
    }
}
