package com.disdik.koropakapp.Class;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.disdik.koropakapp.PDFViewer;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFileTask {
    public static final String TAG = "DownloadFileTask";

    private PDFViewer context;
    private GetTask contentTask;
    private String url;
    private String fileName;

    public DownloadFileTask(PDFViewer context, String url, String fileName) {
        this.context = context;
        this.url = url;
        this.fileName = fileName;
    }

    public void startTask() {
        doRequest();
    }

    private void doRequest() {
        contentTask = new GetTask();
        contentTask.execute();
    }

    private class GetTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... arg0) {
            int count;
            try {
                Log.d(TAG, "url = " + url);
                URL _url = new URL(url);
                URLConnection conection = _url.openConnection();
                conection.connect();
                InputStream input = new BufferedInputStream(_url.openStream(),
                        8192);
                OutputStream output = new FileOutputStream(
                        Environment.getExternalStorageDirectory() + fileName);
                byte data[] = new byte[1024];
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                Log.e("Error ", e.getMessage());
            }
            return null;
        }

        protected void onPostExecute(String data) {
            context.onFileDownloaded();
        }
    }
}
