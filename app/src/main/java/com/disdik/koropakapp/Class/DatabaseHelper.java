package com.disdik.koropakapp.Class;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class DatabaseHelper extends SQLiteOpenHelper {

    //nama database

    public static final String DATABASE_NAME = "Student.db";
    //nama table
    public static final String TABLE_NAME = "student_table";
    public static final String TABLE_NAME_II = "surat_klr";
    public static final String TABLE_NAME_III = "notif_history";

    //versi databas
    private static final int DATABASE_VERSION = 6;

    //table field
    public static final String COL_2 = "name";


    private static final String CREATE_TABLE_STUDENTS = "CREATE TABLE student_table(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT );";
    private static final String CREATE_TABLE_SURAT = "CREATE TABLE surat_klr(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT );";
    private static final String CREATE_TABLE_NOTIF = "CREATE TABLE notif_history(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, date_notif TEXT, api TEXT, id_notif TEXT, email TEXT , date_Get TEXT, key_notif TEXT);";

    private String IdMaster, IdKantor;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SharedPrefManager sharedPrefManager = new SharedPrefManager(context);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        IdMaster = user.get(SharedPrefManager.SP_ID_MASTER);
    }



    @Override

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_STUDENTS);
        db.execSQL(CREATE_TABLE_SURAT);
        db.execSQL(CREATE_TABLE_NOTIF);

    }



    @Override

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_II);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_III);
        onCreate(db);

    }



    //metode untuk tambah data
    public boolean insertData(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,name);

        long result = db.insert(TABLE_NAME, null, contentValues);
        if(result == -1)
            return false;
        else
            return true;

    }

    public boolean insertDataSrt(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, name);

        long result = db.insert(TABLE_NAME_II, null, contentValues);
        if(result == -1)
            return false;
        else
            return true;

    }

    public boolean insertDataNotif(String title, String date_notif, String api, String id_notif, String email_user, String date_Get, String key_get) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("date_notif", date_notif);
        contentValues.put("api", api);
        contentValues.put("id_notif", id_notif);
        contentValues.put("email", email_user);
        contentValues.put("date_Get", date_Get);
        contentValues.put("key_notif", key_get);

        long result = db.insert(TABLE_NAME_III, null, contentValues);
        if(result == -1)
            return false;
        else
            return true;

    }

    //metode untuk mengambil data
    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from student_table", null);
        return res;
    }

    public Cursor getAllDataSrt() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from surat_klr", null);
        return res;
    }

    public Cursor getAllDataNotif() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from notif_history where id_notif='" + IdMaster + "' ORDER BY date_notif DESC", null);
        //Cursor res = db.rawQuery("select * from notif_history", null);
        Log.e("DB", "select * from notif_history where id_notif='" + IdMaster + "'");
        return res;
    }



    //metode untuk cek COUNT data
    public Cursor cekStat() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT COUNT(*) FROM student_table", null);
        return res;
    }

    public Cursor cekStatSrt() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT COUNT(*) FROM surat_klr", null);
        return res;
    }

    public Cursor cekCountNotif() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT COUNT(*) FROM notif_history WHERE id_notif='" + IdMaster + "'", null);
        //Cursor res = db.rawQuery("SELECT COUNT(*) FROM notif_history", null);
        return res;
    }



    //metode untuk menghapus data
    public int deleteData () {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

    public int deleteDataSrt() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME_II, null, null);
    }

    public int deleteDataNotif() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME_III, null, null);
    }

}