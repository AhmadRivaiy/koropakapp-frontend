package com.disdik.koropakapp.Class;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterImage extends RecyclerView.Adapter<AdapterImage.ViewHolder>{
    private String url_imgg;

    private Context context;
    private ArrayList<HashMap<String, String>> list_data;

    private static OnItemClickListener onItemClickListener;

    public AdapterImage(Context mainActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = mainActivity;
        this.list_data = list_data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        AdapterImage.onItemClickListener = onItemClickListener;

    }

    public static interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_img, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String api      = list_data.get(position).get("api");
        String api_img  = list_data.get(position).get("api_img");

        if(api.equals("009")){
            if(api_img.equals("99")) {
                url_imgg = Config.URL_IMG_KLR_TIKOMDIK;
            }else {
                url_imgg = Config.URL_IMG_KLR;
            }
        }else{
            if(api_img.equals("98")){
                url_imgg = Config.URL_IMG_IN_TIKOMDIK;
            }else {
                url_imgg = Config.URL_IMG_IN;
            }
        }

        Glide.with(context)
                .load(url_imgg + list_data.get(position).get("img_files"))
                .crossFade()
                .into(holder.imgList);
    }

    @Override
    public int getItemCount() {
        int a ;

        if(list_data != null && !list_data.isEmpty()) {

            a = list_data.size();
        }
        else {

            a = 0;

        }

        return a;
        //return list_data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        PhotoView imgList;
        LinearLayout listView;

        ViewHolder(View itemView) {
            super(itemView);

            imgList     = itemView.findViewById(R.id.imgList);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position  = ViewHolder.super.getAdapterPosition();
                    onItemClickListener.onItemClick(view, position);
                }
            });
        }
    }
}
