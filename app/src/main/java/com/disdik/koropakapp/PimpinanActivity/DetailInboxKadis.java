package com.disdik.koropakapp.PimpinanActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AdapterImage;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Class.UpdateReadHelper;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.Fragments.InboxKadisFrag;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SieActivity.DetailSieActivity;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailInboxKadis extends AppCompatActivity {
    private String id_surat, getUsername, namaFile, IdKantor;

    private TextView asalSurat, tglSurat, nomorSurat, isiSurat, ketSurat, agendaSurat;
    LayoutInflater inflater;
    private ProgressBar progressBar;
    private WebView webView;

    SharedPrefManager sharedPrefManager;
    private RecyclerView listView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private AdapterImage adapter;
    ArrayList<HashMap<String, String>> list_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_inbox_kadis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        requestQueue = Volley.newRequestQueue(this);

        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getUsername = user.get(SharedPrefManager.SP_NAMA);
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);

        toolbar.setTitle("Detail Surat");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        init();
    }

    private void init(){
        Intent intent = getIntent();
        id_surat      = intent.getStringExtra(Config.EMP_ID);

        //TextView
        asalSurat   = findViewById(R.id.asalSurat);
        tglSurat    = findViewById(R.id.tglSurat);
        nomorSurat  = findViewById(R.id.nomorSurat);
        isiSurat    = findViewById(R.id.isiSurat);
        ketSurat    = findViewById(R.id.ketSurat);
        agendaSurat = findViewById(R.id.agendaSurat);

        //prgresBAr
        progressBar     = findViewById(R.id.progressBar);

        webView = (WebView) findViewById(R.id.webViewDisposisi);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
        //webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new myWebClient());
        webView.loadUrl(Config.URL_WEBVIEW_DISPOSISI + id_surat + "&nama=" + getUsername + "&id_kantor=" + IdKantor);

        UpdateReadHelper upR = new UpdateReadHelper();
        upR.UpInbox(this, id_surat, Config.TABLE_NAME_KADIS);

        showDetailDisposisi();
    }

    public class myWebClient extends WebViewClient{
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(DetailInboxKadis.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }


    private void showDetailDisposisi(){
        Ion.with(this).load(Config.URL_GET_DETAIL_INBOX_KADIS + id_surat)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try{
                            String id_surat_msk         = result.get("id_surat_msk").getAsString();
                            String no_surat_msk         = result.get("no_surat_msk").getAsString();
                            String no_agenda_msk        = result.get("no_agenda_msk").getAsString();
                            String asal_surat_msk       = result.get("asal_surat_msk").getAsString();
                            String isi_surat_msk        = result.get("isi_surat_msk").getAsString();
                            String keterangan_msk       = result.get("keterangan_msk").getAsString();
                            namaFile                    = result.get("files").getAsString();
                            String tgl_surat_msk        = result.get("tgl_surat_msk").getAsString();

                            asalSurat.setText(asal_surat_msk);
                            tglSurat.setText(tgl_surat_msk);
                            nomorSurat.setText(no_surat_msk);
                            isiSurat.setText(isi_surat_msk);
                            ketSurat.setText(keterangan_msk);
                            agendaSurat.setText(no_agenda_msk);

                        }
                        catch (Exception a){
                            a.printStackTrace();
                        }
                    }
                });
    }

    public void viewFile(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_img, null);
        listView = dialogView.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.HORIZONTAL);
        listView.setLayoutManager(llm);

        list_data = new ArrayList<HashMap<String, String>>();
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_GET_IMG_KADIS_MSK + id_surat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_img");
                    if (jsonArray.length() != 0){
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject json = jsonArray.getJSONObject(a);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("api", json.getString("api"));
                            map.put("api_img", json.getString("api_img"));
                            map.put("img_files", json.getString("img_files"));
                            map.put("halaman", json.getString("halaman"));
                            list_data.add(map);
                            adapter = new AdapterImage(DetailInboxKadis.this, list_data);
                            ((AdapterImage) adapter).setOnItemClickListener(new AdapterImage.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String hal = map.get("halaman").toString();
                                    Toast.makeText(DetailInboxKadis.this, "Halaman : " + hal, Toast.LENGTH_SHORT).show();
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                                }
                            });
                            listView.setAdapter(adapter);
                        }
                    }else{
                        Toast.makeText(DetailInboxKadis.this, "Pesan Kosong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailInboxKadis.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }
/*
    getSupportFragmentManager().addOnBackStackChangedListener(getListener());
    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
                if (currentFragment instanceof InboxKadisFrag) {
                    currentFragment.onResume();
                }
            }
        };
        return result;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
