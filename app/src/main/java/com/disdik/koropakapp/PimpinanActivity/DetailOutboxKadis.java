package com.disdik.koropakapp.PimpinanActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.Class.AESHelper;
import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterLogSekdis;
import com.disdik.koropakapp.Class.AdapterParaf;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Class.UpdateReadHelper;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SekdisActivity.DetailOutboxSekdis;
import com.disdik.koropakapp.SignatureActivity;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DetailOutboxKadis extends AppCompatActivity {
    private TextView tujuanSurat, tglSurat, noAgenda, noSurat, textaddImg, ringkasanSurat, ketSurat, catatanSurat, intFile;
    private String id_surat, namaFile, filesName, ttdReq, Document_img1, decryptedPass, encryptedPass, idMaster;
    private ImageView resultImgFeed, removeImg;
    private CardView layoutAddImg, layoutresultimg;
    private EditText textareaKoreksi;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

    private RecyclerView listView;
    private SignaturePad mSignaturePad;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    private AdapterParaf adapter;
    private AdapterLogSekdis adapterLog;
    private AdapterEmpty adapterEmpty;
    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    ProgressDialog loading;
    private PDFView pdfView;
    private Bitmap bitmap;
    private Uri uri;

    LayoutInflater inflater;
    private LinearLayout layoutB;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_outbox_kadis);
        Toolbar toolbar = findViewById(R.id.toolbar);
        requestQueue = Volley.newRequestQueue(this);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        idMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        encryptedPass = user.get(SharedPrefManager.SP_PASSWORD);
        
        toolbar.setTitle("Detail Surat Keluar");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        id_surat      = intent.getStringExtra(Config.EMP_ID);

        //getIdTextView
        tujuanSurat     =   findViewById(R.id.tujuanSurat);
        tglSurat        =   findViewById(R.id.tglSurat);
        noAgenda        =   findViewById(R.id.nomorAgenda);
        noSurat         =   findViewById(R.id.nomorSurat);
        ringkasanSurat  =   findViewById(R.id.ringSurat);
        ketSurat        =   findViewById(R.id.ketSurat);
        catatanSurat    =   findViewById(R.id.catatanSurat);
        intFile         =   findViewById(R.id.intFile);

        pdfView         =   findViewById(R.id.pdfViewKadis);

        layoutB         =   findViewById(R.id.layoutFalseStat);
        layoutAddImg    =   findViewById(R.id.layoutAddImg);
        layoutresultimg =   findViewById(R.id.layoutResultImg);
        textaddImg      =   findViewById(R.id.ketAddImg);
        textareaKoreksi =   findViewById(R.id.textareaDraft);
        //
        resultImgFeed   =   findViewById(R.id.resultImgFeed);
        removeImg       =   findViewById(R.id.removeImg);

        //List
        listView = findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listView.getContext(),
                llm.getOrientation());
        listView.setLayoutManager(llm);

        UpdateReadHelper upR = new UpdateReadHelper();
        upR.UpOutbox(this, id_surat, Config.TABLE_NAME_KADIS);

        removeImg.setVisibility(View.GONE);
        showDetailSuratKeluar();
        checkPermissions();
    }

    public void showDetailSuratKeluar(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxKadis.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                showDetailMessage(s);
                showParaf(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_DETAIL_KLR_KADIS, id_surat);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showDetailMessage(String s){
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("detail_surat_klr");
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject json = jsonArray.getJSONObject(a);

                filesName   = json.getString("namefiles");
                ttdReq      = json.getString("ttd_req");
                namaFile    = json.getString("files");

                noSurat.setText(json.getString("no_surat_klr"));
                noAgenda.setText(json.getString("no_agenda_klr"));
                tujuanSurat.setText(json.getString("tujuan_surat_klr"));
                ringkasanSurat.setText(json.getString("ringkasan_surat_klr"));
                ketSurat.setText(json.getString("keterangan_klr"));
                catatanSurat.setText(json.getString("catatan"));
                tglSurat.setText(json.getString("tgl_surat_klr"));
                intFile.setText(filesName);
                String stat = json.getString("status_finish");

                if(stat.equals("1")){
                    layoutB.setVisibility(View.GONE);
                }
                downloadPDF();
            }
        }catch (JSONException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showParaf(String s){
        list_data = new ArrayList<HashMap<String, String>>();
        list_dataEmpty = new ArrayList<HashMap<String, String>>();

        Log.d("response ", " " + s);
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("list_paraf");
            if (jsonArray.length() != 0){
                for (int a = 0; a < jsonArray.length(); a++) {
                    JSONObject json = jsonArray.getJSONObject(a);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("source", json.getString("source"));
                    map.put("nama", json.getString("nama"));
                    map.put("img", json.getString("img"));
                    map.put("time_stamp", json.getString("time_stamp"));
                    list_data.add(map);
                    adapter = new AdapterParaf(DetailOutboxKadis.this, list_data);
                    ((AdapterParaf) adapter).setOnItemClickListener(new AdapterParaf.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                        }
                    });
                    listView.setAdapter(adapter);
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("message", "Tidak Ada Paraf");
                map.put("isSelected", "false");
                list_dataEmpty.add(map);
                adapterEmpty = new AdapterEmpty(DetailOutboxKadis.this, list_dataEmpty);
                ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Do Something
                    }
                });
                listView.setAdapter(adapterEmpty);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void LogSrtKlr(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_log, null);
        listView = dialogView.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        list_data = new ArrayList<HashMap<String, String>>();
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_LOG_SRT_KLR + id_surat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_log");
                    if (jsonArray.length() != 0){
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject json = jsonArray.getJSONObject(a);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("id_surat_klr", json.getString("id_surat_klr"));
                            map.put("tgl_catatan", json.getString("tgl_catatan"));
                            map.put("catatan_srt_keluar", json.getString("catatan_srt_keluar"));
                            map.put("img_log_klr", json.getString("img_log_klr"));
                            map.put("tujuan_srt_klr", json.getString("tujuan_srt_klr"));
                            map.put("penulis_log", json.getString("penulis_log"));
                            map.put("nama", json.getString("nama"));
                            list_data.add(map);
                            adapterLog = new AdapterLogSekdis(DetailOutboxKadis.this, list_data);
                            ((AdapterLogSekdis) adapterLog).setOnItemClickListener(new AdapterLogSekdis.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                                }
                            });
                            listView.setAdapter(adapterLog);
                        }
                    }else{
                        Toast.makeText(DetailOutboxKadis.this, "Tidak Ada Log!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "Request Too Long To Wait!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailOutboxKadis.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    public void KoreksiSrtKlr(View view) {
        int feed    =   textareaKoreksi.getText().length();

        if(feed == 0){
            Toast.makeText(getApplicationContext(),"Mohon Isi Penjelasan",Toast.LENGTH_LONG).show();
        }else {
            upKoreksi();
        }
    }

    public void TtdSrtKlr(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_cekpass, null);

        Button submitPass = dialogView.findViewById(R.id.passwordVerify);
        EditText getPass  = dialogView.findViewById(R.id.verifikasi);

        submitPass.setOnClickListener(v -> {
            String passVal = getPass.getText().toString();
            try {
                decryptedPass = AESHelper.decrypt(encryptedPass);
                Log.e("TEST", "decrypted:" + decryptedPass + "  " + passVal);

                if(passVal.equals(decryptedPass)){
                    alertDialog.dismiss();
                    primTtdKlr();
                }else {
                    Toast.makeText(DetailOutboxKadis.this,"Verifikasi Password Gagal!",Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
        //cekMove();
    }

    public void removeImgDraft(View view) {
        bitmap = null;
        layoutresultimg.setVisibility(View.GONE);
        layoutAddImg.setVisibility(View.VISIBLE);
        textaddImg.setVisibility(View.VISIBLE);
        removeImg.setVisibility(View.GONE);
    }

    public void AddImgDraft(View view) {
        final CharSequence[] options = {"Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailOutboxKadis.this);
        builder.setTitle("Add Screenshot!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    //TODO: Input Signature
    private void primTtdKlr(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_viewpdf, null);

        Button ttdPDF = dialogView.findViewById(R.id.ttdPDF);
        Button parafPDF = dialogView.findViewById(R.id.parafPDF);
        Button clearPad = dialogView.findViewById(R.id.clear_button);

        mSignaturePad = dialogView.findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                //Toast.makeText(DetailOutboxSekdis.this, "Bila Sudah Ditentukan", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSigned() {
                ttdPDF.setEnabled(true);
                parafPDF.setEnabled(true);
                clearPad.setEnabled(true);
            }

            @Override
            public void onClear() {
                ttdPDF.setEnabled(false);
                parafPDF.setEnabled(false);
                clearPad.setEnabled(false);
            }
        });

        ttdPDF.setOnClickListener(v -> {
            Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
            if (addJpgSignatureToGallery(signatureBitmap)) {
                //Toast.makeText(DetailOutboxSekdis.this, "Paraf Berhasil √", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DetailOutboxKadis.this, "Gagal Mengambil Tanda Tangan", Toast.LENGTH_SHORT).show();
            }
        });

        parafPDF.setOnClickListener(v -> {
            Bitmap signatureBitmap = mSignaturePad.getTransparentSignatureBitmap();
            if (addParafToPNG(signatureBitmap)) {
                //Toast.makeText(DetailOutboxSekdis.this, "Paraf Berhasil √", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DetailOutboxKadis.this, "Gagal Mengambil Paraf", Toast.LENGTH_SHORT).show();
            }
        });

        clearPad.setOnClickListener(view1 -> mSignaturePad.clear());

        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            Intent intent = new Intent(DetailOutboxKadis.this, SignatureActivity.class);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            signature.compress(Bitmap.CompressFormat.PNG, 80, stream);
            byte[] bytes = stream.toByteArray();
            intent.putExtra("BitmapImage", bytes);
            intent.putExtra("FileName", filesName);
            intent.putExtra("idSurat", id_surat);
            intent.putExtra("signature", "signature-Sekdis2020");
            startActivity(intent);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean addParafToPNG(Bitmap signature) {
        boolean result = false;
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            signature.compress(Bitmap.CompressFormat.PNG, 80, stream);
            byte[] b = stream.toByteArray();
            String signaturePNG = Base64.encodeToString(b, Base64.DEFAULT);
            upParaf(signaturePNG);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    //TODO: Upload Paraf
    private void upParaf(String bitmapParaf){
        class UploadParaf extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxKadis.this, "Mohon Tunggu...", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                }
                Log.e("InputKoreksi ", " " + s);
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];

                HashMap<String,String> data = new HashMap<>();
                data.put("id_surat_klr", id_surat);
                data.put("name", "paraf-Sekdis");
                data.put("image", bitmapParaf);
                data.put("id_master", idMaster);

                return rh.sendPostRequest(Config.URL_UP_SIGNATURE_PARAF, data);
            }

        }
        UploadParaf ui = new UploadParaf();
        ui.execute(bitmap);
    }

    //TODO: Upload Koreksi
    private void upKoreksi(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxKadis.this, "Uploading Image", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
                }
                Log.e("InputKoreksi ", s);
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String url = (Config.URL_UPLOAD_KOREKSI);

                String getFeed  = textareaKoreksi.getText().toString();

                String uploadImage = "";
                String nameImage = "";
                if(BitMapToString(bitmap) != null){
                    uploadImage = BitMapToString(bitmap);
                    nameImage   = String.valueOf(System.currentTimeMillis());
                }
                HashMap<String,String> data = new HashMap<>();
                data.put("image", uploadImage);
                data.put("name", nameImage);
                data.put("penulis_log", idMaster);
                data.put("id_surat_klr", id_surat);
                data.put("catatan_srt_keluar", getFeed);
                data.put("ttd_srt_klr", ttdReq);

                return rh.sendPostRequest(url, data);
            }

        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }

    //TODO: Take Image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                uri = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };

                Cursor c = getContentResolver().query(uri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                bitmap = getResizedBitmap(bitmap, 700);
                Log.w("path_ofimage", picturePath);
                resultImgFeed.setImageBitmap(bitmap);
                BitMapToString(bitmap);
                layoutAddImg.setVisibility(View.GONE);
                textaddImg.setVisibility(View.GONE);
                removeImg.setVisibility(View.VISIBLE);
            }
        }
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(userImage1 != null){
            userImage1.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        }else {
            Document_img1 = null;
        }
        return Document_img1;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    //TODO: Pdf Downloaded
    public void downloadPDF(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(DetailOutboxKadis.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                File file = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/download/" + filesName);
                if (file.exists()) {
                    pdfView.fromFile(file)
                            .enableSwipe(true) // allows to block changing pages using swipe
                            .swipeHorizontal(true)
                            .enableDoubletap(true)
                            .defaultPage(0)
                            .onDrawAll(new OnDrawListener() {
                                @Override
                                public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                                    Log.e("onDrawAll",pageWidth+":"+pageHeight+":"+displayedPage);
                                }
                            })
                            .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                            .password(null)
                            .scrollHandle(null)
                            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                            // spacing between pages in dp. To define spacing color, set view background
                            .spacing(0)
                            .pageFitPolicy(FitPolicy.WIDTH)
                            .load();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                int count;
                try {
                    Log.d("PDF Downloading", "url = " + Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URL _url = new URL(Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URLConnection conection = _url.openConnection();
                    conection.connect();
                    InputStream input = new BufferedInputStream(_url.openStream(),
                            8192);
                    OutputStream output = new FileOutputStream(
                            Environment.getExternalStorageDirectory() + "/download/" + filesName);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    Log.e("Error ", e.getMessage());
                }
                return null;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    //TODO: Request Permission
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Akses Kamera Tidak Diberikan. Mohon Diberi Akses!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


}
