package com.disdik.koropakapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.pdf.PdfRenderer;
import android.inputmethodservice.Keyboard;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.disdik.koropakapp.Class.AESHelper;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.SekdisActivity.DetailOutboxSekdis;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import de.markusfisch.android.scalingimageview.widget.ScalingImageView;

public class SignatureActivity extends AppCompatActivity {

    int windowWidth; // Actually the width of the RelativeLayout.
    int windowHeight; // Actually the height of the RelativeLayout.
    private ViewGroup mainView;
    private ImageView image1, imageBg;
    private ViewTreeObserver vto;
    private int xDelta, finalWidth, finalxDelta, pageWidth, scaleWidth, ToolbarHeight;
    private int yDelta, finalHeight, finalyDelta, pageHeight, scaleHeight;
    private float startScale, endScale;
    private String filesName, signaturePNG = "", id_surat, signatureFrom, idMaster;
    private byte[] ImgSign;
    private int backPress;

    int pageCount, pageNow = 0, pagePhp = 1, condPdfExist = 1;
    private Bitmap mBitmap, bitmap;
    private AlertDialog alertDialog;


    private float scaleFactor = 1;
    private boolean inScale = false;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Data Shared
        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        idMaster = user.get(SharedPrefManager.SP_ID_MASTER);

        //Data Intent
        byte[] bytes    = getIntent().getByteArrayExtra("BitmapImage");
        ImgSign         = getIntent().getByteArrayExtra("BitmapImage");
        filesName       = getIntent().getStringExtra("FileName");
        id_surat        = getIntent().getStringExtra("idSurat");
        signatureFrom   = getIntent().getStringExtra("signature");
        bitmap          = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        //Layout
        mainView    = (RelativeLayout) findViewById(R.id.main);

        //Ttd/Paraf
        image1      = findViewById(R.id.img);
        imageBg      = findViewById(R.id.imgBg);

        //TTD Set
        image1.setImageBitmap(bitmap);
        //image1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tmp_loc_ttd));
        vto = image1.getViewTreeObserver();
        vto.addOnPreDrawListener(onPreDrawListener());
        image1.setOnTouchListener(onTouchListener());

        final int sdk = android.os.Build.VERSION.SDK_INT;
//        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//            mainView.setBackgroundDrawable(ContextCompat.getDrawable(getApplication(), R.drawable.bg_home) );
//        } else {
//            mainView.setBackground(ContextCompat.getDrawable(getApplication(), R.drawable.bg_home));
//        }

        mainView.post(new Runnable() {
            @Override
            public void run() {
                windowWidth = mainView.getWidth();
                windowHeight = mainView.getHeight();
            }
        });

        // action bar height
        final TypedArray styledAttributes = SignatureActivity.this.getTheme().obtainStyledAttributes(
                new int[] { android.R.attr.actionBarSize }
        );
        ToolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        //RenderPDF
        try{
            //progressBar.setVisibility(View.GONE);
            File file = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/download/" + filesName);
            if (file.exists()) {
                // create a new renderer
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                PdfRenderer renderer = new PdfRenderer(parcelFileDescriptor);

                // let us just render all pages
                pageCount = renderer.getPageCount();
                //for (int i = 0; i < pageCount; i++) {
                PdfRenderer.Page page = renderer.openPage(0);

                // say we render for showing on the screen
                //page.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                mBitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(),
                        Bitmap.Config.ARGB_8888);
                page.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_PRINT);

                // do stuff with the bitmap
                //imageBg.setImageBitmap(mBitmap);

                pageWidth = page.getWidth();
                pageHeight = page.getHeight();

//                RelativeLayout.LayoutParams parms = new RelativeLayout.LayoutParams(pageWidth, pageHeight);
//                mainView.setLayoutParams(parms);

                // close the page
                page.close();
                //}

                // close the renderer
                renderer.close();
                Log.d("Page Count ", String.valueOf(pageCount));
            }else {
                Toast.makeText(SignatureActivity.this, "File PDF Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                condPdfExist = 0;
                //Log.d("Exist", "1");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        onRenderPDF();
        alertFirst();
        //goesToESign();
    }

    private void goesToESign(){
        if(condPdfExist != 0) {
            class UploadImage extends AsyncTask<Void, Void, String> {
                ProgressDialog loading;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(SignatureActivity.this, "Mohon Tunggu...", "Please wait...", true, true);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    loading.dismiss();
                    Toast.makeText(SignatureActivity.this, s, Toast.LENGTH_SHORT).show();
                }

                @Override
                protected String doInBackground(Void... params) {
                    //Intent intent = new Intent(SignatureActivity.this, ESign.class);
                    Intent intent = new Intent(SignatureActivity.this, ESignVTwo.class);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] bytes = stream.toByteArray();
                    intent.putExtra("SignPNG", ImgSign);
                    intent.putExtra("IdSurat", id_surat);
                    intent.putExtra("FilesName", filesName);
                    intent.putExtra("SignatureFrom", signatureFrom);
                    intent.putExtra("PageNow", String.valueOf(pageNow));
                    intent.putExtra("PagePhp", String.valueOf(pagePhp));
                    intent.putExtra("signatureWidth", String.valueOf(scaleWidth));
                    intent.putExtra("signatureHeight", String.valueOf(scaleHeight));
                    intent.putExtra("IcFile", bytes);
                    startActivity(intent);
                    finish();
                    return "Melanjutkan...";
                }
            }
            UploadImage ui = new UploadImage();
            ui.execute();
        }else{
            Toast.makeText(SignatureActivity.this, "Mohon Muat Ulang Aplikasi. Dan Coba Lagi...", Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onRenderPDF(){
        try{
            //progressBar.setVisibility(View.GONE);
            File file = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/download/" + filesName);
            if (file.exists()) {
                // create a new renderer
                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                PdfRenderer renderer = new PdfRenderer(parcelFileDescriptor);

                // let us just render all pages
                pageCount = renderer.getPageCount();
                //for (int i = 0; i < pageCount; i++) {
                PdfRenderer.Page page = renderer.openPage(pageNow);

                // say we render for showing on the screen
                //page.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
                mBitmap = Bitmap.createBitmap(page.getWidth(), page.getHeight(),
                        Bitmap.Config.ARGB_8888);
                page.render(mBitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_PRINT);

                // do stuff with the bitmap
                imageBg.setImageBitmap(mBitmap);

                pageWidth = page.getWidth();
                pageHeight = page.getHeight();

                // close the page
                page.close();
                //}

                // close the renderer
                renderer.close();
                Log.d("Page Count ", String.valueOf(pageCount));
            }else {
                Toast.makeText(SignatureActivity.this, "File PDF Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                //Log.d("Exist", "1");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Memanggil/Memasang menu item pada toolbar dari layout menu_bar.xml
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_signature, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.nextActionSign) {
            try {
                if(bitmap != null){
                    if((finalxDelta != 0) && (finalyDelta != 0)){
                        //upSignature();
                        //alertSave();
                        goesToESign();
                    }else{
                        Toast.makeText(SignatureActivity.this, "Size Tanda Tangan Anda Kurang Benar", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(SignatureActivity.this, "Gagal Mendapatkan PDF! Mohon Muat Ulang Aplikasi.", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                Log.e("Error Up ", " " + e.getMessage());
            }
//            Toast.makeText(SignatureActivity.this, "Save", Toast.LENGTH_SHORT).show();
//            Log.d("Cordinate Save", String.valueOf("X : " + finalxDelta + " | Y : " + finalyDelta));
//            Log.d("Signature Scale Save", "X : " + finalWidth + " | Y : " + finalHeight);
//            Log.d("Page Scale Save", "X : " + pageWidth + " | Y : " + pageHeight);

        }else if(item.getItemId() == R.id.nextSignature){
            if((pageCount >= 0) && (pageCount - 1 != pageNow)){
                pageNow = pageNow + 1;
                onRenderPDF();
                pagePhp = pagePhp + 1;
                Log.e("Page ", String.valueOf(pageNow + " | " + pagePhp));
            }else {
                Toast.makeText(SignatureActivity.this, "Sudah Dihalaman Akhir!", Toast.LENGTH_SHORT).show();
            }
        }else if(item.getItemId() == R.id.preSignature){
            if(pageNow > 0){
                pageNow = pageNow - 1;
                onRenderPDF();
                pagePhp = pagePhp - 1;
                Log.e("Page ", String.valueOf(pageNow + " | " + pagePhp));
            }else {
                Toast.makeText(SignatureActivity.this, "Sudah Dihalaman Awal!", Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        showDialog();
        return true;
    }

    private boolean isOutReported = false;

    private boolean isOut(View view) {
        // Check to see if the view is out of bounds by calculating how many pixels
        // of the view must be out of bounds to and checking that at least that many
        // pixels are out.
        float percentageOut = 0.50f;
        int viewPctWidth = (int) (view.getWidth() * percentageOut);
        int viewPctHeight = (int) (view.getHeight() * percentageOut);

        return ((-view.getLeft() >= viewPctWidth) ||
                (view.getRight() - windowWidth) > viewPctWidth ||
                (-view.getTop() >= viewPctHeight) ||
                (view.getBottom() - windowHeight) > viewPctHeight);
    }

    private View.OnTouchListener onTouchListener(){
        return new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                //gestureDetector.onTouchEvent(event);
                simpleOnScaleGestureListener.onTouchEvent(event);
                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();

                // Check if the image view is out of the parent view and report it if it is.
                // Only report once the image goes out and don't stack toasts.
                if (isOut(view)) {
                    if (!isOutReported) {
                        isOutReported = true;
                        Toast.makeText(SignatureActivity.this, "Out", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isOutReported = false;
                }
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)
                                view.getLayoutParams();

                        xDelta = x - view.getLeft();
                        yDelta = y - view.getTop();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                .getLayoutParams();
                         //Image is centered to start, but we need to unhitch it to move it around.
                        if (Build.VERSION.SDK_INT >= 19) {
                            layoutParams.removeRule(RelativeLayout.CENTER_HORIZONTAL);
                            layoutParams.removeRule(RelativeLayout.CENTER_VERTICAL);
                        } else {
                            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
                            layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, 0);
                        }

                        layoutParams.leftMargin = x - xDelta;
                        layoutParams.topMargin = y - yDelta;
                        layoutParams.rightMargin = view.getWidth() - layoutParams.leftMargin - windowWidth;
                        layoutParams.bottomMargin = view.getHeight() - layoutParams.topMargin - windowHeight;
                        view.setLayoutParams(layoutParams);
                        break;

                }
                finalxDelta = x;
                finalyDelta = y;
                mainView.invalidate();
                return true;
            }

//            private GestureDetector gestureDetector = new GestureDetector(SignatureActivity.this, new GestureDetector.SimpleOnGestureListener() {
//                @Override
//                public boolean onDoubleTap(MotionEvent e) {
//                    finalWidth = finalWidth + 30;
//                    finalHeight = finalHeight + 30;
//                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) image1.getLayoutParams();
//                    layoutParams.height = finalHeight;
//                    layoutParams.width = finalWidth;
//                    image1.setLayoutParams(layoutParams);
//                    Log.d("Tap", "onDoubleTap" + finalWidth + " " + finalHeight);
//                    return super.onDoubleTap(e);
//                }
//                @Override
//                public boolean onSingleTapConfirmed(MotionEvent event) {
//                    finalWidth = finalWidth - 30;
//                    finalHeight = finalHeight - 30;
//                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) image1.getLayoutParams();
//                    layoutParams.height = finalHeight;
//                    layoutParams.width = finalWidth;
//                    image1.setLayoutParams(layoutParams);
//
//                    return false;
//                }
//
//            });

            private ScaleGestureDetector simpleOnScaleGestureListener = new ScaleGestureDetector( SignatureActivity.this, new ScaleGestureDetector.SimpleOnScaleGestureListener(){
                @Override
                public boolean onScale(ScaleGestureDetector detector) {
                    scaleFactor *= detector.getScaleFactor();
                    scaleFactor = (float) (scaleFactor < 0.3 ? 0.3 : scaleFactor); // prevent our view from becoming too small //
                    scaleFactor = ((float)((int)(scaleFactor * 100))) / 100; // Change precision to help with jitter when user just rests their fingers //
                    image1.setScaleX(scaleFactor);
                    image1.setScaleY(scaleFactor);
                    return true;
                }

                @Override
                public boolean onScaleBegin(ScaleGestureDetector detector) {
                    inScale = true;
                    return true;
                }

                @Override
                public void onScaleEnd(ScaleGestureDetector detector) {
                    inScale = false;
                    scaleHeight = (int) (finalHeight*scaleFactor);
                    scaleWidth  = (int) (finalWidth*scaleFactor);

                    Log.e("Scale IMG", "Height -> "  + scaleHeight + " Width -> " + scaleWidth);
                }
            });
        };
    }


    private ViewTreeObserver.OnPreDrawListener onPreDrawListener(){
        return new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                image1.getViewTreeObserver().removeOnPreDrawListener(this);
                finalHeight = image1.getMeasuredHeight();
                finalWidth = image1.getMeasuredWidth();
                //Log.d("Scale Image ", "Height: " + finalHeight + " Width: " + finalWidth);
                return true;
            }
        };
    }

    public void alertSave() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        LayoutInflater inflater = getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.alert_save_signature, null);
//        TextView close = dialogView.findViewById(R.id.closeClick);
//        TextView yes = dialogView.findViewById(R.id.yesClick);
//        WebView webView = dialogView.findViewById(R.id.previewSignature);
//        ProgressBar loadWeeb = dialogView.findViewById(R.id.loadWeb);
//
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
//        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=http://103.108.158.181/Koropak-app/uploads/pdf/signature-Heru_NotaDinas.pdf");
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//                loadWeeb.setVisibility(View.VISIBLE);
//
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                loadWeeb.setVisibility(View.GONE);
//            }
//        });
//        String pdf = "http://www.adobe.com/devnet/acrobat/pdfs/pdf_open_parameters.pdf";
//
//        close.setOnClickListener(v -> {
//            alertDialog.dismiss();
//        });
//
//        yes.setOnClickListener(v -> {
//            Log.d("Yes ", "True ");
//        });
//
//        alertDialogBuilder
//                .setView(dialogView)
//                .setCancelable(false);
//
//        // membuat alert dialog dari builder
//        alertDialog = alertDialogBuilder.create();
//        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        // menampilkan alert dialog
//        alertDialog.show();
        //cekMove();

    }

    public void alertFirst() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_signature, null);
        TextView close = dialogView.findViewById(R.id.closeClick);

        close.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false);

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // menampilkan alert dialog
        alertDialog.show();
        //cekMove();
    }

    private void showDialog() {
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder
                .setMessage("Yakin Untuk Kembali?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        backPress = 1;
                        SignatureActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });
        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(userImage1 != null){
            userImage1.compress(Bitmap.CompressFormat.PNG, 80, baos);
            byte[] b = baos.toByteArray();
            signaturePNG = Base64.encodeToString(b, Base64.DEFAULT);
        }else {
            signaturePNG = null;
        }
        return signaturePNG;
    }

    private void upSignature(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(SignatureActivity.this, "Mohon Tunggu...", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    Log.e("-","=================================================");
                    Log.e("Output S:", s);
                    Log.e("-","=================================================");
                    Log.e("X Sig", " " + finalxDelta);
                    Log.e("Y Sig", " " + finalyDelta);
                    Log.e("PageW", " " + pageWidth);
                    Log.e("PageH", " " + pageHeight);
                    Log.e("WindowW", " " + windowWidth);
                    Log.e("WindowH", " " + windowHeight);
                    Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(SignatureActivity.this, TesWebSig.class);
//                    intent.putExtra("namePDF", filesName);
//                    startActivity(intent);
                }
                //Log.e("InputKoreksi ", " " + s);
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String uploadImage = "";
                String nameImage = "";
                if(BitMapToString(bitmap) != null){
                    uploadImage = BitMapToString(bitmap);//BitMapToString(combineImages(mBitmap, bitmap));
                    nameImage   = String.valueOf(System.currentTimeMillis());
                }
                int hasilYSig   =   windowHeight - pageHeight;
                int hasilXSig   =   windowWidth  - pageWidth;

                HashMap<String,String> data = new HashMap<>();
                data.put("id_surat_klr", id_surat);
                data.put("id_master", idMaster);
                data.put("name", signatureFrom);
                data.put("image", uploadImage);
                data.put("fileName", filesName);
                data.put("filePage", String.valueOf(pagePhp));
                data.put("pageWidth", String.valueOf(pageWidth));
                data.put("pageHeight", String.valueOf(pageHeight));
                data.put("windowWidth", String.valueOf(windowWidth));
                data.put("windowHeight", String.valueOf(windowHeight));
                data.put("signatureX", String.valueOf(finalxDelta));
                data.put("signatureY", String.valueOf(finalyDelta));
                data.put("signatureWidth", String.valueOf(finalWidth));
                data.put("signatureHeight", String.valueOf(finalHeight));
                data.put("toolbarHeight", String.valueOf(ToolbarHeight));

                return rh.sendPostRequest(Config.URL_UP_SIGNATURE, data);
            }

        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }

    @Override
    public void onBackPressed() {
        if(backPress == 0){
            showDialog();
        }else{
            super.onBackPressed();
        }
    }

    public void openModalFirst(View view) {
        alertFirst();
    }
}
