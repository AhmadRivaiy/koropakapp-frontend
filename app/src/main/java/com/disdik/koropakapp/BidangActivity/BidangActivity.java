package com.disdik.koropakapp.BidangActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiBidangFrag;
import com.disdik.koropakapp.Fragments.HomeBidangFrag;
import com.disdik.koropakapp.Fragments.OutboxBidangFrag;
import com.disdik.koropakapp.Fragments.RiwayatBidangFrag;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.disdik.koropakapp.SieActivity.HomeSieActivity;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BidangActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    SharedPrefManager sharedPrefManager;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bidang);
        sharedPrefManager = new SharedPrefManager(this);
        myDb    =   new DatabaseHelper(this);
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        //Default ProgramFragment
        mToolbar.setTitle("Home");
        loadFragment(new HomeBidangFrag());
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view_bidang);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container_bidang, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem settingsItem = menu.findItem(R.id.action_notif);
        // set your desired icon here based on a flag if you like
        Cursor stat = myDb.cekCountNotif();
        StringBuffer buffer = new StringBuffer();
        while (stat.moveToNext() ) {
            buffer.append(stat.getString(0));
        }
        int a = Integer.parseInt(buffer.toString());
        if(a > 0){
            settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_notification_on));
        }else {
            settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_ring));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notif:
                //Toast.makeText(this, "Notif", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(BidangActivity.this, NotifActivity.class);
                startActivity(intent);
                break;
            case R.id.feedback:
                Intent feedback = new Intent(BidangActivity.this, FeedBackActivity.class);
                startActivity(feedback);
                break;
            case R.id.logout:
                showDialog();
                break;
            case R.id.settings:
                Intent settings = new Intent(BidangActivity.this, HomeSettings.class);
                startActivity(settings);
                break;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.nvg_home:
                fragment = new HomeBidangFrag();
                mToolbar.setTitle("Home");
                break;
            case R.id.nvg_disposisi:
                fragment = new DisposisiBidangFrag();
                mToolbar.setTitle("Disposisi");
                break;
            case R.id.nvg_surat_klr:
                fragment = new OutboxBidangFrag();
                mToolbar.setTitle("Surat Keluar");
                break;
            case R.id.nvg_riwayat:
                fragment = new RiwayatBidangFrag();
                mToolbar.setTitle("Riwayat");
                break;
        }
        return loadFragment(fragment);
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title dialog
        alertDialogBuilder.setTitle("Keluar dari aplikasi?");
        alertDialogBuilder
                .setMessage("Tap Ya untuk keluar!")
                .setIcon(R.mipmap.ic_app_koropak_round)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        //Delete SQLIte
                        //myDb.deleteDataNotif();
                        // jika tombol diklik, maka akan menutup activity ini
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, false);
                        startActivity(new Intent(BidangActivity.this, MainActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();
                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
