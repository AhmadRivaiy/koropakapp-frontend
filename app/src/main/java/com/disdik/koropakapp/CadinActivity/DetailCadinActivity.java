package com.disdik.koropakapp.CadinActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.disdik.koropakapp.BidangActivity.DetailDisposisiBidang;
import com.disdik.koropakapp.Class.AdapterImage;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SieActivity.DetailSieActivity;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DetailCadinActivity extends AppCompatActivity {
    private static String id_disposisi, namaFile, id_bidang, getUsername, noSurat, no_surat_msk, IdJabatan, IdKantor;
    private TextView asalSurat, tglSurat, nomorSurat, isiSurat, ketSurat, agendaSurat, tglUploadSurat;
    private EditText addLapor;

    private ProgressBar progressBar;
    private WebView webView;
    LayoutInflater inflater;
    private Button btnLaksana, btnLapor;

    private RecyclerView listView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    SharedPrefManager sharedPrefManager;
    private AdapterImage adapter;
    String getSessionId;
    ArrayList<HashMap<String, String>> list_data;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
    private String Document_img1="";
    private Uri uri;
    private Bitmap bitmap;
    private ImageView testView;
    private FrameLayout laporView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cadin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        requestQueue = Volley.newRequestQueue(this);


        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getUsername = user.get(SharedPrefManager.SP_NAMA);
        id_bidang = user.get(SharedPrefManager.SP_ID_BIDANG);
        IdJabatan = user.get(SharedPrefManager.SP_ID_JABATAN);
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);

        toolbar.setTitle("Detail Disposisi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(Build.VERSION.SDK_INT >= 24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        init();
    }

    private void init(){
        Intent intent = getIntent();
        id_disposisi      = intent.getStringExtra(Config.EMP_ID);
        no_surat_msk      = intent.getStringExtra(Config.EMP_ID_GETIMG);

        //TextView
        asalSurat   = findViewById(R.id.asalSurat);
        tglSurat    = findViewById(R.id.tglSurat);
        nomorSurat  = findViewById(R.id.nomorSurat);
        isiSurat    = findViewById(R.id.isiSurat);
        ketSurat    = findViewById(R.id.ketSurat);
        agendaSurat = findViewById(R.id.agendaSurat);
        tglUploadSurat = findViewById(R.id.tglUploadSurat);

        //
        addLapor    =   findViewById(R.id.addLapor);

        btnLaksana = findViewById(R.id.laksanakanBtn);

        //prgresBAr
        progressBar     = findViewById(R.id.progressBar);

        //
        laporView   =   findViewById(R.id.laporView);
        laporView.setVisibility(View.GONE);
        testView    =   findViewById(R.id.tesView);

        webView = (WebView) findViewById(R.id.webViewDisposisi);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
        //webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new DetailCadinActivity.myWebClient());
        webView.loadUrl(Config.URL_WEBVIEW_DISPOSISI_CADIN + id_disposisi + "&nama=" + getUsername + "&id_bidang=" + IdJabatan + "&id_kantor=" + IdKantor);

        showDetailDisposisi();
    }

    public void laksanakanBtn(View view) {
        Ion.with(this).load(Config.URL_SET_READ + id_disposisi + "&id_kantor=" + IdKantor)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try{
                            String callback = result.get("status").getAsString();
                            Log.e("Read", callback + id_disposisi);
                            if(callback.equals("true")){
                                expand(laporView);
                                btnLaksana.setText("Ter-laksanakan!");
                            }
                        }
                        catch (Exception a){
                            a.printStackTrace();
                        }
                    }
                });
    }

    public void laporBtn(View view) {
        checkPermissions();
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(DetailCadinActivity.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }

    private void showDetailDisposisi(){
        Ion.with(this).load(Config.URL_GET_DETDIS_CADIN + id_disposisi + "&id_bidang=" + id_bidang)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try{
                            String id_surat_msk         = result.get("id_surat_msk").getAsString();
                            noSurat                     = result.get("no_surat_msk").getAsString();
                            String no_agenda_msk        = result.get("no_agenda_msk").getAsString();
                            String asal_surat_msk       = result.get("asal_surat_msk").getAsString();
                            String isi_surat_msk        = result.get("isi_surat_msk").getAsString();
                            String keterangan_msk       = result.get("keterangan_msk").getAsString();
                            namaFile                    = result.get("files").getAsString();
                            String tgl_surat_msk        = result.get("tgl_surat_msk").getAsString();
                            String tgl_str              = result.get("tgl_str").getAsString();
                            String stat                 = result.get("status_baca").getAsString();
                            String isi_laporan          = result.get("isi_laporan").getAsString();

                            asalSurat.setText(asal_surat_msk);
                            tglSurat.setText(tgl_surat_msk);
                            nomorSurat.setText(noSurat.substring(0,3));
                            isiSurat.setText(isi_surat_msk);
                            ketSurat.setText(keterangan_msk);
                            agendaSurat.setText(no_agenda_msk);
                            tglUploadSurat.setText(tgl_str);

                            if(isi_laporan.equals("0")){
                                addLapor.setText(null);
                            }else {
                                addLapor.setText(isi_laporan);
                            }

                            if(stat.equals("1") || stat.equals("2")){
                                expand(laporView);
                                btnLaksana.setText("Ter-laksanakan!");
                                if(stat.equals("2")){
                                    Glide.with(DetailCadinActivity.this)
                                            .load(Config.URL_UP_LAPORAN + namaFile)
                                            .crossFade()
                                            .into(testView);
                                    laporView.setVisibility(View.GONE);
                                }
                            }
                        }
                        catch (Exception a){
                            a.printStackTrace();
                        }
                    }
                });
    }

    public void viewFile(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_img, null);
        listView = dialogView.findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.HORIZONTAL);
        listView.setLayoutManager(llm);

        list_data = new ArrayList<HashMap<String, String>>();
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_GET_IMG_CADIN + no_surat_msk + "&id_bidang=" + id_bidang, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Data ", response);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_img");
                    if (jsonArray.length() != 0){
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject json = jsonArray.getJSONObject(a);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("api", json.getString("api"));
                            map.put("api_img", json.getString("api_img"));
                            map.put("img_files", json.getString("img_files"));
                            map.put("halaman", json.getString("halaman"));
                            list_data.add(map);
                            adapter = new AdapterImage(DetailCadinActivity.this, list_data);
                            ((AdapterImage) adapter).setOnItemClickListener(new AdapterImage.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Toast.makeText(DetailCadinActivity.this, "Halaman " + list_data.get(position).get("halaman"), Toast.LENGTH_SHORT).show();
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                                }
                            });
                            listView.setAdapter(adapter);
                        }
                    }else{
                        Toast.makeText(DetailCadinActivity.this, "Pesan Kosong!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailCadinActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
        // set pesan dari dialog
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Akses Kamera Tidak Diberikan. Mohon Diberi Akses!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                laporTake();
                break;
        }
    }

    public void laporTake() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);
                    bitmap = getResizedBitmap(bitmap, 700);
                    testView.setImageBitmap(bitmap);
                    BitMapToString(bitmap);

                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(Environment.getExternalStorageDirectory(), String.valueOf(System.currentTimeMillis()) + ".jpg");
                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                uri = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };

                Cursor c = getContentResolver().query(uri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                bitmap = getResizedBitmap(bitmap, 700);
                Log.w("path_ofimage", picturePath);
                testView.setImageBitmap(bitmap);
                BitMapToString(bitmap);
            }
        }
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(userImage1 != null){
            userImage1.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        }else {
            Document_img1 = null;
        }
        return Document_img1;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void AddData(View view) {
        if(BitMapToString(bitmap) != null){
            upLapor();
        }else {
            Toast.makeText(getApplicationContext(),"Mohon Ambil Gambar Kembali!",Toast.LENGTH_LONG).show();
        }
    }

    private void upLapor(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailCadinActivity.this, "Uploading Image", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String url = (Config.URL_UPLOAD_LAPORAN);
                String getLapor = addLapor.getText().toString();

                String uploadImage = BitMapToString(bitmap);
                HashMap<String,String> data = new HashMap<>();
                data.put("image", uploadImage);
                data.put("name", String.valueOf(System.currentTimeMillis()));
                data.put("id_disposisi", id_disposisi);
                data.put("id_kantor", IdKantor);
                data.put("isi_laporan", getLapor);

                String result = rh.sendPostRequest(url, data);

                return result;

            }

        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }

    public static void expand(final View view) {

        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
