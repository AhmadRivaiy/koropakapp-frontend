package com.disdik.koropakapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.disdik.koropakapp.Class.DownloadFileTask;
import com.disdik.koropakapp.Configuration.Config;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.File;

public class PDFViewer extends AppCompatActivity {

    ProgressBar progressBar;
    PDFView pdfView;
    String filesName;

    private final int paintColor = Color.BLACK;
    // defines paint and canvas
    private Paint drawPaint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);

        Intent intent = getIntent();
        filesName = intent.getStringExtra("files");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(filesName);

        progressBar = findViewById(R.id.progressBar);
        pdfView = findViewById(R.id.pdfView);
        downloadFile();
    }

    private void downloadFile() {
        progressBar.setVisibility(View.VISIBLE);
        DownloadFileTask task = new DownloadFileTask(
                PDFViewer.this, Config.URL_DOWNLOAD_PDF + filesName, "/download/" + filesName);
        task.startTask();
    }

    public void onFileDownloaded() {
        progressBar.setVisibility(View.GONE);
        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath()
                + "/download/" + filesName);
        if (file.exists()) {
            pdfView.fromFile(file)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .onDrawAll(new OnDrawListener() {
                        @Override
                        public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                            Log.e("onDrawAll",pageWidth+":"+pageHeight+":"+displayedPage);
                        }
                    })
                    .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    // spacing between pages in dp. To define spacing color, set view background
                    .spacing(0)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .load();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}


