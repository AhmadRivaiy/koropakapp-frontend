package com.disdik.koropakapp.StaffActivity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.disdik.koropakapp.AgendaActivity.HomeAgenda;
import com.disdik.koropakapp.BuildConfig;
import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.FilePath;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.MakeDoc;
import com.disdik.koropakapp.R;
import com.google.android.material.snackbar.Snackbar;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@SuppressLint({"NewApi", "SetJavaScriptEnabled"})
public class InputSuratKeluarStaff extends AppCompatActivity{
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog datePickerDialog;
    private String jenisSurat, tujuanSurat, LOG = "InputSRTKLR";
    private EditText noSurat, noSuratII, ketTujuanSurat, catSurat, dateSurat, perihalSurat, searchS, resultS, searchKCD, resultK;
    private TextView inputDate, namePDF;
    private LinearLayout searchSchool, linearInput, fieldKCD;

    private String IdKantor, npsnSekolah, idMaster, IdBidang;
    private String idMasterIntent;
    String tujuanK, tujuanS, resultSkola;
    // Pdf upload request code.
    public int PDF_REQ_CODE = 1;

    // Define strings to hold given pdf name, path and ID.
    String PdfNameHolder, PdfPathHolder, PdfID;

    // Creating URI .
    Uri uri;
    private LayoutInflater inflater;
    private WebView webView;
    private ProgressBar progressBar;
    private AlertDialog builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_surat_keluar_staff);
        //SetLocaleDate
        dateFormatter   = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);
        idMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);

        Log.e("IDMASTER", " " + idMaster);

        Intent intent = getIntent();
        idMasterIntent      = intent.getStringExtra("id_master");

        //GetEditText
        //noSurat         = findViewById(R.id.noSurat);
        ketTujuanSurat  = findViewById(R.id.keteranganSurat);
        catSurat        = findViewById(R.id.catatanSurat);
        //dateSurat       = findViewById(R.id.dateInputMsg);
        perihalSurat    = findViewById(R.id.ringkasanSurat);

        searchSchool    = findViewById(R.id.fieldSekolah);
        linearInput     = findViewById(R.id.linearInputKlr);
        searchS         = findViewById(R.id.searchSekolah);
        resultS         = findViewById(R.id.resultSekolah);

        fieldKCD        = findViewById(R.id.fieldKcd);
        searchS         = findViewById(R.id.searchSekolah);
        resultK         = findViewById(R.id.resultKCD);
        searchKCD       = findViewById(R.id.searchKCD);

        namePDF        = findViewById(R.id.namePDF);

        searchS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        searchKCD.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilterKCD(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Input Surat Keluar");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

        searchSchool.setVisibility(View.GONE);
        fieldKCD.setVisibility(View.GONE);
        RequestRunTimePermission();
    }

    public void getNoSurat(View view) {
        helpFile();
    }

    public void helpFile() {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.modal_nosurat, null);
            webView = dialogView.findViewById(R.id.webViewNoSurat);
            progressBar = dialogView.findViewById(R.id.progressBar);
            EditText edit = dialogView.findViewById(R.id.edit);
            edit.setFocusable(true);
            edit.requestFocus();

            ButtonClickJavascriptInterface myJavaScriptInterface = new ButtonClickJavascriptInterface(InputSuratKeluarStaff.this);
            webView.addJavascriptInterface(myJavaScriptInterface, "Android");
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(Config.URL_GETNOSURAT);
            webView.setWebViewClient(new InputSuratKeluarStaff.myWebClient());

            alertDialogBuilder.setView(dialogView);
            alertDialogBuilder.setCancelable(true);
            builder = alertDialogBuilder.create();
            builder.show();
        }catch (Exception e){
            e.printStackTrace();
            openAppRating(this);
            Toast.makeText(InputSuratKeluarStaff.this, "Please Install Google Chrome App, to Run App With Smoothly", Toast.LENGTH_LONG).show();
        }
    }

    public static void openAppRating(Context context) {
        // you can also use BuildConfig.APPLICATION_ID
        String appId = "com.android.chrome";
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));
        boolean marketFound = false;

        // find all applications able to handle our rateIntent
        final List<ResolveInfo> otherApps = context.getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp: otherApps) {
            // look for Google Play application
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.koropakapp")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );
                // make sure it does NOT open in the stack of your activity
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // task reparenting if needed
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                // if the Google Play was already open in a search result
                //  this make sure it still go to the app page you requested
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // this make sure only the Google Play app is allowed to
                // intercept the intent
                rateIntent.setComponent(componentName);
                context.startActivity(rateIntent);
                marketFound = true;
                break;

            }
        }

        // if GP not present on device, open web browser
        if (!marketFound) {
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id="+appId));
            context.startActivity(webIntent);
        }
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error){
            //Your code to do
            webView.loadUrl("file:///android_asset/errorpage.html");
            Toast.makeText(InputSuratKeluarStaff.this, "Your Internet Connection May not be active", Toast.LENGTH_LONG).show();
        }
    }

    public class ButtonClickJavascriptInterface {
        Context mContext;
        ButtonClickJavascriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
       public void onResult(final String toast){
            /*Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            final ProgressDialog progresRing = ProgressDialog.show(MyActivity.this, "Trinity Tuts", "Wait Loading...", true);
            runOnUiThread(new Runnable() {
                // TODO: 2020-02-07  
                @Override
                public void run() {
                    noSurat.setText(toast);
                }
            });
            builder.dismiss();
            */
            Thread thread = new Thread(){
                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(1000);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    noSurat.setText(toast);
                                }
                            });
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    builder.dismiss();
                };
            };
            thread.start();
        }
    }

    public void onRadioButtonClick(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton)view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.umumSurat:
                if (checked)
                    jenisSurat = "Umum";
                break;
            case R.id.perintahSurat:
                if (checked)
                    jenisSurat = "Surat Perintah";
                break;
            case R.id.ketSurat:
                if (checked)
                    jenisSurat = "Surat Keterangan";
                break;
            case R.id.izinSurat:
                if (checked)
                    jenisSurat = "Surat Izin";
                break;
            case R.id.undanganSurat:
                if (checked)
                    jenisSurat = "Surat Undangan";
                break;
            case R.id.lainSurat:
                if (checked)
                    jenisSurat = "Lain - Lain";
                break;
            case R.id.uptdTujuan:
                if (checked)
                    tujuanSurat = "15";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            case R.id.kadisTujuan:
                if (checked)
                    tujuanSurat = "2";
                searchSchool.setVisibility(View.GONE);
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "0";
                break;
            case R.id.disdikTujuan:
                if (checked)
                    tujuanSurat = "3";
                    searchSchool.setVisibility(View.GONE);
                    fieldKCD.setVisibility(View.GONE);
                    tujuanS = "0";
                    tujuanK = "0";
                break;
            default:
                jenisSurat = "";
                tujuanSurat = "";
        }
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultSekolah");
                    if (data.length() == 0){
                        Toast.makeText(InputSuratKeluarStaff.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String npsn = json.getString("npsn");
                        String nama = json.getString("nama_sekolah");
                        npsnSekolah = npsn;
                        resultS.setText(nama);
                        Log.e("Result", npsnSekolah);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

        @Override
        protected String doInBackground(Void... params) {
            RequestHandler rh = new RequestHandler();
            String s = rh.sendGetRequestParam(Config.URL_FETCH_SEKOLAH, query);
            return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getFilterKCD(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultKcd");
                    if (data.length() == 0){
                        Toast.makeText(InputSuratKeluarStaff.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String id_master = json.getString("id_master");
                        String nama_jabatan = json.getString("nama_jabatan");
                        idMaster = id_master;
                        resultK.setText(nama_jabatan);
                        Log.e("Result", idMaster);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FETCH_KCD, query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void getDate(View view) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateSurat.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }


    public void pickPDF(View view) {
        // PDF selection code start from here .
        // Creating intent object.
        Intent intent = new Intent();

        // Setting up default file pickup time as PDF.
        intent.setType("application/pdf");

        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PDF_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();

            // After selecting the PDF set PDF is Selected text inside Button.
            namePDF.setText(getFileName(uri));
        }
    }

    // Requesting run time permission method starts from here.
    public void RequestRunTimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(InputSuratKeluarStaff.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Log.d("READ_EXTERNAL_STORAGE", "Show Dialog");
            //Toast.makeText(MakeDoc.this,"READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(InputSuratKeluarStaff.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    String getFileName(Uri uri){
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public void onSubmit(View view) {
        if(uri!=null) {
            if(tujuanS != null) {
                if(tujuanS.equals("0")) {
                    if(tujuanK.equals("0")){
                        //int getNoSurat = noSurat.getText().toString().length();
                        int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                        int getCatSurat = catSurat.getText().toString().length();
                        //int getDateSurat = dateSurat.getText().toString().length();
                        int tujuan = tujuanSurat.length();
                        int jenis = jenisSurat.length();

                        if ((getRingkasanSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0)) {
                            npsnSekolah =   "0";
                            idMaster    =   "0";
                            uploadImage();
                        } else {
                            Toast.makeText(InputSuratKeluarStaff.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        //int getNoSurat = noSurat.getText().toString().length();
                        int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                        int getCatSurat = catSurat.getText().toString().length();
                        //int getDateSurat = dateSurat.getText().toString().length();
                        int tujuan = tujuanSurat.length();
                        int jenis = jenisSurat.length();
                        int id_master = resultK.getText().toString().length();
                        if ((getRingkasanSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0) && (id_master != 0)) {
                            npsnSekolah =   "0";
                            uploadImage();
                        } else {
                            Toast.makeText(InputSuratKeluarStaff.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                        }
                    }
                }else {
                    //int getNoSurat = noSurat.getText().toString().length();
                    int getRingkasanSurat = ketTujuanSurat.getText().toString().length();
                    int getCatSurat = catSurat.getText().toString().length();
                    //int getDateSurat = dateSurat.getText().toString().length();
                    int tujuan = tujuanSurat.length();
                    int jenis = jenisSurat.length();
                    int npsn = resultS.getText().toString().length();

                    if ((getRingkasanSurat != 0) && (tujuan != 0) && (getCatSurat != 0) && (jenis != 0) && (npsn != 0)) {
                        idMaster    =   "0";
                        uploadImage();
                    } else {
                        Toast.makeText(InputSuratKeluarStaff.this, "Tolong Isi Dengan Lenkap!", Toast.LENGTH_LONG).show();
                    }
                }
            }else {
                Toast.makeText(InputSuratKeluarStaff.this,"Tolong Isi Dengan Lenkap!",Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(InputSuratKeluarStaff.this,"File Belum Di Ambil !",Toast.LENGTH_LONG).show();
        }
    }

    private void uploadImage(){
        PdfPathHolder = FilePath.getPath(this, uri);
        Log.e("Path ", " " + PdfPathHolder);
        // If file path object is null then showing toast message to move file into internal storage.
        if (PdfPathHolder == null) {
            Toast.makeText(InputSuratKeluarStaff.this, "Tolong Pindahkan File PDF-nya Ke Penyimpanan Internal Anda dan Coba Lagi.", Toast.LENGTH_LONG).show();
        }
        // If file path is not null then PDF uploading file process will starts.
        else {
            try {
                PdfID = UUID.randomUUID().toString();
                //new MultipartUploadRequest(this, PdfID, "http://103.108.158.181/LetterApp/Template/uploadFilePDF")
                MultipartUploadRequest uploadRequest = new MultipartUploadRequest(this, PdfID, Config.URL_UPLOAD_PDF)
                        .addFileToUpload(PdfPathHolder, "pdf")
                        .addParameter("name", getFileName(uri).replace(".pdf", ""))
                        //.setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(5);
                UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
                //.startUpload();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
                    NotificationChannel channel = new NotificationChannel("Upload", "Upload service", NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);

                    UploadNotificationConfig notificationConfig = new UploadNotificationConfig();
                    notificationConfig.setNotificationChannelId("Upload");

                    uploadRequest.setNotificationConfig(notificationConfig);
                } else {
                    // If android < Oreo, just set a simple notification (or remove if you don't wanna any notification
                    // Notification is mandatory for Android > 8
                    uploadRequest.setNotificationConfig(new UploadNotificationConfig());
                }

                uploadRequest.startUpload();
                UploadFix();
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void UploadFix(){
        class UploadFix extends AsyncTask<Void, Void, String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(InputSuratKeluarStaff.this, "Uploading Data", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Log.e("Output ", "Null");
                    Toast.makeText(getApplicationContext(), "Tidak Ada Internet!",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Berhasil Input!",Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                    SharedPrefManager sharedPrefManager = new SharedPrefManager(getApplicationContext());
                    HashMap<String, String> user = sharedPrefManager.getSPUser();
                    String Master = user.get(SharedPrefManager.SP_ID_MASTER);
                    //GetText
                    //String getNoSurat = noSurat.getText().toString();
                    String getTujuanSurat = ketTujuanSurat.getText().toString();
                    String getPerihalSurat = perihalSurat.getText().toString();
                    String getCatSurat = catSurat.getText().toString();
                    //String getDateSurat = dateSurat.getText().toString();

                    HashMap<String,String> data = new HashMap<>();
                    data.put("id_kantor", IdKantor);
                    data.put("name", getFileName(uri));
                    data.put("kode_surat", "");
                    data.put("isi_surat_klr", getPerihalSurat);
                    data.put("jenis_surat_klr", jenisSurat);
                    data.put("tujuan_surat_klr", tujuanSurat);
                    data.put("keterangan_klr", getTujuanSurat);
                    data.put("tgl_surat_klr", "");
                    data.put("catatan", getCatSurat);
                    data.put("npsn_tujuan", npsnSekolah);
                    data.put("req_ttd", tujuanSurat);
                    data.put("pengelola_master", Master);
                    data.put("id_master", idMaster);
                    data.put("id_bidang", IdBidang);

                    for (Map.Entry<String,String> entry : data.entrySet()) {
                        System.out.printf("%s -> %s%n", entry.getKey(), entry.getValue());
                    }

                    return rh.sendPostRequest(Config.URL_UP_NEW_SRTKELUAR, data);

            }
        }

        UploadFix ui = new UploadFix();
        ui.execute();
    }

    public static void expand(final View view) {

        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    public void helpFile(View view) {
    }

}
