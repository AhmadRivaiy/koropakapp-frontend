package com.disdik.koropakapp.StaffActivity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.disdik.koropakapp.BuildConfig;
import com.disdik.koropakapp.Class.AdapterImage;
import com.disdik.koropakapp.Class.AdapterLogSekdis;
import com.disdik.koropakapp.Class.FilePath;
import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Class.UpdateReadHelper;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnDrawListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.google.android.material.snackbar.Snackbar;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class DetailOutboxStaff extends AppCompatActivity {
    private TextView tujuanSurat, tglSurat, noAgenda, noSurat, ringkasanSurat, ketSurat, catatanSurat, intFile, textaddImg, pengelolaSurat, statusFinish, namePDF;
    private EditText searchS, resultS, searchKCD, resultK;
    private String id_surat, filesName, ttdReq, filesNameInt, tujuanSuratString;
    private String jenisSurat, LOG = "InputSRTKLR", tujuanK, tujuanS, resultSkola, npsnSekolah;
    private LinearLayout searchSchool, linearInput, fieldKCD, sendSuratklr;
    private ProgressBar progressBar;
    PDFView pdfView;

    private Button pickPDF, uploadPDF, reSendSrt, nextAgenda, cancelLetter, downloadStaff;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
    private String Document_img1="", idMaster, CondSrt, IdBidang, IdKantor;
    private Uri uri;
    private Bitmap bitmap;

    private AlertDialog alertDialog;
    ProgressDialog loading;
    private RecyclerView listView;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;
    private AdapterImage adapter;
    private AdapterLogSekdis adapterLog;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> list_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_outbox_staff);
        Toolbar toolbar = findViewById(R.id.toolbar);
        requestQueue = Volley.newRequestQueue(this);

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        idMaster = user.get(SharedPrefManager.SP_ID_MASTER);
        IdBidang = user.get(SharedPrefManager.SP_ID_BIDANG);
        IdKantor = user.get(SharedPrefManager.SP_ID_KANTOR);

        toolbar.setTitle("Detail Surat Keluar");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        id_surat      = intent.getStringExtra(Config.EMP_ID);
        CondSrt       = intent.getStringExtra("Cond_Srt");

        //getIdTextView
        pengelolaSurat  =   findViewById(R.id.pengelolaSurat);
        tujuanSurat     =   findViewById(R.id.tujuanSurat);
        tglSurat        =   findViewById(R.id.tglSurat);
        noAgenda        =   findViewById(R.id.nomorAgenda);
        noSurat         =   findViewById(R.id.nomorSurat);
        ringkasanSurat  =   findViewById(R.id.ringSurat);
        ketSurat        =   findViewById(R.id.ketSurat);
        catatanSurat    =   findViewById(R.id.catatanSurat);
        intFile         =   findViewById(R.id.intFile);
        statusFinish    =   findViewById(R.id.statusFinish);

        pdfView         =   findViewById(R.id.pdfViewDetail);
        progressBar     =   findViewById(R.id.progressBar);

        searchSchool    = findViewById(R.id.fieldSekolah);
        linearInput     = findViewById(R.id.linearInputKlr);
        searchS         = findViewById(R.id.searchSekolah);
        resultS         = findViewById(R.id.resultSekolah);

        fieldKCD        = findViewById(R.id.fieldKcd);
        searchS         = findViewById(R.id.searchSekolah);
        resultK         = findViewById(R.id.resultKCD);
        searchKCD       = findViewById(R.id.searchKCD);

        sendSuratklr    = findViewById(R.id.sendSuratklr);
        reSendSrt       = findViewById(R.id.reSendSrt);
        nextAgenda      = findViewById(R.id.nextAgenda);
        cancelLetter    = findViewById(R.id.cancelSurat);

        downloadStaff   = findViewById(R.id.downloadBtnStaff);
        //List
        listView = findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        UpdateReadHelper upR = new UpdateReadHelper();
        upR.UpOutbox(this, id_surat, Config.TABLE_NAME_SEKDIS);


        //removeImg.setVisibility(View.GONE);

        searchSchool.setVisibility(View.GONE);
        fieldKCD.setVisibility(View.GONE);
        searchSchool.setVisibility(View.GONE);
        fieldKCD.setVisibility(View.GONE);
        downloadStaff.setVisibility(View.GONE);

        searchS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilter(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        searchKCD.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                //filter(editable.toString());
                getFilterKCD(editable.toString());
                Log.e("Searched", editable.toString());

            }
        });

        showDetailSuratKeluar();
    }

    private void getFilter(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultSekolah");
                    if (data.length() == 0){
                        Toast.makeText(DetailOutboxStaff.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String npsn = json.getString("npsn");
                        String nama = json.getString("nama_sekolah");
                        npsnSekolah = npsn;
                        resultS.setText(nama);
                        Log.e("Result", npsnSekolah);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FETCH_SEKOLAH, query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void getFilterKCD(final String query){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(getActivity(),"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray data = jsonObject.getJSONArray("resultKcd");
                    if (data.length() == 0){
                        Toast.makeText(DetailOutboxStaff.this, "Pencarian Tidak Ditemukan!", Toast.LENGTH_SHORT).show();
                    }else {
                        JSONObject json = data.getJSONObject(0);
                        String id_master = json.getString("id_master");
                        String nama_jabatan = json.getString("nama_jabatan");
                        idMaster = id_master;
                        resultK.setText(nama_jabatan);
                        Log.e("Result", idMaster);
                    }
                }catch (JSONException a){
                    a.printStackTrace();
                    Snackbar snackbar = Snackbar
                            .make(linearInput, "No Internet Connection!", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                String s = rh.sendGetRequestParam(Config.URL_FETCH_KCD, query);
                return s;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    public void showDetailSuratKeluar(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxStaff.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //loading.dismiss();
                //String url = Config.URL_GET_IMG_SEKDIS_OUT + id_surat;
                showListMessage(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();
                return rh.sendGetRequestParam(Config.URL_GET_DETAIL_KLR_STAFF, id_surat);
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    private void showListMessage(String s){
        try {
            JSONObject jsonObject = new JSONObject(s);
            JSONArray jsonArray = jsonObject.getJSONArray("detail_surat_klr");
            for (int a = 0; a < jsonArray.length(); a++) {
                JSONObject json = jsonArray.getJSONObject(a);

                ttdReq = json.getString("ttd_req");
                filesNameInt = json.getString("files");
                filesName = json.getString("namefiles");

                String pengelola            =   json.getString("nama_pengelola");
                String no_surat_klr         =   json.getString("no_surat_klr");
                String no_agenda_klr        =   json.getString("no_agenda_klr");
                String tujuan_surat_klr     =   json.getString("tujuan_surat_klr");
                String ringkasan_surat_klr  =   json.getString("ringkasan_surat_klr");
                String keterangan_klr       =   json.getString("keterangan_klr");
                String catatan              =   json.getString("catatan");
                String tgl_surat_klr        =   json.getString("tgl_surat_klr");
                String status_valid         =   json.getString("status_valid");
                String status_finish        =   json.getString("status_finish");
                String status_ttd_uptd      =   json.getString("status_ttd_uptd");
                String status_surat         =   json.getString("status_surat");

                pengelolaSurat.setText(pengelola);
                tujuanSurat.setText(tujuan_surat_klr);
                tglSurat.setText(tgl_surat_klr);
                noAgenda.setText(no_agenda_klr);
                noSurat.setText(no_surat_klr);
                ringkasanSurat.setText(ringkasan_surat_klr);
                ketSurat.setText(keterangan_klr);
                catatanSurat.setText(catatan);
                intFile.setText(filesName);

                if(status_valid.equals("0")){
                    statusFinish.setText("- Belum Valid -");
                    sendSuratklr.setVisibility(View.GONE);
                    nextAgenda.setEnabled(false);
                }else{
                    statusFinish.setText("- Sudah Valid -");
                    sendSuratklr.setVisibility(View.VISIBLE);
                    downloadStaff.setVisibility(View.VISIBLE);
                    reSendSrt.setEnabled(false);
                    nextAgenda.setEnabled(false);
                    cancelLetter.setEnabled(false);
                }

                if(status_finish.equals("8")){
                    nextAgenda.setEnabled(false);
                    cancelLetter.setEnabled(false);
                }else if(status_finish.equals("7")){
                    nextAgenda.setEnabled(false);
                    cancelLetter.setEnabled(false);
                }

                if(status_ttd_uptd.equals("1")){
                    nextAgenda.setEnabled(true);
                    cancelLetter.setEnabled(true);
                }

                if(status_surat.equals("1")){
                    nextAgenda.setEnabled(false);
                    cancelLetter.setEnabled(false);
                }

                downloadPDF();
            }
        }catch (JSONException j){
            j.printStackTrace();
            Toast.makeText(DetailOutboxStaff.this, "Server Maintenance!", Toast.LENGTH_SHORT).show();
        }
    }

    //TODO: Pdf Downloaded
    public void downloadPDF(){
        class GetListPengawas extends AsyncTask<Void,Void,String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //loading = ProgressDialog.show(DetailOutboxKadis.this,"Mengambil...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                File file = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath()
                        + "/download/" + filesName);
                if (file.exists()) {
                    pdfView.fromFile(file)
                            .enableSwipe(true) // allows to block changing pages using swipe
                            .swipeHorizontal(true)
                            .enableDoubletap(true)
                            .defaultPage(0)
                            .onDrawAll(new OnDrawListener() {
                                @Override
                                public void onLayerDrawn(Canvas canvas, float pageWidth, float pageHeight, int displayedPage) {
                                    Log.e("onDrawAll",pageWidth + ":" + pageHeight + ":" + displayedPage);
                                }
                            })
                            .enableAnnotationRendering(true) // render annotations (such as comments, colors or forms)
                            .password(null)
                            .scrollHandle(null)
                            .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                            // spacing between pages in dp. To define spacing color, set view background
                            .spacing(0)
                            .pageFitPolicy(FitPolicy.WIDTH)
                            .load();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                int count;
                try {
                    Log.d("PDF Downloading", "url = " + Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URL _url = new URL(Config.URL_DOWNLOAD_PDF_SRTKLR + filesName);
                    URLConnection conection = _url.openConnection();
                    conection.connect();
                    InputStream input = new BufferedInputStream(_url.openStream(),
                            8192);
                    OutputStream output = new FileOutputStream(
                            Environment.getExternalStorageDirectory() + "/download/" + filesName);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    Log.e("Error ", e.getMessage());
                }
                return null;
            }
        }
        GetListPengawas ge = new GetListPengawas();
        ge.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public void LogSrtKlr(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.modal_log, null);
        listView = dialogView.findViewById(R.id.listView);
        ProgressBar progressBarLoad = dialogView.findViewById(R.id.loadLog);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        list_data = new ArrayList<HashMap<String, String>>();
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_LOG_STAFF + id_surat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressBarLoad.setVisibility(View.GONE);
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("list_log");
                    if (jsonArray.length() != 0){
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject json = jsonArray.getJSONObject(a);

                            HashMap<String, String> map = new HashMap<String, String>();
                            map.put("id_surat_klr", json.getString("id_surat_klr"));
                            map.put("tgl_catatan", json.getString("tgl_catatan"));
                            map.put("catatan_srt_keluar", json.getString("catatan_srt_keluar"));
                            map.put("img_log_klr", json.getString("img_log_klr"));
                            map.put("tujuan_srt_klr", json.getString("tujuan_srt_klr"));
                            map.put("penulis_log", json.getString("penulis_log"));
                            map.put("nama", json.getString("nama"));
                            list_data.add(map);
                            adapterLog = new AdapterLogSekdis(DetailOutboxStaff.this, list_data);
                            ((AdapterLogSekdis) adapterLog).setOnItemClickListener(new AdapterLogSekdis.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    /*
                                    Intent intent = new Intent(getActivity(), DetailDisposisi.class);
                                    HashMap<String,String> map = (HashMap)list_data.get(position);
                                    String empId = map.get(Config.TAG_ID_SURAT_MSK).toString();
                                    intent.putExtra(Config.EMP_ID, empId);
                                    startActivity(intent);
                                    */
                                }
                            });
                            listView.setAdapter(adapterLog);
                        }
                    }else{
                        Toast.makeText(DetailOutboxStaff.this, "Tidak Ada Log!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "Request Too Long To Wait!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailOutboxStaff.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }


    public void TtdSrtKlr(View view) {

    }


    public void reSend(View view) {
        // PDF selection code start from here .
                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
                inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.modal_uppdf, null);
                pickPDF      = dialogView.findViewById(R.id.pickPDF);
                uploadPDF    = dialogView.findViewById(R.id.uploadPDF);
                namePDF      = dialogView.findViewById(R.id.namePDF);

                pickPDF.setOnClickListener(v -> {
                    // PDF selection code start from here .
                    // Creating intent object.
                    Intent intent = new Intent();

                    // Setting up default file pickup time as PDF.
                    intent.setType("application/pdf");

                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Select Pdf"), 1);
                });

                uploadPDF.setOnClickListener(v -> {
                    PdfUploadFunction();
                });

                alertDialogBuilder
                        .setView(dialogView)
                        .setCancelable(false)
                        .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                // membuat alert dialog dari builder
                alertDialog = alertDialogBuilder.create();

                // menampilkan alert dialog
                alertDialog.show();
        RequestRunTimePermission();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();

            // After selecting the PDF set PDF is Selected text inside Button.
            pickPDF.setText(getString(R.string.pdf_picked));
            namePDF.setText(getFileName(uri));
        }
    }

    // Requesting run time permission method starts from here.
    public void RequestRunTimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(DetailOutboxStaff.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Log.d("READ_EXTERNAL_STORAGE", "Show Dialog");
            //Toast.makeText(MakeDoc.this,"READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(DetailOutboxStaff.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    public String getFileName(Uri uri){
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    // PDF upload function starts from here.
    public void PdfUploadFunction() {
        stringRequest = new StringRequest(Request.Method.GET, Config.URL_UPDATE_PDF + id_surat + "&nama_file=" + getFileName(uri) + "&id_master=" + idMaster + "&id_bidang=" + IdBidang + "&id_kantor=" + IdKantor, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    alertDialog.dismiss();
                    Toast.makeText(DetailOutboxStaff.this, response, Toast.LENGTH_LONG).show();
                }catch (Exception e){
                    e.printStackTrace();
                    if(getApplicationContext() != null) {
                        Toast.makeText(getApplicationContext(), "Request Too Long To Wait!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DetailOutboxStaff.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(stringRequest);

        String PdfPathHolder = FilePath.getPath(this, uri);
        // If file path object is null then showing toast message to move file into internal storage.
        if (PdfPathHolder == null) {
            Toast.makeText(DetailOutboxStaff.this, "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
        }
        // If file path is not null then PDF uploading file process will starts.
        else {
            try {
                String PdfID = UUID.randomUUID().toString();
                //new MultipartUploadRequest(this, PdfID, "http://103.108.158.181/LetterApp/Template/uploadFilePDF")
                MultipartUploadRequest uploadRequest = new MultipartUploadRequest(this, PdfID, Config.URL_UPLOAD_PDF)
                        .addFileToUpload(PdfPathHolder, "pdf")
                        .addParameter("name", getFileName(uri).replace(".pdf", ""))
                        //.setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(5);
                UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
                //.startUpload();
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
                    NotificationChannel channel = new NotificationChannel("Upload", "Upload service", NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(channel);

                    UploadNotificationConfig notificationConfig = new UploadNotificationConfig();
                    notificationConfig.setNotificationChannelId("Upload");

                    uploadRequest.setNotificationConfig(notificationConfig);
                } else {
                    // If android < Oreo, just set a simple notification (or remove if you don't wanna any notification
                    // Notification is mandatory for Android > 8
                    uploadRequest.setNotificationConfig(new UploadNotificationConfig());
                }

                uploadRequest.startUpload();
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {
        switch (RC) {
            case 1:
                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(DetailOutboxStaff.this,"Permission Granted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(DetailOutboxStaff.this,"Permission Canceled", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(getActivity(), "Refreshed", Toast.LENGTH_SHORT).show();
    }

    public void onRadioButtonClick(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton)view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.umumSurat:
                if (checked)
                    jenisSurat = "Umum";
                break;
            case R.id.perintahSurat:
                if (checked)
                    jenisSurat = "Surat Perintah";
                break;
            case R.id.ketSurat:
                if (checked)
                    jenisSurat = "Surat Keterangan";
                break;
            case R.id.izinSurat:
                if (checked)
                    jenisSurat = "Surat Izin";
                break;
            case R.id.undanganSurat:
                if (checked)
                    jenisSurat = "Surat Undangan";
                break;
            case R.id.lainSurat:
                if (checked)
                    jenisSurat = "Lain - Lain";
                break;
            case R.id.uptdTujuan:
                if (checked)
                    tujuanSuratString = "UPTD Tikomdik";
                searchSchool.setVisibility(View.GONE);
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "0";
                break;
            case R.id.disdikTujuan:
                if (checked)
                    tujuanSuratString = "Disdik Jawa Barat";
                searchSchool.setVisibility(View.GONE);
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "0";
                break;
            case R.id.kcdTujuan:
                if (checked)
                    tujuanSuratString = "KCD";
                searchSchool.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "1";
                expand(fieldKCD);
                break;
            case R.id.sekolahTujuan:
                if (checked)
                    tujuanSuratString = "Sekolah";
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "1";
                tujuanK = "0";
                expand(searchSchool);
                break;
            case R.id.instansiTujuan:
                if (checked)
                    tujuanSuratString = "Instansi Lain";
                searchSchool.setVisibility(View.GONE);
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "0";
                break;
            case R.id.perTujuan:
                if (checked)
                    tujuanSuratString = "Per-Orangan";
                searchSchool.setVisibility(View.GONE);
                fieldKCD.setVisibility(View.GONE);
                tujuanS = "0";
                tujuanK = "0";
                break;
            default:
                jenisSurat = "";
                tujuanSuratString = "";
        }
    }

    public static void expand(final View view) {

        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Set initial height to 0 and show the view
        view.getLayoutParams().height = 0;
        view.setVisibility(View.VISIBLE);

        ValueAnimator anim = ValueAnimator.ofInt(view.getMeasuredHeight(), targetHeight);
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = (int) (targetHeight * animation.getAnimatedFraction());
                view.setLayoutParams(layoutParams);
            }
        });
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                // At the end of animation, set the height to wrap content
                // This fix is for long views that are not shown on screen
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
        });
        anim.start();
    }

    public void sendKlr(View view) {

    }

    public void NextAgenda(View view) {
        class NextAgendaValid extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxStaff.this,"Melanjutkan...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(DetailOutboxStaff.this, "Tidak Ada Koneksi Internet", Toast.LENGTH_LONG).show();
                }else {
                    showDetailSuratKeluar();
                    Toast.makeText(DetailOutboxStaff.this, s, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_surat_klr", id_surat);
                data.put("id_master", idMaster);

                return rh.sendPostRequest(Config.URL_VALIDATION_LETTER, data);
            }
        }
        NextAgendaValid ge = new NextAgendaValid();
        ge.execute();
    }

    public void CancelLetter(View view) {
        class CancelLetterKlr extends AsyncTask<Void,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(DetailOutboxStaff.this," Membatalkan...","Please Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(DetailOutboxStaff.this, "Tidak Ada Koneksi Internet", Toast.LENGTH_LONG).show();
                }else {
                    showDetailSuratKeluar();
                    Toast.makeText(DetailOutboxStaff.this, s, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                RequestHandler rh = new RequestHandler();

                HashMap<String,String> data = new HashMap<>();
                data.put("id_surat_klr", id_surat);
                data.put("id_master", idMaster);

                return rh.sendPostRequest(Config.URL_CANCEL_LETTER, data);
            }
        }
        CancelLetterKlr ge = new CancelLetterKlr();
        ge.execute();
    }

    public void downloadBtn(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_download_msk, null);
        TextView close = dialogView.findViewById(R.id.closeClick);
        TextView filePdf    =   dialogView.findViewById(R.id.nameFileKartu);

        filePdf.setText(filesName);

        close.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
        alertDialogBuilder
                .setView(dialogView)
                .setCancelable(false);

        // membuat alert dialog dari builder
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // menampilkan alert dialog
        alertDialog.show();
    }
}
