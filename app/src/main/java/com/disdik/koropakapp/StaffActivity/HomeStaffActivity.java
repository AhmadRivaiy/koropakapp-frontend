package com.disdik.koropakapp.StaffActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.disdik.koropakapp.Class.CustomToast;
import com.disdik.koropakapp.Class.DatabaseHelper;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.FeedBackActivity;
import com.disdik.koropakapp.Fragments.DisposisiStaffFrag;
import com.disdik.koropakapp.Fragments.HomeSieFrag;
import com.disdik.koropakapp.Fragments.HomeStaffFrag;
import com.disdik.koropakapp.Fragments.KonsepSuratFrag;
import com.disdik.koropakapp.Fragments.LainnyaFrag;
import com.disdik.koropakapp.Fragments.OutboxStaffFrag;
import com.disdik.koropakapp.MainActivity;
import com.disdik.koropakapp.NotifActivity;
import com.disdik.koropakapp.R;
import com.disdik.koropakapp.SettingsActivity.HomeSettings;
import com.disdik.koropakapp.SieActivity.HomeSieActivity;
import com.disdik.koropakapp.TikomdikActivity.KepalaTikomdikActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class HomeStaffActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private Toolbar mToolbar;
    SharedPrefManager sharedPrefManager;
    DatabaseHelper myDb;
    String getIdMaster;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_staff);
        sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        getIdMaster     = user.get(SharedPrefManager.SP_ID_MASTER);

        myDb    =   new DatabaseHelper(this);

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        //Default ProgramFragment
        mToolbar.setTitle("Home");
        loadFragment(new HomeStaffFrag());

        bottomNavigationView = findViewById(R.id.nav_view_staff);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container_staff, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.nvg_home:
                fragment = new HomeStaffFrag();
                mToolbar.setTitle("Home");
                break;
            case R.id.nvg_disposisi:
                fragment = new DisposisiStaffFrag();
                mToolbar.setTitle("Disposisi");
                break;
            case R.id.nvg_surat_klr:
                fragment = new OutboxStaffFrag();
                mToolbar.setTitle("Surat Keluar");
                break;
            case R.id.nvg_konsep_surat:
                fragment = new KonsepSuratFrag();
                mToolbar.setTitle("Konsep Surat");
                break;
            case R.id.nvg_lainnya:
                fragment = new LainnyaFrag();
                mToolbar.setTitle("Lainnya");
                break;
        }
        return loadFragment(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Ion.with(this).load(Config.URL_CHECK_STATUS_STAFF + getIdMaster)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            try {
                                int countDis    = result.get("dis_msk").getAsInt();
                                int countSrtKlr = result.get("srt_klr").getAsInt();

                                if(countDis > 0) bottomNavigationView.getOrCreateBadge(R.id.nvg_disposisi).setNumber(countDis);
                                if(countDis == 0) bottomNavigationView.getOrCreateBadge(R.id.nvg_disposisi).setVisible(false);

                                if(countSrtKlr > 0) bottomNavigationView.getOrCreateBadge(R.id.nvg_surat_klr).setNumber(countSrtKlr);
                                if(countSrtKlr == 0) bottomNavigationView.getOrCreateBadge(R.id.nvg_surat_klr).setVisible(false);
                            } catch (Exception a) {
                                Log.e("Error ", "Get Count Disposisi");
                            }
                        }
                    });
        }catch (Exception r){
            r.printStackTrace();
        }
    }
}
