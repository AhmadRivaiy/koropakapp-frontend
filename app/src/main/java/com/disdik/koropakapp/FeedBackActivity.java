package com.disdik.koropakapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.disdik.koropakapp.Class.RequestHandler;
import com.disdik.koropakapp.Class.SharedPrefManager;
import com.disdik.koropakapp.Configuration.Config;
import com.disdik.koropakapp.StaffActivity.DetailDisposisiStaff;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FeedBackActivity extends AppCompatActivity {
    private RelativeLayout layoutAlert;
    private CardView layoutAddImg, layoutresultimg;
    private TextView textaddImg;
    private EditText textareaFeed, emailphone;
    private ImageView resultImgFeed, removeImg;

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
    private int jenisFeed = 1;
    private String Document_img1="", idMaster;
    private Uri uri;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        layoutAlert     =   findViewById(R.id.layoutAlert);
        layoutAddImg    =   findViewById(R.id.layoutAddImg);
        layoutresultimg =   findViewById(R.id.layoutResultImg);

        textaddImg      =   findViewById(R.id.ketAddImg);
        textareaFeed    =   findViewById(R.id.textareaFeed);
        emailphone      =   findViewById(R.id.emailPhone);

        //
        resultImgFeed   =   findViewById(R.id.resultImgFeed);
        removeImg       =   findViewById(R.id.removeImg);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("FeedBack");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });

        SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
        HashMap<String, String> user = sharedPrefManager.getSPUser();
        idMaster = user.get(SharedPrefManager.SP_ID_MASTER);

        removeImg.setVisibility(View.GONE);
    }

    public void closeAlert(View view) {
        layoutAlert.setVisibility(View.GONE);
    }

    public void onFeedBackClick(View view) {
        boolean checked = ((RadioButton)view).isChecked();

        switch (view.getId()){
            case R.id.problemChoose:
                if(checked)
                    jenisFeed = 1;
                    textareaFeed.setHint("Jelaskan Masalah yang di alami, selama menggunakan Aplikasi Ini");

                break;
            case R.id.adviceChoose:
                if(checked)
                    jenisFeed = 2;
                    textareaFeed.setHint("Apa masukan anda tentang Aplikasi ini atau saran anda untuk kemajuan Aplikasi ini");

                break;
        }
    }

    public void AddImgFeed(View view) {
        checkPermissions();
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Akses Kamera Tidak Diberikan. Mohon Diberi Akses!", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                takeImgFeed();
                break;
        }
    }

    public void takeImgFeed() {
        final CharSequence[] options = {"Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                uri = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };

                Cursor c = getContentResolver().query(uri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                bitmap = getResizedBitmap(bitmap, 700);
                Log.w("path_ofimage", picturePath);
                resultImgFeed.setImageBitmap(bitmap);
                BitMapToString(bitmap);
                layoutAddImg.setVisibility(View.GONE);
                textaddImg.setVisibility(View.GONE);
                removeImg.setVisibility(View.VISIBLE);
            }
        }
    }

    public String BitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if(userImage1 != null){
            userImage1.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            Document_img1 = Base64.encodeToString(b, Base64.DEFAULT);
        }else {
            Document_img1 = null;
        }
        return Document_img1;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public void removeImgFeed(View view) {
        bitmap = null;
        layoutresultimg.setVisibility(View.GONE);
        layoutAddImg.setVisibility(View.VISIBLE);
        textaddImg.setVisibility(View.VISIBLE);
        removeImg.setVisibility(View.GONE);
    }

    public void sendFeeback(View view) {
        int feed    =   textareaFeed.getText().length();
        int emphone =   emailphone.getText().length();


        if(feed == 0){
            Toast.makeText(getApplicationContext(),"Mohon Isi Penjelasan",Toast.LENGTH_LONG).show();
        }else if(emphone == 0){
            Toast.makeText(getApplicationContext(),"Mohon Isi Email/Telepon",Toast.LENGTH_LONG).show();
        }else {
            upFeedBack();
        }
    }

    private void upFeedBack(){
        class UploadImage extends AsyncTask<Bitmap,Void,String> {
            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(FeedBackActivity.this, "Uploading Image", "Please wait...",true,true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                if(s.equals("")){
                    Toast.makeText(getApplicationContext(),"Tidak Ada Koneksi Internet",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
                    onBackPressed();
                }
                Log.e("InputLapor ", s);
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String url = (Config.URL_UPLOAD_FEEDBACK);

                String getFeed  = textareaFeed.getText().toString();
                String getEmail = emailphone.getText().toString();
                String jenis    = String.valueOf(jenisFeed);

                String uploadImage = "";
                String nameImage = "";
                if(BitMapToString(bitmap) != null){
                    uploadImage = BitMapToString(bitmap);
                    nameImage   = String.valueOf(System.currentTimeMillis());
                }
                HashMap<String,String> data = new HashMap<>();
                data.put("image", uploadImage);
                data.put("name", nameImage);
                data.put("id_master", idMaster);
                data.put("jenis_feed", jenis);
                data.put("isi_feed", getFeed);
                data.put("email_phone", getEmail);

                return rh.sendPostRequest(url, data);
            }

        }
        UploadImage ui = new UploadImage();
        ui.execute(bitmap);
    }


}
