package com.disdik.koropakapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.disdik.koropakapp.Class.AdapterEmpty;
import com.disdik.koropakapp.Class.AdapterNotif;
import com.disdik.koropakapp.Class.DatabaseHelper;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class NotifActivity extends AppCompatActivity {

    private RecyclerView listView;
    private AdapterNotif adapter;
    private AdapterEmpty adapterEmpty;
    private DatabaseHelper myDb;
    private SwipeRefreshLayout swipeRefreshLayout;

    ArrayList<HashMap<String, String>> list_data;
    private ArrayList<HashMap<String, String>> list_dataEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        myDb = new DatabaseHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        mTitle.setText("Notifikasi");
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listView = findViewById(R.id.listView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(RecyclerView.VERTICAL);
        listView.setLayoutManager(llm);

        swipeRefreshLayout  =   findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new Handler().postDelayed(new Runnable() {
                            @Override public void run() {
                                GetNotif();
                                swipeRefreshLayout.setRefreshing(false);
                            }
                        }, 2000);
                    }
                }
        );
        GetNotif();
    }

    public void GetNotif(){
        Cursor stat = myDb.cekCountNotif();
        StringBuffer buffer = new StringBuffer();
        while (stat.moveToNext() ) {
            buffer.append(stat.getString(0));
        }
        int a = Integer.parseInt(buffer.toString());
        Cursor res = myDb.getAllDataNotif();

        list_dataEmpty = new ArrayList<HashMap<String, String>>();
        list_data = new ArrayList<HashMap<String, String>>();
        if (a != 0) {
            if (res.moveToFirst()) {
                do {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id", res.getString(0));
                    map.put("title", res.getString(1));
                    map.put("date_notif", res.getString(2));
                    map.put("api", res.getString(3));
                    map.put("id_notif", res.getString(4));
                    map.put("date_Get", res.getString(6));
                    map.put("key_notif", res.getString(7));
                    list_data.add(map);
                    adapter = new AdapterNotif(getApplicationContext(), list_data);
                    ((AdapterNotif) adapter).setOnItemClickListener(new AdapterNotif.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            //Intent intent = new Intent(getApplicationContext(), DetailDisposisiBidang.class);
                            HashMap<String, String> map = (HashMap) list_data.get(position);
                            String empId = map.get("id_notif").toString();
                            //Toast.makeText(getApplicationContext(), empId, Toast.LENGTH_SHORT).show();
                            //intent.putExtra(Config.EMP_ID, empId);
                            //intent.putExtra(Config.EMP_ID_GETIMG, noSuratMsk);
                            //startActivity(intent);
                        }
                    });
                    if (getApplicationContext() != null) {
                        listView.setAdapter(adapter);
                    }
                } while (res.moveToNext());
            }
        } else {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("message", "Notifikasi Kosong!");
            map.put("isSelected", "false");
            list_dataEmpty.add(map);
            adapterEmpty = new AdapterEmpty(getApplicationContext(), list_dataEmpty);
            ((AdapterEmpty) adapterEmpty).setOnItemClickListener(new AdapterEmpty.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    // Do Something
                }
            });
            if (getApplicationContext()!=null){
                listView.setAdapter(adapterEmpty);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
